# Astralathe - Psychonauts enhancement and modding framework

Astralathe is a plugin for the modern digital release of Psychonauts. It provides a variety of engine-side enhancements for the game including bugfixes and widescreen support, as well as implementing full mod support with a full mod loading system and in-game UI for mod management.

## Features

- Several engine fixes and enhancements
- A full mod loading system complete with an in-game UI for managing mods
- Provides debugging tools including both the game's own native debug tools (level select, debug menu) and additional debug tools (restoration of debug rendering functions, an in-game Lua console)

## Installing

For information on installing and playing the game with Astralathe, please see the wiki page here: [https://gitlab.com/scrunguscrungus/astralathe/-/wikis/Installing%20Astralathe](https://gitlab.com/scrunguscrungus/astralathe/-/wikis/Installing%20Astralathe)

## Issues

While Astralathe is in a good state at the moment and no major issues are known, it is still considered to be in beta. Features may not work as intended, issues may arise in the game, it may even crash.

If you have any issues with Astralathe, or even have an idea or request for a feature to be added, please do so via the issues page.

## Building

Astralathe is built as a Visual Studio 2022 solution and should be relatively painless to build using MSBuild.

1. Clone Astralathe to a directory
2. Make sure to init Git submodules via `git submodule update --init --recursive` or equivalent GUI option
3. Build Astralathe.sln via MSBuild or Visual Studio 2022. If building via MSBuild, you will likely need to restore Nuget packages for Astralathe_CobwebDuster.