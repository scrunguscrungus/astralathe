#include "AL_VERSION.h"

#ifdef ENABLE_UPDATE_CHECK
#define CURL_STATICLIB
#include "curl/curl.h"
#endif
#ifdef _WIN32
#include "windows.h"
#endif

#include "AstralatheConfig.h"
#include <iostream>
#include <filesystem>
#include "json.hpp"

using json = nlohmann::json;

constexpr const char* pszDLL = "Astralathe.dll";

#ifdef ENABLE_UPDATE_CHECK
constexpr const char* pszUpdateCheckURL = "https://gitlab.com/api/v4/projects/34250039/releases/permalink/latest";
constexpr const char* pszReleasesPage = "https://gitlab.com/scrunguscrungus/astralathe/-/releases/permalink/latest";
#endif

bool DoesNativeAssetsExist()
{
	return std::filesystem::exists("native_assets") && std::filesystem::is_directory("native_assets");
}

void Pause()
{
	//TODO: Crossplat
#ifdef _WIN32
	std::system("pause");
#endif
}

#ifdef _WIN32
std::string GetErrorString(DWORD err)
{
	if (err == 0)
		return std::string("No error");

	LPSTR msgBuf = nullptr;
	size_t s = FormatMessageA(
		FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		err,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		reinterpret_cast<LPSTR>(&msgBuf),
		0,
		NULL
	);

	std::string msg(msgBuf, s);

	LocalFree(msgBuf);

	return msg;
}
#endif

void FirstTimeSetup()
{
	std::cout << "Beginning first time setup..." << std::endl;

	bool bSuccess = false;

	//TODO: Crossplat
#if _WIN32

	PROCESS_INFORMATION processInfo;
	STARTUPINFO sInfo = { sizeof(sInfo) };

	bSuccess = CreateProcess(L"Astralathe_CobwebDuster.exe",
		nullptr,
		nullptr,
		nullptr,
		false,
		NORMAL_PRIORITY_CLASS,
		nullptr,
		nullptr,
		&sInfo,
		&processInfo);

	if (!bSuccess)
	{
		DWORD code = GetLastError();
		std::cerr << "Failed to launch Cobweb Duster!" << std::endl;
		std::cerr << "Error: " << GetErrorString(code) << "(" << code << ")" << std::endl;
		Pause();
	}

	DWORD exitCode;
	do
	{
		GetExitCodeProcess(processInfo.hProcess, &exitCode);
	} while (exitCode == STILL_ACTIVE);

	if (exitCode != 0)
	{
		std::cerr << "It seems that Cobweb Duster did not succeed in extraction." << std::endl;
		std::cerr << "This may cause some issues when using mods." << std::endl;
		std::cerr << "Please submit a copy of astralathe_cobwebduster_log.log to the developer if it exists." << std::endl;
		Pause();
	}
	else
	{
		std::cout << "First-time setup complete!" << std::endl;
	}
#endif
}

#ifdef _WIN32
//TODO: Crossplat
void PrintProcessErrorHelp(DWORD code)
{
	switch (code)
	{
	case ERROR_FILE_NOT_FOUND:
		std::cout << "The launcher couldn't find Psychonauts.exe" << std::endl;
		std::cout << "Please make sure Astralathe is located inside the root folder of Psychonauts." << std::endl;
		break;
	case ERROR_ACCESS_DENIED:
		std::cout << "The launcher was denied access when attempting to launch Psychonauts.exe" << std::endl;
		break;
	case ERROR_NOT_ENOUGH_MEMORY:
		std::cout << "There wasn't enough system memory to launch Psychonauts. This is pretty bad." << std::endl;
		break;
	case ERROR_ELEVATION_REQUIRED:
		std::cout << "Astralathe requires administrator rights to run Psychonauts." << std::endl;
		std::cout << "You likely have compatibility options set on Psychonauts.exe that have flagged it to always launch as administrator." << std::endl;
		std::cout << "To fix this, you must either remove the compatibility options (right click > Properties > Compatibility) or run Astralathe as administrator as well (right click > Run as Administrator)." << std::endl;
		break;
	default:
		std::cout << "No help string for this error.";
		break;
	}
}

//TODO: Crossplat
PROCESS_INFORMATION StartGame()
{
	std::cout << "Starting Psychonauts..." << std::endl;

	PROCESS_INFORMATION pInfo;

	STARTUPINFO sInfo = { sizeof(sInfo) };
	bool bResult = CreateProcess(L"Psychonauts.exe",
		nullptr,
		nullptr,
		nullptr,
		false,
		NORMAL_PRIORITY_CLASS | CREATE_SUSPENDED,
		nullptr,
		nullptr,
		&sInfo,
		&pInfo);

	if (!bResult)
	{
		DWORD code = GetLastError();
		std::cerr << "Failed to start Psychonauts!" << std::endl;
		std::cerr << "Error: " << GetErrorString(code) << "(" << code << ")" << std::endl;
		std::cout << "Information:" << std::endl;
		PrintProcessErrorHelp(code);
		Pause();
		return pInfo;
	}

	return pInfo;
}

//TODO: Crossplat
bool Inject(HANDLE hTarget)
{
	std::cout << "Allocating memory in game process..." << std::endl;

	LPVOID pMem = VirtualAllocEx(hTarget,
		nullptr,
		strlen(pszDLL) + 1,
		MEM_RESERVE | MEM_COMMIT,
		PAGE_EXECUTE_READWRITE);
	if (!pMem)
	{
		std::cerr << "Failed to allocate memory in process!" << std::endl;
		DWORD code = GetLastError();
		std::cerr << "Error: " << GetErrorString(code) << "(" << code << ")" << std::endl;
		return false;
	}

	std::cout << "Writing DLL into game memory..." << std::endl;
	BOOL bWriteSuccess = WriteProcessMemory(
		hTarget,
		pMem,
		pszDLL,
		strlen(pszDLL) + 1,
		nullptr);
	if (!bWriteSuccess)
	{
		std::cerr << "Failed to write " << pszDLL << " into memory!" << std::endl;
		DWORD code = GetLastError();
		std::cerr << "Error: " << GetErrorString(code) << "(" << code << ")" << std::endl;
		return false;
	}

	HMODULE hKrnl = GetModuleHandle(L"kernel32.dll");
	if (hKrnl == NULL)
	{
		std::cerr << "Failed to get Kernel32 handle!" << std::endl;
		DWORD code = GetLastError();
		std::cerr << "Error: " << GetErrorString(code) << "(" << code << ")" << std::endl;
		return false;
	}

	LPVOID loadLib = GetProcAddress(hKrnl, "LoadLibraryA");

	std::cout << "Creating remote thread..." << std::endl;
	HANDLE hLoadLib = CreateRemoteThread(hTarget, nullptr, 0, LPTHREAD_START_ROUTINE(loadLib), pMem, 0, nullptr);
	if (hLoadLib == NULL)
	{
		std::cerr << "Failed to create thread to load library!" << std::endl;
		DWORD code = GetLastError();
		std::cerr << "Error: " << GetErrorString(code) << "(" << code << ")" << std::endl;
		return false;
	}

	std::cout << "Waiting..." << std::endl;
	WaitForSingleObject(hLoadLib, INFINITE);

	std::cout << "Done!" << std::endl;

	std::cout << "Cleaning up..." << std::endl;

	VirtualFreeEx(
		hTarget, pMem, strlen(pszDLL) + 1,
		MEM_RELEASE);

	CloseHandle(hLoadLib);

	return true;
}
#endif

//https://stackoverflow.com/questions/9786150/save-curl-content-result-into-a-string-in-c
static size_t WriteCallback(void* contents, size_t size, size_t nmemb, void* userp)
{
	((std::string*)userp)->append((char*)contents, size * nmemb);
	return size * nmemb;
}

//Update check stuff
AstralatheVersion::VersionCheckResponse GetLatestVersion()
{	
#ifdef ENABLE_UPDATE_CHECK
	std::cout << "Checking for updates...\n";

	CURL* curl = curl_easy_init();
	if (curl)
	{
		std::string readBuf;

		curl_easy_setopt(curl, CURLOPT_URL, pszUpdateCheckURL);

		curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, true); //Check URL will redirect us to latest release page

		curl_easy_setopt(curl, CURLOPT_TIMEOUT, 3);
		curl_easy_setopt(curl, CURLOPT_CONNECTTIMEOUT, 3);

		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &readBuf);

		curl_easy_setopt(curl, CURLOPT_SSL_OPTIONS, CURLSSLOPT_NATIVE_CA); //This should be default. Why isn't it default?

		CURLcode response = curl_easy_perform(curl);

		if (response == CURLE_OK)
		{
			json j = json::parse(readBuf);

			AstralatheVersion::VersionInfo v;
			v = AstralatheVersion::ParseVersion(j["tag_name"]);

			AstralatheVersion::VersionCheckResponse response(v, j["description"]);

			return response;
		}
	}

	std::cout << "Failed to check for updates..." << std::endl;
#endif
	AstralatheVersion::VersionCheckResponse response(AstralatheVersion::ERROR_VERSION, "");
	return response;
}

int main()
{
#if _WIN32
	SetWindowText(GetConsoleWindow(), L"Astralathe Launcher 2.0");
#endif

#ifdef ENABLE_UPDATE_CHECK

	AstralatheVersion::VersionCheckResponse ver = GetLatestVersion();
	if (ver.versionInfo != AstralatheVersion::ERROR_VERSION && ver.versionInfo > AstralatheVersion::CURRENT_VERSION)
	{
		std::cout << "A new version of Astralathe is available!" << std::endl;
		std::cout << "Your version: " << AstralatheVersion::CURRENT_VERSION << std::endl;
		std::cout << "Latest version: " << ver.versionInfo << std::endl;
		std::cout << "Changelist:" << std::endl;
		for (std::string change : ver.changelist)
		{
			std::cout << change << std::endl;
		}
		std::cout << std::endl;
		std::cout << "You can get the latest version from" << std::endl << pszReleasesPage << std::endl << std::endl;
		std::cout << "Do you want to be taken there now (opens default browser)? (y/n) ";

		char r;
		std::cin >> r;
		if (r == 'Y' || r == 'y')
		{
#ifdef _WIN32
			ShellExecuteA(nullptr, "open", pszReleasesPage, nullptr, nullptr, SW_SHOWNORMAL);
			exit(1);
#endif
		}
	}
	else if (ver.versionInfo < AstralatheVersion::CURRENT_VERSION)
	{
		std::cout << "Wow, you're running a newer release than the latest! Aren't you fancy?" << std::endl;
		std::cout << "Your version: " << AstralatheVersion::CURRENT_VERSION << std::endl;
		std::cout << "Latest version: " << ver.versionInfo << std::endl;
	}
	else if (ver.versionInfo == AstralatheVersion::CURRENT_VERSION)
	{
		std::cout << "Astralathe up to date on version " << ver.versionInfo << "!" << std::endl;
	}

	AstralatheConfig::Init();

	if (AstralatheConfig::GetConfigValue(CONFIG_USE_STEAM))
	{
		if (!std::filesystem::exists("steam_appid.txt"))
		{
			std::ofstream appidTxt;
			appidTxt.open("steam_appid.txt", std::ios::trunc | std::ios::out);
			if (appidTxt.is_open())
			{
				appidTxt << "3830";
				appidTxt.close();
			}
		}
	}
	else if ( std::filesystem::exists("steam_appid.txt") )
	{
		std::filesystem::remove("steam_appid.txt");
	}

#endif

	//TODO: Need to implement CobwebDuster versioning
	//If I ever change something that warrants re-extraction of native assets,
	//there's currently no way to flag that.
	if (!DoesNativeAssetsExist())
	{
		FirstTimeSetup();
	}

	PROCESS_INFORMATION process = StartGame();

	if (process.hProcess == NULL || process.hProcess == INVALID_HANDLE_VALUE)
		exit(-1);

#if _DEBUG
	Pause();
#endif

	bool bInjectSuccess = Inject(process.hProcess);

	if (!bInjectSuccess)
	{
		Pause();
		ResumeThread(process.hThread);

		TerminateProcess(process.hProcess, 1);

		CloseHandle(process.hThread);
		CloseHandle(process.hProcess);
		exit(-2);
	}
	else
	{
		std::cout << "Success! Unsuspending Psychonauts..." << std::endl;

		ResumeThread(process.hThread);

		CloseHandle(process.hThread);
		CloseHandle(process.hProcess);
	}

	exit(0);
}