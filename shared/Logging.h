#ifndef LOGGING_H
#define LOGGING_H

#include "vadefs.h"
#include "ConfigOption.h"
#include <string>

class ILogger
{
public:
	virtual void LogAstralatheMessage(std::string msg) = 0;
};

void LogInit(const char* pszFilename);
void LogSetAdditionalLogger(ILogger* logger);
void VLogMessageRaw( const char* msg, va_list args );
void LogMessageRaw( const char* msg, ... );
void LogMessage( const char* msg, ... );

void LogMessage( ConfigOption filter, const char* msg, ... );

void InitConsole();

#endif