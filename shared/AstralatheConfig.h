#ifndef ASTRALATHE_CONFIG_H
#define ASTRALATHE_CONFIG_H

#include "inireader/IniReader.h"
#include "ConfigOption.h"
#include <map>
#include <variant>
#include "Logging.h"

template <typename T>
class AstralatheConfigOption
{
public:
	AstralatheConfigOption(const char* pszName, T tDefault, const char* pszCategory = "MAIN", bool needsRestart = true) :
		m_pszName(pszName), m_pszCategory(pszCategory), m_tDefault(tDefault), m_bRequiresRestart(needsRestart)
	{
		m_tValue = tDefault;
	}

	const char* GetName() const { return m_pszName; }
	const char* GetCategory() const { return m_pszCategory; }
	const T GetDefault() const { return m_tDefault; }
	const T GetValue() const { return m_tValue; }
	const void SetValue(T newVal) { m_tValue = newVal;  }
	const bool NeedsRestart() { return m_bRequiresRestart;  }
private:
	const char* m_pszName;
	const char* m_pszCategory;
	const T m_tDefault;
	const bool m_bRequiresRestart;
	T m_tValue;
};

typedef std::variant<AstralatheConfigOption<bool>*,
	AstralatheConfigOption<int>*,
	AstralatheConfigOption<float>*> MappedConfigOption;

extern std::unordered_map<ConfigOption, MappedConfigOption> ConfigOptions;

class AstralatheConfig
{
public:
	static const int Version = 2;
	
	static void Init();

	template <typename T>
	static T GetConfigValue(ConfigOption option)
	{
		T d = T();

		if (ConfigOptions.find(option) == ConfigOptions.end())
		{
#ifdef DEBUG
			MessageBoxA(NULL, "Tried to get a non-existent config option! Did you forget to build AstralatheShared?", "Astralathe", MB_OK);
#endif
			LogMessage("Tried to get config option %i which was not defined!\n", option);
			return d;
		}

		MappedConfigOption o = ConfigOptions[option];
		try {
			AstralatheConfigOption<T>* opt = std::get<AstralatheConfigOption<T>*>(o);
			return opt->GetValue();
		}
		catch (std::bad_variant_access)
		{
			LogMessage("Tried to get config option %i as wrong type\n", option);
			return d;
		}
	};

	template <typename T>
	static void SetConfigValue(ConfigOption option, T value)
	{
		T d = T();

		if (ConfigOptions.find(option) == ConfigOptions.end())
		{
			LogMessage("Tried to get config option %i which was not defined!\n", option);
		}

		MappedConfigOption o = ConfigOptions[option];
		try {
			AstralatheConfigOption<T>* opt = std::get<AstralatheConfigOption<T>*>(o);
			opt->SetValue(value);
		}
		catch (std::bad_variant_access)
		{
			LogMessage("Tried to get config option %i as wrong type\n", option);
		}
	};

	//For legacy support reasons, this unmarked function is for bools implicitly for now
	static bool GetConfigValue(ConfigOption option);

	static const char* GetConfigOptionName(ConfigOption option);

	template <typename T>
	static bool GetRequiresRestart(ConfigOption option)
	{
		if (ConfigOptions.find(option) == ConfigOptions.end())
		{
			LogMessage("Tried to get config option %i which was not defined!\n", option);
			return false;
		}

		MappedConfigOption o = ConfigOptions[option];
		try {
			AstralatheConfigOption<T>* opt = std::get<AstralatheConfigOption<T>*>(o);
			return opt->NeedsRestart();
		}
		catch (std::bad_variant_access)
		{
			LogMessage("Tried to get config option %i as wrong type\n", option);
			return false;
		}
	}

	static void Write();
private:
	static CIniReader config;
};

#define CONFIG_ENABLED( value ) AstralatheConfig::GetConfigValue( value )

#endif