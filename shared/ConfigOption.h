#ifndef ASTRALATHE_CONFIGOPTION_H
#define ASTRALATHE_CONFIGOPTION_H

typedef enum
{
#define defEnum(x) x,
#include "ConfigEnum.h"
#undef defEnum
} ConfigOption;

#endif