#ifndef AL_VERSION_H
#define AL_VERSION_H

#include <vector>
#include <string>
#include <sstream>
//#include <algorithm>

namespace AstralatheVersion
{
	//Major and minor are always guaranteed but revision is optional
	class VersionInfo {
	public:
		int major;
		int minor;
		int rev;

		VersionInfo() : major(0), minor(0), rev(0)
		{}

		VersionInfo(int maj, int min, int rev) : major(maj), minor(min), rev(rev) {}

		bool operator==(VersionInfo const& A) const
		{
			return major == A.major &&
				minor == A.minor &&
				rev == A.rev;
		}

		bool operator>(VersionInfo const& A) const
		{
			if (major > A.major)
			{
				return true;
			}
			else if ( major == A.major )
			{
				if (minor > A.minor)
				{
					return true;
				}
				else if (minor == A.minor)
				{
					return rev > A.rev;
				}
			}
			return false;
		}
		bool operator>=(VersionInfo const& A) const
		{
			return *this == A || *this > A;
		}
		bool operator<(VersionInfo const& A) const
		{
			return A > *this;
		}
		bool operator<=(VersionInfo const& A) const
		{
			return A >= *this;
		}

		//Returns version string in the format MAJOR.MINOR.REVISION
		friend std::ostream& operator<<(std::ostream& os, const VersionInfo& A)
		{
			os << A.major << "." << A.minor << "." << A.rev;
			return os;
		}

		std::string GetString() const
		{
			if (major == -1)
				return "";
			std::stringstream str;
			str << *this;
			return str.str();
		}
	};

	class VersionCheckResponse {
	private:
		void populateChangelist(std::string lines)
		{
			std::stringstream stream(lines);
			std::string l;
			bool inChangeList = false;
			while (std::getline(stream, l))
			{
				if (inChangeList)
				{
					if (l == "====")
					{
						inChangeList = false;
						break;
					}
					else if (l.empty() || l.contains("/uploads/") ) //hack
					{
						continue;
					}
					changelist.push_back(l);
				}
				else
				{
					if (l == "==CHANGES==")
						inChangeList = true;
				}
			}
		}

	public:
		VersionCheckResponse(VersionInfo info, std::string rawChanges)
		{
			versionInfo = info;
			populateChangelist(rawChanges);
		}

		VersionInfo versionInfo;
		std::vector<std::string> changelist;
	};

	const VersionInfo CURRENT_VERSION = { 2, 1, 0 }; //THIS BUILD of Astralathe. Remember to update me!
	const VersionInfo ERROR_VERSION = { -1, -1, -1 }; //Placeholder version to indicate unknown version.

	VersionInfo ParseVersion(std::string verString);
}

#endif