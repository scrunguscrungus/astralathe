#include "physfs.h"
#include "Logging.h"
#include "AstralatheConfig.h"

#include <fstream>
#include <filesystem>
#include <unordered_set>
#include <vector>

//TODO
#include <windows.h>
#include "Astralathe.h"
#include "ModFS.h"
#include "json.hpp"

ModFS* g_pModFS;

#define PHYSFS_LastErrorString PHYSFS_getErrorByCode( PHYSFS_getLastErrorCode() )

bool ModFS::TryLoadModJSON(std::filesystem::directory_entry dir, std::string modName, std::filesystem::path modJsonPath)
{
	nlohmann::json modJsonData;
	try
	{
		std::ifstream modJsonFile(modJsonPath);
		if (!modJsonFile.is_open())
		{
			LogMessage("Failed to load mod JSON data from %s!\n", modJsonPath.string().c_str());
			return false;
		}

		modJsonFile >> modJsonData;
		modJsonFile.close();

		if (!modJsonData.contains("id") || !modJsonData["id"].is_string())
		{
			LogMessage("Mod %s has a malformed JSON file! No ID!\n", modName.c_str());
			return false;
		}

		ModData data;
		std::string modID = modJsonData["id"];
		std::string name = modJsonData.value("name", modName);
		auto existing = AllModDatas.find(modID);
		if (existing != AllModDatas.end())
		{
			LogMessage("Duplicate mod ID! Mod %s and %s have the same ID (%s)! %s will not be loaded!\n", name.c_str(), existing->second.Name.c_str(), modID.c_str(), name.c_str());
			return false;
		}
		data.ID = modID;
		data.Name = name;
		data.Author = modJsonData.value("author", "");
		data.Description = modJsonData.value("description", "");
		data.Version = AstralatheVersion::ParseVersion(modJsonData.value("version", "-1.-1.-1"));

		bool dependenciesValid = true;
		if (modJsonData.contains("dependencies") && modJsonData["dependencies"].is_array())
		{
			for (const nlohmann::json& dep : modJsonData["dependencies"])
			{
				if (!dep.contains("id"))
				{
					LogMessage("Mod %s has a malformed JSON file! Dependency with no ID!\n", data.Name.c_str());
					return false;
				}
				ModDependency depend(dep["id"], dep.value("soft", false));
				data.Dependencies.push_back(depend);
			}
		}

		LogMessage("Registered mod %s (%s) from path %s\n", data.Name.c_str(), modID.c_str(), dir.path().string().c_str());
		AllModsByFolder[modName] = modID;
		AllFoldersByMod[modID] = dir.path().lexically_relative("ModResource").string();
		AllModDatas[modID] = data;
	}
	catch (const std::exception& e)
	{
		LogMessage("Failed to load mod JSON data from %s!\n%s\n", modJsonPath.string().c_str(), e.what());
		return false;
	}
	return true;
}

void ModFS::LoadAllModData()
{
	//Loads ALL mod data just to get their info
	const std::filesystem::path modResourcePath = "ModResource";

	if (!std::filesystem::exists(modResourcePath) || !std::filesystem::is_directory(modResourcePath))
	{
		LogMessage("No ModResource folder! Creating...\n");
		std::filesystem::create_directory(modResourcePath);
	}

	const std::filesystem::path modJsonFilename = "mod.json";

	for (const std::filesystem::directory_entry& dir : std::filesystem::directory_iterator(modResourcePath))
	{
		if (!dir.is_directory())
			continue;

		std::filesystem::path modJsonPath = dir.path() / modJsonFilename;
		std::string modName = dir.path().filename().string();

		if (std::filesystem::exists(modJsonPath))
		{
			TryLoadModJSON(dir, modName, modJsonPath);
		}
		else
		{
			LogMessage("No mod.json found in %s\n", modName.c_str());
			//Legacy mod - BUT WAIT! Before we give up and just assume this is a legacy mod,
			//see if we can find a mod.json in one of the subfolders.
			bool found = false;
			for (const std::filesystem::directory_entry& subdir : std::filesystem::directory_iterator(dir))
			{
				if (!subdir.is_directory())
					continue;

				LogMessage("Checking subdirectory %s\n", subdir.path().string().c_str());

				std::filesystem::path nestedModJsonPath = subdir.path() / modJsonFilename;
				std::string nestedModName = subdir.path().filename().string();
				if (!std::filesystem::exists(nestedModJsonPath))
					continue;

				LogMessage("Mod JSON %s exists, attempting load\n", nestedModJsonPath.string().c_str());
				found = TryLoadModJSON(subdir, nestedModName, nestedModJsonPath);
				if (found)
				{
					LogMessage("Found a mod nested in subdirectory at %s\n", nestedModJsonPath.string().c_str());
				}
			}
			
			if (!found)
			{
				//Alright - we didn't find any mod.json files in any of the subfolders. Just load this as a legacy mod.
				LogMessage("Warning: Couldn't find a mod JSON at %s (or in any subfolders)!\n", modJsonPath.string().c_str());
				AllModsByFolder[modName] = modName;
				AllFoldersByMod[modName] = modName;
				ModData data;
				data.ID = modName;
				data.Name = modName;
				data.Description = "No description (no mod JSON)";
				data.Version = AstralatheVersion::ERROR_VERSION;
				data.Author = "Unknown author (no mod JSON)";
				AllModDatas[modName] = data;
			}
		}
	}
}

void ModFS::InitLoadOrder()
{
	std::ifstream lofile("astralathe_mods.txt");
	if (!lofile.good())
	{
		if (!std::filesystem::exists("astralathe_mods.txt"))
		{
			std::ofstream mods;
			mods.open("astralathe_mods.txt", std::ios::out | std::ios::trunc);
			mods.close();
		}
		else
		{
			LogMessage("Couldn't open astralathe_mods.txt\n");
			return;
		}
	}

	//Parse load order TXT
	//At this stage, we've already found all the mods, we're just figuring out which ones
	//go in the load order and where.
	std::unordered_set<std::string> modsAddedToOrder;

	std::string line;
	while (std::getline(lofile, line, '\n'))
	{
		if (line.empty()) continue;

		size_t commaIdx = line.find(',');
		std::string modFolder;
		bool enabled = true;

		if (commaIdx != std::string::npos)
		{
			modFolder = line.substr(0, commaIdx);
			enabled = line.substr(commaIdx + 1) == "1";
		}
		else
		{
			//Pre 2.0 legacy TXT
			modFolder = line;
		}

		auto it = AllModsByFolder.find(modFolder);
		if (it == AllModsByFolder.end())
		{
			LogMessage("Folder %s did not correspond to any loaded mod!\n", modFolder.c_str());
		}
		else
		{
			ModData* data = &AllModDatas[it->second];
			data->Loaded = enabled && data->CheckDependencies() < ModDependency::VersionCheckResult::BADORDER;
			modsAddedToOrder.insert(it->second);
			LoadOrder.push_back(data);
			LogMessage("Mod %s (%s) %s!\n", data->Name.c_str(), data->ID.c_str(), enabled ? "enabled" : "disabled");
		}
	}

	lofile.close();
	
	//Now, we're going to look through the mod data store and find any mods that we picked up on but weren't added to the
	//load order. Those mods are new, so we want to enable them and append them to the bottom of the load order.

	//TODO: This must be consolidated with auto-sort!
	bool newMods = false;
	std::function<void(ModData&)> RecursivelyPushToLoadOrder;
	std::unordered_set<std::string> CurrentEvaluations;

	RecursivelyPushToLoadOrder = [&](ModData& data) -> void
	{
		for (const ModDependency& dep : data.Dependencies)
		{
			if (modsAddedToOrder.find(dep.ID) != modsAddedToOrder.end()) continue;
			if (CurrentEvaluations.find(dep.ID) != CurrentEvaluations.end())
			{
				LogMessage("%s is already being evaluated!\n", dep.ID.c_str());
				continue;
			}
			CurrentEvaluations.insert(dep.ID);
			auto dependencyIt = AllModDatas.find(dep.ID);
			if (dependencyIt == AllModDatas.end()) continue;
			LogMessage("Recursing dependency %s\n", dependencyIt->first.c_str());
			RecursivelyPushToLoadOrder(dependencyIt->second);
		}
		LogMessage("Adding new mod: %s (%s)\n", data.Name.c_str(), data.ID.c_str());
		if (data.CheckDependencies() < ModDependency::VersionCheckResult::BADORDER)
		{
			data.Loaded = true;
		}
		else
		{
			LogMessage("Mod %s is missing hard dependencies! Not loading!\n", data.Name.c_str());
		}
		LoadOrder.push_back(&data);
		newMods = true;
		modsAddedToOrder.insert(data.ID);
	};
	for (auto& [id, data] : AllModDatas)
	{
		if (modsAddedToOrder.find(id) == modsAddedToOrder.end())
		{
			RecursivelyPushToLoadOrder(data);
		}
	}

	if (newMods)
	{
		WriteModLoadOrder(); //New mods found, save out the load order file
	}
}

bool ModFS::Init()
{
	LogMessage( "---INIT MODFS---\n" );
	g_pModFS = new ModFS();

	int physfs_Success = PHYSFS_init( nullptr );
	if ( !physfs_Success )
	{
		LogMessage( "ModFS init failed: %s\n", PHYSFS_LastErrorString );
		return false;
	}

	//PHYSFS_permitSymbolicLinks( 1 );

	PHYSFS_setWriteDir( PHYSFS_getBaseDir() );

	if (!std::filesystem::exists("ModData") || !std::filesystem::is_directory("ModData"))
	{
		std::filesystem::create_directory("ModData");
	}

	g_pModFS->LoadAllModData();
	g_pModFS->InitLoadOrder();
	g_pModFS->MountModPaths();

	LogMessage( "---FINISHED INITIALISING MODFS---\n" );
	return true;
}

void ModFS::RunAutoSort()
{
	std::unordered_set<std::string> modsAddedToOrder;
	std::function<void(ModData&)> RecursivelyPushToLoadOrder;
	std::unordered_set<std::string> CurrentEvaluations;

	LoadOrder.clear();

	RecursivelyPushToLoadOrder = [&](ModData& data) -> void
		{
			for (const ModDependency& dep : data.Dependencies)
			{
				if (modsAddedToOrder.find(dep.ID) != modsAddedToOrder.end()) continue;
				if (CurrentEvaluations.find(dep.ID) != CurrentEvaluations.end())
				{
					LogMessage("%s is already being evaluated!\n", dep.ID.c_str());
					continue;
				}
				CurrentEvaluations.insert(dep.ID);
				auto dependencyIt = AllModDatas.find(dep.ID);
				if (dependencyIt == AllModDatas.end()) continue;
				RecursivelyPushToLoadOrder(dependencyIt->second);
			}
			LogMessage("Adding sorted mod: %s (%s)\n", data.Name.c_str(), data.ID.c_str());
			if (data.CheckDependencies() >= ModDependency::VersionCheckResult::BADORDER)
			{
				LogMessage("Mod %s is missing hard dependencies! Not loading!\n", data.Name.c_str());
				data.Loaded = false;
			}
			LoadOrder.push_back(&data);
			modsAddedToOrder.insert(data.ID);
		};
	for (auto& [id, data] : AllModDatas)
	{
		if (modsAddedToOrder.find(id) == modsAddedToOrder.end())
		{
			RecursivelyPushToLoadOrder(data);
		}
	}
}

void ModFS::WriteModLoadOrder()
{
	LogMessage("Writing mod load order...\n");
	std::ofstream loadOrder("astralathe_mods.txt", std::ios::out | std::ios::trunc);
	if (!loadOrder.good())
	{
		LogMessage("Couldn't write mod load order! Failed to open!\n");
		return;
	}

	for (ModData* mod : LoadOrder)
	{
		std::string folder = AllFoldersByMod[mod->ID];
		bool enabled = mod->Loaded;

		loadOrder << folder << "," << (enabled ? "1" : "0") << std::endl;
	}
	loadOrder.close();
	LogMessage("Done!\n");
}

void ModFS::FixSlashes( const char* pszFilename, char pBuf[MAX_PATH] )
{
	int idx = 0;
	char cCurChar = pszFilename[ idx ];
	while ( cCurChar != '\0' )
	{
		if ( idx > MAX_PATH )
			return;
		pBuf[ idx ] = cCurChar;
		if ( cCurChar == '\\' )
		{
			pBuf[ idx ] = '/';
		}

		idx++;
		cCurChar = pszFilename[ idx ];
	}
	pBuf[ idx ] = '\0';
}

bool ModFS::ModFileExists( const char* pszFilename )
{
	return PHYSFS_exists( pszFilename );
}

bool ModFS::NativeAssetExists(const char* pszFilename)
{
	//HUGE HACK
	PHYSFS_mount("native_assets", NULL, 1);
	bool e = PHYSFS_exists(pszFilename);
	PHYSFS_unmount("native_assets");
	return e;
}

int ModFS::GetSize( const char* pszFilename )
{
	if ( !ModFileExists( pszFilename ) )
		return -1;

	PHYSFS_File* pFile = PHYSFS_openRead( pszFilename );
	int iLen = PHYSFS_fileLength( pFile );
	PHYSFS_close( pFile );
	return iLen;
}

//For each search path, check if pszFile exists in the mod and operator on it if so
//TODO: When the mod system is more fleshed out, let IForEachInSearchPath know which mod a file is from
void ModFS::ForEachInSearchPaths( std::filesystem::path File, ModFS::IForEachInSearchPath& op )
{
	std::vector<std::filesystem::path> vecPaths;
	char** SearchPaths = PHYSFS_getSearchPath();

	for ( char** i = SearchPaths; *i != nullptr; i++ )
	{
		std::filesystem::path p1 = *i;

		std::filesystem::path potentialPath = p1 / File;

		if ( std::filesystem::exists( potentialPath ) )
		{
			vecPaths.push_back( potentialPath );
		}
	}
	PHYSFS_freeList( SearchPaths );

	//getSearchPath gave us the paths in reverse load order, so go backwards through this vector
	for ( int i = vecPaths.size() - 1; i >= 0; i-- )
	{
		op.Operate( vecPaths[ i ] );
	}
}

std::string ModFS::SanitizeIntoModData(const char* pszPath)
{
	const std::string modBase = "./ModData/";

	char pszFixedSlashes[MAX_PATH];
	FixSlashes(pszPath, pszFixedSlashes);

	std::vector<std::string> pathTokens;

	std::istringstream iss(pszFixedSlashes);
	std::string token;
	while (std::getline(iss, token, '/'))
	{
		//Ignore empty token or curdir dot
		if (token.empty() || token == ".")
			continue;

		//"Go up" by removing the last token
		if (token == "..")
		{
			if (!pathTokens.empty())
				pathTokens.pop_back();
		}
		else //Put the token in our list
		{
			if ( token.find(':') != std::string::npos )
				token.erase(std::remove(token.begin(), token.end(), ':'), token.end());

			pathTokens.push_back(token);
		}
	}

	std::filesystem::path sanitized = modBase;
	for (std::string t : pathTokens)
	{
		std::filesystem::path previous = sanitized;
		sanitized /= t;
		if (std::filesystem::exists(sanitized) && std::filesystem::is_symlink(sanitized))
		{
			//Completely ignore symlinks
			sanitized = previous;
		}
	}

	return sanitized.string();
}

void ModFS::HandleWidescreenMounting()
{
	bool inWidescreen = Astralathe::Get().ShouldRunWidescreen();
	if (inWidescreen && !widescreenMounted)
	{
		//Certain assets don't fit widescreen so we provide our own special mod
		PHYSFS_mount("WidescreenResource", NULL, 1);
		LogMessage("ModFS: Mounted widescreen content\n");
		widescreenMounted = true;
	}
	else if (!inWidescreen && widescreenMounted)
	{
		PHYSFS_unmount("WidescreenResource");
		LogMessage("ModFS: Unmounted widescreen content\n");
		widescreenMounted = false;
	}
}

//This is kind of weird in how it works because I stripped it down and tweaked it for mod loading 2.0?
//Fine for now, though.
#define SEARCH_PATH_PREPEND 0
void ModFS::MountModPaths()
{
	PHYSFS_mount( PHYSFS_getBaseDir(), nullptr, 0 ); //So PHSYFS understands what we want

	std::vector<std::string> vecToMount;
	for ( ModData* mod : LoadOrder)
	{
		if (!mod->Loaded)
			continue;
		char folderPath[ MAX_PATH ]; //Base path
		snprintf( folderPath, MAX_PATH, "ModResource/%s", AllFoldersByMod[mod->ID].c_str());

		vecToMount.push_back(folderPath);
	}

	for ( std::string& path : vecToMount )
	{
		std::filesystem::path modPath = std::filesystem::path( path );
		std::string modName = modPath.filename().replace_extension().string();

		std::filesystem::path baseDir = std::filesystem::path( PHYSFS_getBaseDir() );

		LogMessage( "Mounting mod %s from %s\n", modName.c_str(), path.c_str() );
		int success = PHYSFS_mount( path.c_str(), NULL, SEARCH_PATH_PREPEND );
		if ( success == 0 )
		{
			LogMessage( "Failed to mount: %s\n", PHYSFS_LastErrorString );
		}
		else
		{
			continue;
		}
	}

	//To prevent loose loading from root folder
	PHYSFS_unmount( PHYSFS_getBaseDir() );
}

bool ModFS::LoadFile( const char* pszFilename, void* pBuf )
{
	if ( !PHYSFS_exists( pszFilename ) )
		return false;

	PHYSFS_File* pFile = PHYSFS_openRead( pszFilename );
	if ( !pFile )
	{
		LogMessage( "Failed to open mod file %s for read: %s\n", pszFilename, PHYSFS_LastErrorString );
		return false;
	}

	PHYSFS_sint64 iLen = PHYSFS_fileLength( pFile );
	PHYSFS_sint64 iRead = PHYSFS_readBytes( pFile, pBuf, iLen);
	if ( iLen != iRead )
	{
		if ( iRead == -1 )
		{
			LogMessage( "Failed to read mod file %s: %s\n", pszFilename, PHYSFS_LastErrorString );
			PHYSFS_close( pFile );
			return false;
		}
		else
		{
			LogMessage( "WARN: Read of %s requested %i bytes but read %i\n", pszFilename, iLen, iRead );
		}
	}
	PHYSFS_close( pFile );
	return true;
}

bool ModFS::LoadFile( const char* pszFilename, ModFile* pFile, bool bIncludeNativeAssets )
{
	if (bIncludeNativeAssets)
		PHYSFS_mount("native_assets", NULL, 1);
	int iSize = GetSize( pszFilename );
	if ( iSize > 0 )
	{
		pFile->SetSize( iSize );
		if ( LoadFile( pszFilename, pFile->pData ) )
		{
			strncpy_s( pFile->pszName, pszFilename, MAX_PATH );
			if (bIncludeNativeAssets)
				PHYSFS_unmount("native_assets");
			return true;
		}
	}
	if (bIncludeNativeAssets)
		PHYSFS_unmount("native_assets");
	return false;

}

bool ModFS::LoadScriptClassFile( const char* pszClassname, ModFile* pFile )
{
	char pResourcePath[ MAX_PATH ];

	ClassnameToPath( pResourcePath, pszClassname );

	int iSize = GetSize( pResourcePath );
	if ( iSize > 0 )
	{	
		pFile->SetSize( iSize );
		if ( LoadFile( pResourcePath, pFile->pData ) )
		{
			strncpy_s( pFile->pszName, pResourcePath, MAX_PATH );
			return true;
		}
		return false;
	}
	return false;
	
}

bool ModFS::LoadScriptClassFile( const char* pszClassname, void* pBuf )
{
	char pResourcePath[ MAX_PATH ];

	ClassnameToPath( pResourcePath, pszClassname );

	return LoadFile( pResourcePath, pBuf );
}

void ModFS::ClassnameToPath( char* pszPath, const char* pszClassname )
{
	char pReplaced[ MAX_PATH ];
	strncpy_s( pReplaced, MAX_PATH, pszClassname, MAX_PATH );
	for ( char* idx = pReplaced; *idx != NULL; idx = idx + 1 )
	{
		if ( *idx == '.' )
			*idx = '/';
	}

	snprintf( pszPath, MAX_PATH, "scripts/%s.lua", pReplaced );
}

const char* ModFS::AfterFirstElement( const char* pszPath )
{
	if ( !pszPath )
		return nullptr;

	int idx = 0;
	char cCurChar = pszPath[ idx ];
	while ( cCurChar != '\0' )
	{
		if ( cCurChar == '\\' || cCurChar == '/' )
		{
			return pszPath + ( idx + 1 );
		}

		idx++;
		cCurChar = pszPath[idx];
	}

	return nullptr;
}

bool ModFS::GetModFilePath( const char* pszPath, char pBuf[MAX_PATH] )
{
	char constructedPath[ MAX_PATH ];


	const char* realDir = PHYSFS_getRealDir( pszPath );
	if ( !realDir )
		return false;

	sprintf_s( constructedPath, "%s/%s", realDir, pszPath );

	strncpy_s( pBuf, MAX_PATH, constructedPath, MAX_PATH );
	return true;
}