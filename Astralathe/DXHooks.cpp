//NOTE:
//If you're wondering why this file is structured and formatted completely differently to anything else in Astralathe,
//I pulled this code from my x86Sacraments project for Silent Hill 4 and adapted it for Psychonauts (just changing DX8 to 9)

#include "Logging.h"

#include "DXHooks.h"

//NOTE:
//Building with ImGui will require you to have the DirectX9 SDK set up.

#ifdef USE_IMGUI
#pragma comment(lib, "d3d9.lib")
#pragma comment(lib, "dinput8.lib")
#pragma comment(lib, "dxguid.lib")
//#pragma comment(lib, "thirdparty/dx8/lib/dinput8.lib")
//#pragma comment(lib, "thirdparty/dx8/lib/dxguid.lib")

#include <d3d9.h>
#define DIRECTINPUT_VERSION 0x0800
#include <dinput.h>

#include "imgui.h"
#include "backends/imgui_impl_win32.h"
#include "backends/imgui_impl_dx9.h"

#include "polyhook2/Detour/x86Detour.hpp"
#include "polyhook2/PE/IatHook.hpp"
#include "polyhook2/Virtuals/VFuncSwapHook.hpp"

//#include "sacraments_imgui.h"
#include "ImGui/AstralatheImGui.h"

extern IMGUI_IMPL_API LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
#endif

typedef uint64_t Trampoline;
typedef char* HFunc;
typedef const char* Signature;
typedef unsigned int uint;

#ifdef USE_IMGUI

const char* pszImGuiINI = "astralathe_imgui.ini";
const char* pszImGuilog = "astralathe_imgui_log.log";

namespace Trampolines
{
	namespace D3D
	{
		static Trampoline Direct3DCreate9;


		static PLH::VFuncMap IDirect3D9;
		static PLH::VFuncMap IDirect3DDevice9;
	}

	namespace DInput
	{
		static Trampoline DirectInput8Create;

		static PLH::VFuncMap IDirectInput8A;

		static PLH::VFuncMap IDirectInputDevice8A;
	}
}

namespace VTableOffsets
{
	namespace D3D
	{
		const uint16_t CreateDevice = 16; //IDirect3D9

		//IDirect3DDevice9
		const uint16_t Reset = 16;
		const uint16_t EndScene = 42;
	}

	//DInput
	namespace DInput
	{
		const uint16_t CreateDevice = 3;
		const uint16_t GetDeviceState = 9;
		const uint16_t GetDeviceData = 10;
		const uint16_t SetCooperativeLevel = 13;
	}
}

bool g_bImGuiHasFocus = false;
bool g_bImGuiJustShown = false;

namespace Hooks
{
	namespace DInput
	{
		static IDirectInputDevice8A* g_pMouseDevice = nullptr;
	}

#ifdef USE_IMGUI
	namespace D3D
	{
		static IDirect3DDevice9* g_pD3DDevice = nullptr;

		static bool g_bIMGUIReady = false;

		static void DrawImGui()
		{
			if (!g_bIMGUIReady || !g_bImGuiHasFocus) { return; }

			ImGui_ImplDX9_NewFrame();
			ImGui_ImplWin32_NewFrame();

			if (g_bImGuiJustShown)
			{
				AstralatheImGui::Get().OnJustShown();
				g_bImGuiJustShown = false;
			}
			AstralatheImGui::Get().Draw();

			ImGui_ImplDX9_RenderDrawData(ImGui::GetDrawData());
		}

		static HRESULT __stdcall EndScene(IDirect3DDevice9* pThis)
		{
			DrawImGui();

			return PLH::FnCast(Trampolines::D3D::IDirect3DDevice9.at(VTableOffsets::D3D::EndScene), EndScene)(pThis);
		}

		static WNDPROC oldWndProc;
		static LRESULT CALLBACK ImguiWndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
		{
			if (uMsg == WM_SYSKEYDOWN && wParam == VK_F10)
			{
				LogMessage( CONFIG_DEBUGIMGUI, "Toggling menu focus\n");
				if (g_bImGuiHasFocus)
				{
					AstralatheImGui::Get().OnPreClose();
				}
				g_bImGuiHasFocus = !g_bImGuiHasFocus;
				if (DInput::g_pMouseDevice != nullptr)
				{
					if (g_bImGuiHasFocus)
					{
						DInput::g_pMouseDevice->Unacquire();
					}
					else
					{
						DInput::g_pMouseDevice->Acquire();
					}
				}
				if (g_bImGuiHasFocus)
				{
					g_bImGuiJustShown = true;
				}
				return TRUE;
			}
			if (g_bImGuiHasFocus)
			{
				if (ImGui_ImplWin32_WndProcHandler(hWnd, uMsg, wParam, lParam))
					return TRUE;
			}

			return CallWindowProc(oldWndProc, hWnd, uMsg, wParam, lParam);
		}

		static void InitImgui(IDirect3DDevice9* pDevice, HWND hWnd, bool bReset = false)
		{
			if (!g_bIMGUIReady)
			{
				IMGUI_CHECKVERSION();
				ImGui::CreateContext();

				ImGuiIO& io = ImGui::GetIO();
				io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;
				io.IniFilename = pszImGuiINI;
				io.LogFilename = pszImGuilog;


				ImGui::StyleColorsDark();

				LogMessage( CONFIG_DEBUGIMGUI, "ImGui: Win32 init\n");
				ImGui_ImplWin32_Init(hWnd);
				LogMessage( CONFIG_DEBUGIMGUI, "ImGui: DX9 init\n");
				ImGui_ImplDX9_Init(pDevice);

				if (!bReset)
				{
					LogMessage( CONFIG_DEBUGIMGUI, "ImGui: WndProc setup\n");
					oldWndProc = (WNDPROC)SetWindowLong(hWnd, GWL_WNDPROC, (LONG)ImguiWndProc);
				}

				LogMessage( CONFIG_DEBUGIMGUI, "ImGui initialised!\n");

				LogSetAdditionalLogger(&AstralatheImGui::Get().GetConsole());

				AstralatheImGui::Get().OnImGuiReady();

				g_bIMGUIReady = true;
			}
			else
			{
				LogMessage( CONFIG_DEBUGIMGUI, "ImGui double init?\n");
			}
		}

		static HRESULT __stdcall Reset(IDirect3DDevice9* pDevice, D3DPRESENT_PARAMETERS* pPresentationParameters)
		{
			LogMessage( CONFIG_DEBUGIMGUI, "D3D9 resetting...\n");

			ImGui_ImplDX9_Shutdown();
			ImGui_ImplWin32_Shutdown();

			ImGui::DestroyContext();

			g_bIMGUIReady = false;

			HRESULT ret = PLH::FnCast(Trampolines::D3D::IDirect3DDevice9.at(VTableOffsets::D3D::Reset), Reset)(pDevice, pPresentationParameters);

			InitImgui(pDevice, GetForegroundWindow(), true);

			return ret;
		}

		static HRESULT __stdcall hCreateDevice(
			IDirect3D9* pThis,
			UINT Adapter,
			D3DDEVTYPE DeviceType,
			HWND hFocusWindow,
			DWORD BehaviourFlags,
			D3DPRESENT_PARAMETERS* pPresentationParams,
			IDirect3DDevice9** ppDevice
		)
		{
			HRESULT ret = PLH::FnCast(Trampolines::D3D::IDirect3D9.at(VTableOffsets::D3D::CreateDevice), hCreateDevice)(pThis,
				Adapter,
				DeviceType,
				hFocusWindow,
				BehaviourFlags,
				pPresentationParams,
				ppDevice);

			g_pD3DDevice = *ppDevice;

			LogMessage( CONFIG_DEBUGIMGUI, "--IMGUI SUPPORT--\n");
			LogMessage( CONFIG_DEBUGIMGUI, "D3D9 device created!\n");
			LogMessage( CONFIG_DEBUGIMGUI, "Initialising Dear ImGui...\n");
			InitImgui(*ppDevice, hFocusWindow);

			LogMessage( CONFIG_DEBUGIMGUI, "Hooking Reset and EndScene...\n");
			PLH::VFuncMap redirect = {
				{VTableOffsets::D3D::Reset, (uint64_t)&Reset},
				{VTableOffsets::D3D::EndScene, (uint64_t)&EndScene}
			};
			PLH::VFuncSwapHook* hook = new PLH::VFuncSwapHook((const char*)g_pD3DDevice, redirect, &Trampolines::D3D::IDirect3DDevice9);
			if (hook->hook())
			{
				LogMessage( CONFIG_DEBUGIMGUI, "Hooked!\n");
			}
			else
			{
				LogMessage( CONFIG_DEBUGIMGUI, "Failed!\n");
			}

			LogMessage( CONFIG_DEBUGIMGUI, "Hope we don't crash\n");
			return ret;
		}

		static IDirect3D9* __stdcall Direct3DCreate9(UINT SDKversion)
		{
			IDirect3D9* pRet = ::Direct3DCreate9(SDKversion);

			LogMessage( CONFIG_DEBUGIMGUI, "--IMGUI SUPPORT--\n");
			LogMessage( CONFIG_DEBUGIMGUI, "D3D9 version %u created! Hooking CreateDevice...\n", SDKversion);

			PLH::VFuncMap redirect = { {VTableOffsets::D3D::CreateDevice, (uint64_t)&hCreateDevice} };
			PLH::VFuncSwapHook* hook = new PLH::VFuncSwapHook((const char*)pRet, redirect, &Trampolines::D3D::IDirect3D9);
			if (hook->hook())
			{
				LogMessage( CONFIG_DEBUGIMGUI, "Hooked!\n");
			}
			else
			{
				LogMessage( CONFIG_DEBUGIMGUI, "Failed!\n");
			}

			return pRet;
		}
	}

	namespace DInput
	{

		static HRESULT __stdcall SetCooperativeLevel(IDirectInputDevice8A* pThis,
			HWND hwnd,
			DWORD dwFlags)
		{
			DIDEVICEINSTANCEA inst;
			inst.dwSize = sizeof(DIDEVICEINSTANCEA);

			HRESULT success = pThis->GetDeviceInfo(&inst);
			if (success == DI_OK)
			{
				LogMessage(CONFIG_DEBUGIMGUI, "Setting cooperative level of device %s: %x\n", inst.tszInstanceName, dwFlags);
			}
			else
			{
				LogMessage(CONFIG_DEBUGIMGUI, "Set cooperative level, but we couldn't get any information about it. Result: %x\n", success);
			}
			return PLH::FnCast(Trampolines::DInput::IDirectInputDevice8A.at(VTableOffsets::DInput::SetCooperativeLevel), SetCooperativeLevel)(
				pThis, hwnd, dwFlags);
		}

		static HRESULT __stdcall GetDeviceData(IDirectInputDevice8A* pThis,
			DWORD cbObjectData,
			LPDIDEVICEOBJECTDATA rgdod,
			LPDWORD pdwInOut,
			DWORD dwFlags)
		{
			ImGuiIO& io = ImGui::GetIO();
			if (g_bImGuiHasFocus && (io.WantCaptureMouse || io.WantCaptureKeyboard))
			{
				//TODO: Like in the GetDeviceState example below, this is MASSIVELY schnasty.
				//Is it even necessary since we now unacquire/acquire the mouse ourself in WndProc
				//to work around the game's mouse exclusivity?
				rgdod = nullptr;
				*pdwInOut = 0;
			}
			//LogMessage("GetDeviceData %u elements, %u flags\n", *pdwInOut, dwFlags);
			HRESULT ret = PLH::FnCast(Trampolines::DInput::IDirectInputDevice8A.at(VTableOffsets::DInput::GetDeviceData), GetDeviceData)(
				pThis, cbObjectData, rgdod, pdwInOut, dwFlags);
			//LogMessage("GetDeviceData returned %u elements\n", *pdwInOut);
			return ret;
		}

		static HRESULT __stdcall GetDeviceState(IDirectInputDevice8A* pThis,
			DWORD cbData,
			LPVOID lpvData)
		{
			ImGuiIO& io = ImGui::GetIO();
			if (g_bImGuiHasFocus && (io.WantCaptureMouse || io.WantCaptureKeyboard))
			{
				//TODO: A little dirty but it works. Proper way would probably be to hook the game's input handling properly.
				memset(lpvData, 0x0, cbData);
				return DI_OK;
			}
			HRESULT ret = PLH::FnCast(Trampolines::DInput::IDirectInputDevice8A.at(VTableOffsets::DInput::GetDeviceState), GetDeviceState)(
				pThis, cbData, lpvData);
			return ret;
		}

		static bool bHookedIDirectInputDevice8A = false;
		static HRESULT __stdcall CreateDevice(IDirectInput8A* pThis, REFGUID rguid, LPDIRECTINPUTDEVICE8A* lplpDirectInputDevice, LPUNKNOWN pUnkOuter)
		{
			const GUID* pGUID1 = &GUID_SysKeyboard;
			const GUID* pGUID2 = &GUID_SysMouse;
			HRESULT ret = PLH::FnCast(Trampolines::DInput::IDirectInput8A.at(VTableOffsets::DInput::CreateDevice), CreateDevice)(
				pThis, rguid, lplpDirectInputDevice, pUnkOuter);

			if (!bHookedIDirectInputDevice8A)
			{
				PLH::VFuncMap redirect = {
					{VTableOffsets::DInput::GetDeviceState , (uint64_t)&GetDeviceState },
					{VTableOffsets::DInput::GetDeviceData, (uint64_t)&GetDeviceData },
					{VTableOffsets::DInput::SetCooperativeLevel, (uint64_t)&SetCooperativeLevel },
				};
				PLH::VFuncSwapHook* hook = new PLH::VFuncSwapHook((const char*)*lplpDirectInputDevice, redirect, &Trampolines::DInput::IDirectInputDevice8A);
				if (hook->hook())
				{
					LogMessage( CONFIG_DEBUGIMGUI, "Hooked!\n");
				}
				else
				{
					LogMessage( CONFIG_DEBUGIMGUI, "Failed!\n");
				}
				bHookedIDirectInputDevice8A = true;
			}

			LogMessage( CONFIG_DEBUGIMGUI, "Created a DInput device, result %i\n", ret);
			if (IsEqualGUID(rguid, *pGUID2))
			{
				LogMessage( CONFIG_DEBUGIMGUI, "Is mouse device\n");
				g_pMouseDevice = *lplpDirectInputDevice;
			}
			else if (IsEqualGUID(rguid, *pGUID1))
			{
				LogMessage( CONFIG_DEBUGIMGUI, "Is keyboard device\n");
			}
			else
			{
				LogMessage( CONFIG_DEBUGIMGUI, "Unhandled device\n");
				IDirectInputDevice8A* pDevice = *lplpDirectInputDevice;
				if (pDevice)
				{
					DIDEVICEINSTANCEA inst;
					inst.dwSize = sizeof(DIDEVICEINSTANCEA);

					HRESULT success = pDevice->GetDeviceInfo(&inst);
					if (success == DI_OK)
					{
						LogMessage( CONFIG_DEBUGIMGUI, "Name: %s\n", inst.tszProductName);
						LogMessage( CONFIG_DEBUGIMGUI, "Type: %s\n", inst.tszInstanceName);
					}
					else
					{
						LogMessage( CONFIG_DEBUGIMGUI, "...but we couldn't get any information about it. Result: %x\n", success);
					}
				}
				else
				{
					LogMessage( CONFIG_DEBUGIMGUI, "...but it was null.\n");
				}
			}
			return ret;
		}

		static HRESULT __stdcall DirectInput8Create(HINSTANCE hInst,
			DWORD dwVersion,
			REFIID riidltf,
			LPVOID* ppvOut,
			LPUNKNOWN punkouter)
		{
			HRESULT hRet = ::DirectInput8Create(hInst, dwVersion, riidltf, ppvOut, punkouter);

			LogMessage( CONFIG_DEBUGIMGUI, "--IMGUI SUPPORT--\n");
			LogMessage( CONFIG_DEBUGIMGUI, "DInput8 (version %x, ref %x) created! Hooking what we care about...\n", dwVersion, riidltf.Data1);
			IDirectInput8A* pInput = *(IDirectInput8A**)ppvOut;

			PLH::VFuncMap redirect = { {VTableOffsets::DInput::CreateDevice, (uint64_t)&CreateDevice} };
			PLH::VFuncSwapHook* hook = new PLH::VFuncSwapHook((const char*)pInput, redirect, &Trampolines::DInput::IDirectInput8A);
			if (hook->hook())
			{
				LogMessage( CONFIG_DEBUGIMGUI, "Hooked!\n");
			}
			else
			{
				LogMessage( CONFIG_DEBUGIMGUI, "Failed!\n");
			}

			return hRet;
		}
	}
#endif
}

#endif

#ifdef USE_IMGUI
static IDirect3DDevice9* g_pD3DDevice = nullptr;

#endif

#ifdef USE_IMGUI
#pragma comment(user, "ImGui build")

void ApplyDXHooks()
{
	LogMessage( CONFIG_DEBUGIMGUI, "--IMGUI SUPPORT--\n");
	LogMessage( CONFIG_DEBUGIMGUI, "Hooking Direct3DCreate9...\n");

	//This'll crash in debug if you don't use debug PolyHook2
	std::string DLL = "d3d9.dll";
	std::string Func = "Direct3DCreate9";
	std::wstring Module = L"Psychonauts.exe";
	PLH::IatHook* createDeviceHook = new PLH::IatHook(DLL, Func, (HFunc)&Hooks::D3D::Direct3DCreate9, &Trampolines::D3D::Direct3DCreate9, Module);
	if (createDeviceHook->hook())
	{
		LogMessage( CONFIG_DEBUGIMGUI, "Hooked!\n");
	}
	else
	{
		LogMessage( CONFIG_DEBUGIMGUI, "Failed!\n");
		return;
	}

	LogMessage( CONFIG_DEBUGIMGUI, "Hooking DInput8...\n");
	PLH::IatHook* inpCreateDeviceHook = new PLH::IatHook("dinput8.dll", "DirectInput8Create", (HFunc)&Hooks::DInput::DirectInput8Create, &Trampolines::DInput::DirectInput8Create, Module);
	if (inpCreateDeviceHook->hook())
	{
		LogMessage( CONFIG_DEBUGIMGUI, "Hooked!\n");
	}
	else
	{
		LogMessage( CONFIG_DEBUGIMGUI, "Failed!\n");
		return;
	}
}
#else
#pragma comment(user, "ImGui-less build")

void ApplyDXHooks()
{
	LogMessage( CONFIG_DEBUGIMGUI, "ImGui disabled, not applying DX hooks\n");
}
#endif