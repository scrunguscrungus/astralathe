#pragma once

template<typename T>
class Singleton
{
public:
	Singleton& operator=(const Singleton&) = delete;

	static T& Get()
	{
		static T _instance;
		return _instance;
	}
};