#ifndef ASTRALATHE_H
#define ASTRALATHE_H

#include "Discord/DiscordManager.h"
#include "Singleton.h"
#include "HookDef.h"

class Astralathe : public Singleton<Astralathe>
{
private:
	DiscordManager m_DiscordManager;

public:
	Astralathe()
		: m_DiscordManager(pszDiscordApplicationID)
	{}

	static DiscordManager& DiscordManager() { return Get().m_DiscordManager; }

	void Startup();
	void Shutdown();

	bool ShouldRunWidescreen();

private:
	void SetupConsole(); //Sets up the console output and redirects STDOUT

	void SetupHooks();
};

#endif