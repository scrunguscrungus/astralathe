#include "LuaSecurity.h"
#include "Logging.h"
#include "ModFS.h"

#ifdef _WIN32
#include "windows.h"
#include <io.h>
#define F_OK 0
#define access _access
#endif

#include "stdio.h"
#include <string>
#include <filesystem>


void __cdecl InstallSecurity_Execute(lua_State* L)
{
	lua_pushnil(L);
	lua_setglobal(L, "execute");
}

//This isn't really a security consideration per se but there's no real reason a mod should need to do this
void __cdecl InstallSecurity_Exit(lua_State* L)
{
	lua_pushnil(L);
	lua_setglobal(L, "exit");
}

void __cdecl InstallSecurity_getenv(lua_State* L)
{
	lua_pushnil(L);
	lua_setglobal(L, "getenv");
}

//This actually works fine as a callable, but Double Fine forgot to define OLD_ANSI when compiling
//Psychonauts so their version of Lua macros strerror to always be "generic I/O error" and errno
//to always be -1 which is not useful.
static int pushresult(lua_State* L, int i) {
	if (i) {
		lua_pushuserdata(L, NULL);
		return 1;
	}
	else {
		lua_pushnil(L);
		lua_pushstring(L, strerror(errno));
		lua_pushnumber(L, errno);
		return 3;
	}
}

static int __cdecl secure_io_open(lua_State* L)
{
	IOCtrl* ctrl = (IOCtrl*)lua_touserdata(L, -1);
	FILE* f;
	lua_pop(L, 1);  /* remove upvalue */

	char* pszPath = luaL_check_string(L, 1);
	std::string sanitary = g_pModFS->SanitizeIntoModData(pszPath);

	f = fopen(sanitary.c_str(), luaL_check_string(L, 2));
	if (f) {
		lua_pushusertag(L, f, ctrl->iotag);
		return 1;
	}
	else
		return pushresult(L, 0);
}

static int __cdecl io_readfrom(lua_State* L) {
	return secure_io_fromto(L, 0, "r");
}

static int __cdecl secure_io_writeto(lua_State* L)
{
	return secure_io_fromto(L, 1, "w");
}

static void __cdecl setfilebyname(lua_State* L, IOCtrl* ctrl, FILE* f,
	const char* name) {
	lua_pushusertag(L, f, ctrl->iotag);
	lua_setglobal(L, name);
}

#define setfile(L,ctrl,f,inout)	(setfilebyname(L,ctrl,f,filenames[inout]))	

static FILE* __cdecl getnonullfile(lua_State* L, IOCtrl* ctrl, int arg) {
	FILE* f = (FILE*)gethandle(L, ctrl, arg);
	luaL_arg_check(L, f, arg, "invalid file handle");
	return f;
}

static int __cdecl closefile(lua_State* L, IOCtrl* ctrl, FILE* f) {
	if (f == stdin || f == stdout || f == stderr)
		return 1;
	else {
		lua_pushusertag(L, f, ctrl->iotag);
		lua_settag(L, ctrl->closedtag);
		return (fclose(f) == 0);
	}
}

int __cdecl secure_io_fromto(lua_State* L, int inout, const char* mode) {
	IOCtrl* ctrl = (IOCtrl*)lua_touserdata(L, -1);
	FILE* current;
	lua_pop(L, 1);  /* remove upvalue */
	if (lua_isnull(L, 1)) {
		closefile(L, ctrl, (FILE*)getfilebyref(L, ctrl, inout));
		current = (inout == 0) ? stdin : stdout;
	}
	else if (lua_tag(L, 1) == ctrl->iotag)  /* deprecated option */
		current = (FILE*)lua_touserdata(L, 1);
	else {
		char* pszPath = luaL_check_string(L, 1);
		std::string sanitary = g_pModFS->SanitizeIntoModData(pszPath);
		current = fopen(sanitary.c_str(), mode);
	}
	if (current == NULL)
		return pushresult(L, 0);
	else {
		setfile(L, ctrl, current, inout);
		lua_pushusertag(L, current, ctrl->iotag);
		return 1;
	}
}

int __cdecl __cdecl secure_io_appendto(lua_State* L) {
	IOCtrl* ctrl = (IOCtrl*)lua_touserdata(L, -1);
	FILE* current;
	lua_pop(L, 1);  /* remove upvalue */
	char* pszPath = luaL_check_string(L, 1);
	std::string sanitary = g_pModFS->SanitizeIntoModData(pszPath);
	current = fopen(sanitary.c_str(), "a");
	if (current == NULL)
		return pushresult(L, 0);
	else {
		setfile(L, ctrl, current, 1);
		lua_pushusertag(L, current, ctrl->iotag);
		return 1;
	}
}

static int __cdecl io_flush(lua_State* L) {
	IOCtrl* ctrl = (IOCtrl*)lua_touserdata(L, -1);
	FILE* f;
	lua_pop(L, 1);  /* remove upvalue */
	f = (FILE*)gethandle(L, ctrl, 1);
	luaL_arg_check(L, f || lua_isnull(L, 1), 1, "invalid file handle");
	return pushresult(L, fflush(f) == 0);
}

static int __cdecl read_chars(lua_State* L, FILE* f, size_t n) {
	char* buffer;
	size_t n1;
	char statbuff[BUFSIZ];
	if (n <= BUFSIZ)
		buffer = statbuff;
	else {
		buffer = (char*)malloc(n);
		if (buffer == NULL)
		{
			lua_error(L, "not enough memory to read a file");
			return false; //Shut up, C6387
		}
	}
	n1 = fread(buffer, sizeof(char), n, f);
	lua_pushlstring(L, buffer, n1);
	if (buffer != statbuff) free(buffer);
	return (n1 > 0 || n == 0);
}

static int __cdecl read_number(lua_State* L, FILE* f) {
	double d;
	if (fscanf(f, "%lf", &d) == 1) {
		lua_pushnumber(L, d);
		return 1;
	}
	else return 0;  /* read fails */
}

static int __cdecl read_word(lua_State* L, FILE* f) {
	int c;
	luaL_Buffer b;
	luaL_buffinit(L, &b);
	do { c = fgetc(f); } while (isspace(c));  /* skip spaces */
	while (c != EOF && !isspace(c)) {
		luaL_putchar(&b, c);
		c = fgetc(f);
	}
	ungetc(c, f);
	luaL_pushresult(&b);  /* close buffer */
	return (lua_strlen(L, -1) > 0);
}	

static int __cdecl read_line(lua_State* L, FILE* f) {
	int n = 0;
	luaL_Buffer b;
	luaL_buffinit(L, &b);
	for (;;) {
		char* p = luaL_prepbuffer(&b);
		if (!fgets(p, LUAL_BUFFERSIZE, f))  /* read fails? */
			break;
		n = strlen(p);
		if (p[n - 1] != '\n')
			luaL_addsize(&b, n);
		else {
			luaL_addsize(&b, n - 1);  /* do not add the `\n' */
			break;
		}
	}
	luaL_pushresult(&b);  /* close buffer */
	return (n > 0);  /* read something? */
}

static void __cdecl read_file(lua_State* L, FILE* f) {
	size_t len = 0;
	size_t size = BUFSIZ;
	char* buffer = (char*)malloc(size);
	if (buffer == NULL) { //Astralathe: New check here to catch asap
		lua_error(L, "not enough memory to read a file");
		return;
	}
	memset(buffer, 0x0, size); //Astralathe: Zero memory to ensure no garbage

	for (;;) {
		char* newbuffer = (char*)realloc(buffer, size);
		if (newbuffer == NULL) {
			free(buffer);
			lua_error(L, "not enough memory to read a file");
			return; //Explicit return to quiet C6387
		}
		else if (size > BUFSIZ) //Astralathe: Size is bigger than our default means we've realloc'd to a bigger size
		{
			size_t sizeIncrease = size / 2; //Astralathe: We always do size*2 so we know the increase will be equal to current size/2
			memset(newbuffer + sizeIncrease, 0x0, sizeIncrease); //Astralathe: Once again, clear the garbage
		}
		buffer = newbuffer;
		//This is still gonna fuck up the buffer if the file's in write mode and not read mode but I think I've done all I can
		size_t readCount = fread(buffer + len, sizeof(char), size - len, f);
		len += readCount;
		if ((len < size) || (feof(f)) || ferror(f)) //Failed to read all data or reached EOF
		{
			buffer[len + readCount] = '\0';
			break;
		}
		size *= 2;
	}
	if (ferror(f))
	{
		lua_pushnil(L);
		free(buffer); //Welp, don't need this
		return;
	}
	lua_pushlstring(L, buffer, len);
	free(buffer);
	return;
}

static int __cdecl io_read(lua_State* L) {	
	IOCtrl* ctrl = (IOCtrl*)lua_touserdata(L, -1);
	int lastarg = lua_gettop(L) - 1;
	int firstarg = 1;
	FILE* f = (FILE*)gethandle(L, ctrl, firstarg);
	int n;
	if (f) firstarg++;
	else f = (FILE*)getfilebyref(L, ctrl, 0);  /* get _INPUT */
	lua_pop(L, 1);
	if (firstarg > lastarg) {  /* no arguments? */
		lua_settop(L, 0);  /* erase upvalue and other eventual garbage */
		firstarg = lastarg = 1;  /* correct indices */
		lua_pushstring(L, "*l");  /* push default argument */
	}
	else  /* ensure stack space for all results and for auxlib's buffer */
		luaL_checkstack(L, lastarg - firstarg + 1 + LUA_MINSTACK, "too many arguments");
	for (n = firstarg; n <= lastarg; n++) {
		int success;
		if (lua_isnumber(L, n))
			success = read_chars(L, f, (size_t)lua_tonumber(L, n));
		else {
			const char* p = luaL_check_string(L, n);

			//Pattern support deprecated so why even bother? Let people use
			//shit without the "*"
			int c = 0;
			if (p[0] == '*')
				c = 1;

			switch (p[c]) {
				case 'n':  /* number */
					if (!read_number(L, f)) goto endloop;  /* read fails */
					continue;  /* number is already pushed; avoid the "pushstring" */
				case 'l':  /* line */
					success = read_line(L, f);
					break;
				case 'a':  /* file */
					read_file(L, f);
					success = 1; /* always success */
					break;
				case 'w':  /* word */
					success = read_word(L, f);
					break;
				default:
					luaL_argerror(L, n, "invalid format");
					success = 0;  /* to avoid warnings */
			}
		}
		if (!success) {
			lua_pop(L, 1);  /* remove last result */
			break;  /* read fails */
		}
	} endloop:
	return n - firstarg;
}

static int __cdecl io_close(lua_State* L) {
	IOCtrl* ctrl = (IOCtrl*)lua_touserdata(L, -1);
	lua_pop(L, 1);  /* remove upvalue */
	return pushresult(L, closefile(L, ctrl, getnonullfile(L, ctrl, 1)));
}

static int __cdecl luaL_findstring(const char* name, const char* const list[]) {
	int i;
	for (i = 0; list[i]; i++)
		if (strcmp(list[i], name) == 0)
			return i;
	return -1;  /* name not found */
}

static int __cdecl io_seek(lua_State* L) {
	static const int mode[] = { SEEK_SET, SEEK_CUR, SEEK_END };
	static const char* const modenames[] = { "set", "cur", "end", NULL };
	IOCtrl* ctrl = (IOCtrl*)lua_touserdata(L, -1);
	FILE* f;
	int op;
	long offset;
	lua_pop(L, 1);  /* remove upvalue */
	f = getnonullfile(L, ctrl, 1);
	op = luaL_findstring(luaL_opt_string(L, 2, "cur"), modenames);
	offset = luaL_opt_long(L, 3, 0);
	luaL_arg_check(L, op != -1, 2, "invalid mode");
	op = fseek(f, offset, mode[op]);
	if (op)
		return pushresult(L, 0);  /* error */
	else {
		lua_pushnumber(L, ftell(f));
		return 1;
	}
}

static int __cdecl io_write(lua_State* L) {
	int lastarg = lua_gettop(L) - 1;
	IOCtrl* ctrl = (IOCtrl*)lua_touserdata(L, -1);
	int arg = 1;
	int status = 1;
	FILE* f = (FILE*)gethandle(L, ctrl, arg);
	if (f) arg++;
	else f = (FILE*)getfilebyref(L, ctrl, 1);  /* get _OUTPUT */
	for (; arg <= lastarg; arg++) {
		if (lua_type(L, arg) == LUA_TNUMBER) {  /* LUA_NUMBER */
			/* optimization: could be done exactly as for strings */
			status = status && fprintf(f, "%.16f", lua_tonumber(L, arg)) > 0;
		}
		else {
			size_t l;
			const char* s = luaL_check_lstr(L, arg, &l);
			status = status && (fwrite(s, sizeof(char), l, f) == l);
		}
	}
	pushresult(L, status);
	return 1;
}

static int __cdecl secure_io_rename(lua_State* L) {
	char* pszPath1 = luaL_check_string(L, 1);
	char* pszPath2 = luaL_check_string(L, 2);
	std::string sanitary1 = g_pModFS->SanitizeIntoModData(pszPath1);
	std::string sanitary2 = g_pModFS->SanitizeIntoModData(pszPath2);

	return pushresult(L, rename(sanitary1.c_str(), sanitary2.c_str()) == 0);
}
static int __cdecl secure_io_remove(lua_State* L) {
	char* pszPath = luaL_check_string(L, 1);
	std::string sanitary = g_pModFS->SanitizeIntoModData(pszPath);

	return pushresult(L, remove(sanitary.c_str()) == 0);
}
static int __cdecl io_exists(lua_State* L) {
	char* pszPath = luaL_check_string(L, 1);
	std::string sanitary = g_pModFS->SanitizeIntoModData(pszPath);

	return pushresult(L, access(sanitary.c_str(), F_OK) == 0);
}

static int failifdirnotexist(lua_State* L, std::string dir)
{
	if (std::filesystem::is_directory(dir))
	{
		lua_pushuserdata(L, NULL);
		return 1;
	}
	else
	{
		lua_pushnil(L);
		return 1;
	}
}

static int __cdecl io_direxists(lua_State* L) {
	char* pszPath = luaL_check_string(L, 1);
	std::string sanitary = g_pModFS->SanitizeIntoModData(pszPath);

	return failifdirnotexist(L, sanitary);
}

static int __cdecl io_mkdirs(lua_State* L) {
	char* pszPath = luaL_check_string(L, 1);
	std::string sanitary = g_pModFS->SanitizeIntoModData(pszPath);

	std::filesystem::create_directories(sanitary);

	return failifdirnotexist(L, sanitary);
}


static int file_collect(lua_State* L) {
	IOCtrl* ctrl = (IOCtrl*)lua_touserdata(L, -1);
	FILE* f = getnonullfile(L, ctrl, 1);
	if (f != stdin && f != stdout && f != stderr)
		fclose(f);
	return 0;
}

//Overrides for other iolib funcs, as well as our custom ones
static const struct luaL_reg iolib[]{
	{"rename", secure_io_rename},
	{"remove", secure_io_remove},

	{"fexists", io_exists},
	{"direxists", io_direxists},
	{"mkdirs", io_mkdirs},
};

static const struct luaL_reg iolibtag[] = {
  {"appendto", secure_io_appendto},

  {"closefile",   io_close},
  {"fclose",   io_close},

  {"flush",     io_flush},
  {"fflush",     io_flush},

  {"openfile",   secure_io_open},
  {"fopen",   secure_io_open},

  {"read",     io_read},
  {"fread",     io_read},

  {"readfrom", io_readfrom},
  {"seek",     io_seek},

  {"write",    io_write},
  {"fwrite",    io_write},

  {"writeto",  secure_io_writeto}
};

//Complete replacement for openwithcontrol (liolib.c:686) that installs secure versions of functions instead of the defaults
//This means the default closures never even get pushed
void __cdecl LuaSecurity_openwithcontrol(lua_State* L)
{
	IOCtrl* ctrl = (IOCtrl*)lua_newuserdata(L, sizeof(IOCtrl));
	unsigned int i;
	ctrl->iotag = lua_newtag(L);
	ctrl->closedtag = lua_newtag(L);
	for (i = 0; i < sizeof(iolibtag) / sizeof(iolibtag[	0]); i++) {
		/* put `ctrl' as upvalue for these functions */
		lua_pushvalue(L, -1);
		lua_pushcclosure(L, iolibtag[i].func, 1);
		lua_setglobal(L, iolibtag[i].name);
	}
	/* create references to variable names */
	lua_pushstring(L, filenames[0]);
	ctrl->ref[0] = lua_ref(L, 1);
	lua_pushstring(L, filenames[1]);
	ctrl->ref[1] = lua_ref(L, 1);
	/* predefined file handles */
	setfile(L, ctrl, stdin, 0);
	setfile(L, ctrl, stdout, 1);
	setfilebyname(L, ctrl, stdin, "_STDIN");
	setfilebyname(L, ctrl, stdout, "_STDOUT");
	setfilebyname(L, ctrl, stderr, "_STDERR");
	/* close files when collected */
	lua_pushcclosure(L, file_collect, 1);  /* pops `ctrl' from stack */
	lua_settagmethod(L, ctrl->iotag, "gc");
}

static int passresults(lua_State* L, int status, int oldtop) {
	static const char* const errornames[] =
	{ "ok", "run-time error", "file error", "syntax error",
	 "memory error", "error in error handling" };
	if (status == 0) {
		int nresults = lua_gettop(L) - oldtop;
		if (nresults > 0)
			return nresults;  /* results are already on the stack */
		else {
			lua_pushuserdata(L, NULL);  /* at least one result to signal no errors */
			return 1;
		}
	}
	else {  /* error */
		lua_pushnil(L);
		lua_pushstring(L, errornames[status]);  /* error code */
		return 2;
	}
}

ModFile LuaBDoFileModFile;
static int luaB_dofile(lua_State* L) {
	int oldtop = lua_gettop(L);
	const char* fname = luaL_opt_string(L, 1, NULL);

	std::string sanitary;
	if (g_pModFS->ModFileExists(fname))
	{
		if (g_pModFS->LoadFile(fname, &LuaBDoFileModFile))
		{
			sanitary = LuaBDoFileModFile.pszName;
		}
	}
	else
	{
	//	LogMessage("%s did not exist!\n", fname);
		//It wasn't in a mod, but there's a good chance it's hanging around in native_assets
		//For dofiles the game doesn't normally check its normal search paths
		if (g_pModFS->NativeAssetExists(fname))
		{
	//		LogMessage("%s is a native asset!\n", fname);
			if (g_pModFS->LoadFile(fname, &LuaBDoFileModFile, true))
			{
	//			LogMessage("Loading %s via NativeAssetExists hack!\n", fname);
				sanitary = LuaBDoFileModFile.pszName;
			}
		}
		else
		{
	//		LogMessage("%s is not a native asset!\n", fname);
		}
	}

	return passresults(L, lua_dofile(L, sanitary.c_str()), oldtop);
}

void InstallSecurity_dofile(lua_State* L)
{
	lua_pushnil(L);
	lua_setglobal(L, "dofile");

	lua_pushcclosure(L, luaB_dofile, 0);
	lua_setglobal(L, "dofile");
}


void InstallLuaSecurity(lua_State* L)
{
	InstallSecurity_Execute(L);
	InstallSecurity_Exit(L);
	InstallSecurity_getenv(L);
	InstallSecurity_dofile(L);

	for (unsigned int i = 0; i < sizeof(iolib) / sizeof(iolib[0]); i++) {
		lua_pushnil(L);
		lua_setglobal(L, iolib[i].name);

		//The below call crashed for a while
		//It took me 2 hours to even figure out that it was this call that was crashing
		//It took less than a minute to read the Lua docs and figure out that
		//the final argument is for upvalues and that these functions, having
		//no upvalues, needed a final argument of 0.
		//
		//I didn't realise this because of a convoluted callstack and the fact that
		//this actually partially worked until I added the new directory-related functions.
		//I thought it was memory corruption. Maybe it was. I'm not sure what the fuck
		//Lua is actually doing internally that causes a bad state when this arg is wrong.
		lua_pushcclosure(L, iolib[i].func, 0);
		lua_setglobal(L, iolib[i].name);
	}
}