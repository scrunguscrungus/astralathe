#include "SettingsMenu.h"

void SettingsMenu::BoolBox(ConfigOption option, const char* label)
{
	bool currentConfigValue = AstralatheConfig::GetConfigValue(option);
	bool needRestart = AstralatheConfig::GetRequiresRestart<bool>(option);

	bool newValue = currentConfigValue;
	ImGui::Checkbox(label, &newValue);

	if (newValue != currentConfigValue)
	{
		//Autohook here is a hack, whatever
		if ( (needRestart || option == CONFIG_AUTOHOOK) && !OldOptionStatesSet[option])
		{
			OldOptionStatesSet[option] = true;
			OldOptionStates[option].isBool = true;
			OldOptionStates[option].ValueBool = currentConfigValue;
		}
		AstralatheConfig::SetConfigValue(option, newValue);
	}
}

void SettingsMenu::FloatBox(ConfigOption option, const char* label, float min, float max)
{
	float currentConfigValue = AstralatheConfig::GetConfigValue<float>(option);
	bool needRestart = AstralatheConfig::GetRequiresRestart<float>(option);

	float newValue = currentConfigValue;
	ImGui::DragFloat(label, &newValue, 1.0f, min, max);

	if (newValue != currentConfigValue)
	{
		if (needRestart && !OldOptionStatesSet[option])
		{
			OldOptionStatesSet[option] = true;
			OldOptionStates[option].isBool = false;
			OldOptionStates[option].ValueFloat = currentConfigValue;
		}
		AstralatheConfig::SetConfigValue(option, newValue);
	}
}

bool SettingsMenu::CheckDirty()
{
	for (int i = 0; i < CONFIG_COUNT; i++)
	{
		if (i == CONFIG_AUTOHOOK) continue;

		if (OldOptionStatesSet[i] && OldOptionStates[i].isBool )
		{
			return OldOptionStates[i].ValueBool != AstralatheConfig::GetConfigValue((ConfigOption)i);
		}
		else if (OldOptionStatesSet[i])
		{
			return OldOptionStates[i].ValueFloat != AstralatheConfig::GetConfigValue<float>((ConfigOption)i);
		}
	}
	return false;
}

void SettingsMenu::Draw()
{
	if (!ShouldDraw())
		return;

	ImGui::SetNextWindowSize(ImVec2(800, 500), ImGuiCond_FirstUseEver);
	if (!ImGui::Begin("Astralathe Settings", &bEnabled))
	{
		ImGui::End();
		return;
	}

	ImVec2 region = ImGui::GetContentRegionAvail();
	if (ImGui::BeginChild("##tabChild", ImVec2(region.x, region.y * 0.9f)))
	{
		if (!ImGui::BeginTabBar("##optionsTabBar"))
		{
			ImGui::End();
			return;
		}

	
		if (ImGui::BeginTabItem("Logging"))
		{
			if (ImGui::CollapsingHeader("Standard Logging", ImGuiTreeNodeFlags_DefaultOpen))
			{
				BoolBox(CONFIG_DEBUGOUTPUT, "Debug Logging");
			}
			if (ImGui::CollapsingHeader("Astralathe Debugging", ImGuiTreeNodeFlags_DefaultOpen))
			{
				BoolBox(CONFIG_ASTRALATHEOUTPUT, "Astralathe Output");
				GenericTooltip("Toggle logging produced by Astralathe itself\nDefault: True");
				BoolBox(CONFIG_VERBOSEHASHBASESPEW, "Verbose Hashbase Spew");
				GenericTooltip("Toggle verbose logging produced by Astralathe's hashbase hooks\nDefault: False");
				BoolBox(CONFIG_VERBOSEFILEMANAGER, "Verbose File Manager");
				GenericTooltip("Toggle verbose logging produced by Astralathe's file manager hooks\nDefault: False");
				//BoolBox(CONFIG_DEBUGLOADSINGLEMESH, "Verbose LoadSingleMesh");
				//BoolBox(CONFIG_DEBUGIMGUI, "Debug ImGui");
			}

			ImGui::EndTabItem();
		}

		if (ImGui::BeginTabItem("Debugging"))
		{
			BoolBox(CONFIG_DEBUGMENU, "Debug Menu");
			GenericTooltip("Allows access to the ingame debug menu by pressing F9 while the journal is open.\nDefault: True");
			BoolBox(CONFIG_LEVELSELECT, "Level Select");
			GenericTooltip("Toggle whether the game will boot into the debug level select by default\nDefault: False");
			BoolBox(CONFIG_CHEATKEYS, "Cheat Keys");
			GenericTooltip("Toggle whether cheats can be accessed via the F4-F8 keys by default\nDefault: False");

			ImGui::EndTabItem();
		}

		if (ImGui::BeginTabItem("Fixes/Enhancements"))
		{
			BoolBox(CONFIG_FIX_WORLDTOSCREEN, "World to Screen Fix");
			GenericTooltip("Fixes a bug that causes certain visual elements to appear misaligned at resolutions higher than 640x480.\nDefault: True");
			BoolBox(CONFIG_ENHANCEMENT_DISCORDRP, "Discord Rich Presence");
			GenericTooltip("Enables support for Discord Rich Presence.\nDefault: True");

			if (ImGui::CollapsingHeader("Widescreen", ImGuiTreeNodeFlags_DefaultOpen))
			{
				BoolBox(CONFIG_WIDESCREEN_AUTODETECT, "Auto-detect widescreen");
				GenericTooltip("Attempts to auto-detect when the game is in a widescreen resolution, removing the need to manually adjust the widescreen fix option.\nDefault: True.");
				ImGui::BeginDisabled(AstralatheConfig::GetConfigValue(CONFIG_WIDESCREEN_AUTODETECT));
				BoolBox(CONFIG_FIX_WIDESCREEN, "Widescreen fix");
				GenericTooltip("Applies various fixes to improve the game's widescreen support\nDefault: False.\nEnable only if you play the game in widescreen, or need to override the autodetect\nCannot be used when autodetect is enabled");
				ImGui::EndDisabled();
				ImGui::BeginDisabled(!AstralatheConfig::GetConfigValue(CONFIG_FIX_WIDESCREEN));
				FloatBox(CONFIG_WIDESCREEN_HUDOFFSET, "Widescreen HUD offset");
				GenericTooltip("Adjusts the offset of the HUD when playing with widescreen fixes enabled\nDefault: 100");
				ImGui::EndDisabled();
			}

			if (ImGui::CollapsingHeader("Effects", ImGuiTreeNodeFlags_DefaultOpen))
			{
				BoolBox(CONFIG_FIX_FX, "Visual Effects Fix");
				GenericTooltip("Fixes certain visual effects appearing blurry or pixelated. This can have an effect on performance in some cases, depending on your resolution.\nDefault: True");
				FloatBox(CONFIG_FIX_FX_SCALE, "Visual effects scale factor", 0.25f, 4);
				GenericTooltip("When \"Visual Effects Fix\" is enabled, scales the visual effect buffers by this factor. 1x matches your screen resolution. 2x is double, 0.5 is half, etc.\nLowering this can help with performance and increasing this will further increase effect clarity.\nDefault: 1");
			}

			ImGui::EndTabItem();
		}

		if (ImGui::BeginTabItem("Misc."))
		{
			BoolBox(CONFIG_USE_STEAM, "Use Steam");
			GenericTooltip("Toggles whether or not Steam will automatically register you as playing Psychonauts when using AstralatheLauncher. Default: True\nDisable this if you're worried about accidentally cheating achievements while playing.");
			BoolBox(CONFIG_AUTOHOOK, "Autohook");
			GenericTooltip("Toggles whether or not Astralathe will load automatically any time Psychonauts is started. Default: TruenNote that disabling this will require you to manually launch Astralathe via AstralatheLauncher in the game directory in the future!");
			ImGui::EndTabItem();
		}

		ImGui::EndTabBar();
	}
	ImGui::EndChild();

	if (CheckDirty())
	{
		ImGui::Text("One or more options have changed that require a game restart to take effect.");
	}

	if (ImGui::BeginPopupModal("Warning", nullptr, ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoResize))
	{
		ImVec2 avail = ImGui::GetContentRegionAvail();

		const char* warning1 = "Disabling autohook will require you to manually launch Astralathe via AstralatheLauncher in the game directory in the future!";
		const char* warning2 = "Are you sure you wish to disable it?";
		ImGui::SetCursorPosX(ImGui::GetCursorPosX() + (avail.x - ImGui::CalcTextSize(warning1).x) * 0.5f);
		ImGui::Text(warning1);
		ImGui::SetCursorPosX(ImGui::GetCursorPosX() + (avail.x - ImGui::CalcTextSize(warning2).x) * 0.5f);
		ImGui::Text(warning2);

		ImGui::Spacing();

		const char* yesText = "Yes";
		const char* noText = "No";

		float yesWidth = ImGui::CalcTextSize(yesText).x + ImGui::GetStyle().FramePadding.x * 2;;
		float noWidth = ImGui::CalcTextSize(noText).x + ImGui::GetStyle().FramePadding.x * 2;

		float totalWidth = yesWidth + noWidth + (ImGui::GetStyle().ItemSpacing.x + 4.0f * 2);

		ImGui::SetCursorPosX(ImGui::GetCursorPosX() + (avail.x - totalWidth) * 0.5f);

		bool clicked = false;
		if (ImGui::Button("Yes"))
		{
			clicked = true;
		}
		ImGui::SameLine(0.0f, ImGui::GetStyle().ItemSpacing.x + 4.0f);
		if (ImGui::Button("No"))
		{
			AstralatheConfig::SetConfigValue(CONFIG_AUTOHOOK, true);
			clicked = true;
		}

		if (clicked)
		{
			AstralatheConfig::Write();
			ImGui::CloseCurrentPopup();
		}

		ImGui::EndPopup();
	}

	if (ImGui::Button("Save"))
	{
		if (OldOptionStatesSet[CONFIG_AUTOHOOK])
		{
			if (OldOptionStates[CONFIG_AUTOHOOK].ValueBool != AstralatheConfig::GetConfigValue(CONFIG_AUTOHOOK))
			{
				if (!AstralatheConfig::GetConfigValue(CONFIG_AUTOHOOK))
				{
					ImGui::OpenPopup("Warning");
				}
			}
		}
		else
		{
			AstralatheConfig::Write();
		}
	}
	ImGui::End();
}
