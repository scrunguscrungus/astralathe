#include "JADS.h"

#include "Psychonauts/PsychoTypes.h"
#include "Psychonauts/Proxying/GameAppProxy.h"
#include "Psychonauts/Proxying/EEntityProxy.h"
#include "Psychonauts/Proxying/EEntityInfoProxy.h"
#include "Psychonauts/Proxying/EEntityManagerProxy.h"
#include "Psychonauts/Proxying/EEntityIterProxy.h"

#include "misc/cpp/imgui_stdlib.h"

#include "AstralatheImGui.h"
#include "Logging.h"

#include <format>

void JADS::Draw()
{
	if (ShouldDraw())
	{

		ImGui::SetNextWindowSize(ImVec2(500, 500), ImGuiCond_FirstUseEver);
		if (!ImGui::Begin("Astralathe Debug Scratchpad", &bEnabled))
		{
			ImGui::End();
			return;
		}

		if (!gEntities_Proxy)
		{
			ImGui::Text("No entities proxy :(");
			ImGui::End();
			return;
		}

		EArrayProxy<EEntityInfo> entArray = gEntities_Proxy->GetArrayProxy();
		ImGui::Text("Entity count: %i", entArray.Num());
		ImGui::Separator();
#if 0
		static int infoAbout = 0;
		ImGui::InputInt("Index", &infoAbout); ImGui::SameLine();
		if (ImGui::Button("Print entity info"))
		{
			if (gEntities_Proxy)
			{

				EEntityInfoProxy pEntInfo = EEntityInfoProxy(entArray[infoAbout]);
				EEntityProxy ent = EEntityProxy(pEntInfo.Entity.GetValue());

				LogMessage("Entity %i\n", infoAbout);
				LogMessage("Name: %s", ent.Name.GetValue());
			}
			else
			{
				LogMessage("NO ENTITIES PROXY???\n");
			}
		}
#endif

		if (ImGui::BeginChild("##entityReport", ImVec2(ImGui::GetContentRegionAvail().x, ImGui::GetContentRegionAvail().y)))
		{
			if (ImGui::BeginTable("##entList", 2))
			{
				EEntityIterProxy iterator(nullptr, true, false);
				int i = 0;
				while (iterator)
				{
					ImGui::TableNextColumn();
					ImGui::Text(iterator->Name.GetValue());
					ImGui::TableNextColumn();
					ImGui::Text(iterator->IsDomainSleeping() ? "Sleeping" : "Awake");

					i++;
					++iterator;
				}

				ImGui::EndTable();
			}
		}
		ImGui::EndChild();

		ImGui::End();
	}
}
