#include "DebugConsole.h"

#include "Psychonauts/PsychoTypes.h"

#include "misc/cpp/imgui_stdlib.h"

#include "AstralatheImGui.h"

#include <format>

int RunCallback(ImGuiInputTextCallbackData* data)
{
	return AstralatheImGui::GetConsole().ConsoleCallback(data);
}

void DebugConsole::Draw()
{
	if (ShouldDraw())
	{

		ImGui::SetNextWindowSize(ImVec2(500, 500), ImGuiCond_FirstUseEver);
		if (!ImGui::Begin("Console", &bEnabled))
		{
			ImGui::End();
			return;
		}

		if (ImGui::BeginPopup("OptionsPopup"))
		{
			ImGui::Checkbox(" Autoscroll", &bAutoscroll);
			GenericTooltip("Automatically scroll the console log output to the latest entry");

			ImGui::Text("Filtering:");

			ImGui::PushStyleColor(ImGuiCol_Text, LogColourAstralathe);
			ImGui::CheckboxFlags("Astralathe output", (int*)&currentFilter, (int)CLT_Astralathe);
			ImGui::PopStyleColor();
			GenericTooltip("Log text produced by Astralathe itself that is not part of the game");

			ImGui::PushStyleColor(ImGuiCol_Text, LogColourNormal);
			ImGui::CheckboxFlags("Psychonauts native output", (int*)&currentFilter, (int)CLT_Normal);
			ImGui::PopStyleColor();
			GenericTooltip("Psychonauts' native log output, produced by the game itself");

			ImGui::PushStyleColor(ImGuiCol_Text, LogColourST);
			ImGui::CheckboxFlags("Lua Stack Trace", (int*)&currentFilter, (int)CLT_StackTrace);
			ImGui::PopStyleColor();
			GenericTooltip("Lua code stack traces");

			ImGui::PushStyleColor(ImGuiCol_Text, LogColourError);
			ImGui::CheckboxFlags("Error", (int*)&currentFilter, (int)CLT_Error);
			ImGui::PopStyleColor();
			GenericTooltip("Errors");

			ImGui::EndPopup();
		}
		
		if (ImGui::Button("Options"))
		{
			ImGui::OpenPopup("OptionsPopup");
		}
		
		ImGui::Separator();

		std::string consoleBuffer;

		ImVec2 logSize = ImGui::GetContentRegionAvail();
		logSize.y *= 0.9f;
		if (ImGui::BeginChild("ConsoleLog", logSize))
		{
			ImGui::PushTextWrapPos();

			for (ConsoleEntry& item : ConsoleEntries)
			{
				if ((item.type & (currentFilter | CLT_UserEntry)) == 0)
				{
					continue;
				}
				ImVec4 col;
				switch (item.type)
				{
				case CLT_UserEntry:
					col = LogColourUser;
					break;
				case CLT_StackTrace:
					col = LogColourST;
					break;
				case CLT_Error:
					col = LogColourError;
					break;
				case CLT_Astralathe:
					col = LogColourAstralathe;
					break;
				case CLT_Normal:
				default:
					col = LogColourNormal;
					break;
				}
				ImGui::PushStyleColor(ImGuiCol_Text, col);
				ImGui::TextUnformatted(item.entryString.c_str());
				ImGui::PopStyleColor();
			}

			ImGui::PopTextWrapPos();
			if (bAutoscroll)
			{
				ImGui::SetScrollY(ImGui::GetScrollMaxY());
			}
		}
		ImGui::EndChild();
		ImGui::Separator();

		if (!bCommandMode)
		{
			ImGui::Text("Lua Input");
		}
		else
		{
			ImGui::Text("Debug Command");
		}
		ImGui::SameLine();
		ImGui::PushItemWidth(-1);
		if (ImGui::InputText("##luaConsoleInput",
			&consoleBuffer,
			ImGuiInputTextFlags_EnterReturnsTrue | ImGuiInputTextFlags_CallbackHistory |
			ImGuiInputTextFlags_CallbackCompletion,
			RunCallback))
		{
			if (!consoleBuffer.empty() && consoleBuffer.length() > 0 && consoleBuffer.at(0) != '\0')
			{
				AddConsoleHistory(consoleBuffer);
				if (currentConsoleHistoryIdx > -1)
				{
					currentConsoleHistoryIdx = ConsoleHistory.size() - 1;
				}
				LogPush(std::format("] {}", consoleBuffer), CLT_UserEntry);

				if (!bCommandMode)
				{
					lua_dobuffer(GetLuaState(), consoleBuffer.data(), consoleBuffer.length(), consoleBuffer.data());
				}
				else
				{
					std::string cmdString = std::format("DebugCommand(\"{}\")", consoleBuffer);
					lua_dobuffer(GetLuaState(), cmdString.data(), cmdString.length(), cmdString.data());
				}
				consoleBuffer.clear();
				bShouldFocusTextBoxNow = true;
			}
		}
		if (bShouldFocusTextBoxNow)
		{
			ImGui::SetKeyboardFocusHere(-1);
			bShouldFocusTextBoxNow = false;
		}
		ImGui::PopItemWidth();

		ImGui::End();
	}

}

void DebugConsole::OnJustShown()
{
	bShouldFocusTextBoxNow = true;
}

void DebugConsole::OnPreClose()
{
}

void DebugConsole::LogAstralatheMessage(std::string msg)
{
	LogPush(msg, CLT_Astralathe);
}

void DebugConsole::LogPush(std::string newEntry, ConsoleLogType type)
{
	ConsoleEntry entry(newEntry, type);

	ConsoleEntries.push_back(entry);
	if (ConsoleEntries.size() > 1000)
	{
		ConsoleEntries.erase(ConsoleEntries.begin());
	}
}

void DebugConsole::Init()
{
	LogColourNormal = ImGui::GetStyle().Colors[ImGuiCol_Text];
	LogColourUser = ImVec4(LogColourNormal.x * 0.5f, LogColourNormal.y * 0.5f, LogColourNormal.z * 0.5f, LogColourNormal.w);
	LogColourST = ImVec4(50.f / 255.f, 153.f / 255.f, 255.f / 255.f, 1.0f);
	LogColourError = ImVec4(1.0f, 0.1f, 0.1f, 1.0f);
	LogColourAstralathe = ImVec4(198.f / 255.f, 181.f / 255.f, 1.0f, 1.0f);
}

void DebugConsole::AddConsoleHistory(std::string newEntry)
{
	if (ConsoleHistory.size() > 0)
	{
		ConsoleHistory.pop_back();
	}
	ConsoleHistory.push_back(newEntry);
	if (ConsoleHistory.size() > 32)
	{
		ConsoleHistory.erase(ConsoleHistory.begin());
	}
	ConsoleHistory.push_back("");
}

int DebugConsole::ConsoleCallback(ImGuiInputTextCallbackData* data)
{
	if (data->EventFlag == ImGuiInputTextFlags_CallbackHistory)
	{
		if (ConsoleHistory.size() < 1)
		{
			return 0;
		}

		if (currentConsoleHistoryIdx < 0)
		{
			currentConsoleHistoryIdx = ConsoleHistory.size() - 1;
		}

		if (data->EventKey == ImGuiKey_UpArrow)
		{
			currentConsoleHistoryIdx--;
		}
		else
		{
			currentConsoleHistoryIdx++;
		}
		if (currentConsoleHistoryIdx < 0)
			currentConsoleHistoryIdx = ConsoleHistory.size() - 1;
		else if ((size_t)currentConsoleHistoryIdx > ConsoleHistory.size() - 1)
			currentConsoleHistoryIdx = 0;

		data->DeleteChars(0, data->BufTextLen);

		data->InsertChars(data->CursorPos, ConsoleHistory[currentConsoleHistoryIdx].data());
	}
	else if (data->EventFlag == ImGuiInputTextFlags_CallbackCompletion)
	{
		bCommandMode = !bCommandMode;
	}

	return 0;
}
