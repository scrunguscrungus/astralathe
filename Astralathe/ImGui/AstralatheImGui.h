#ifndef ASTRALATHE_IMGUI_H
#define ASTRALATHE_IMGUI_H

#ifdef USE_IMGUI
#include "Singleton.h"

#include "SettingsMenu.h"
#include "DebugConsole.h"
#include "ModList.h"
#include "JADS.h"


class AstralatheImGui : public Singleton<AstralatheImGui>
{
private:
	bool m_bJustShown = false;

	SettingsMenu m_Settings;
	DebugConsole m_Console;
	ModList m_ModList;
	JADS m_JADS;

public:

	void Draw();

	void OnImGuiReady();
	void OnJustShown();
	void OnPreClose();

	static DebugConsole& GetConsole() { return Get().m_Console; }
};

#else
enum ConsoleLogType
{
	None = 0,
	CLT_UserEntry = 1 << 0,
	CLT_Normal = 1 << 1,
	CLT_StackTrace = 1 << 2,
	CLT_Error = 1 << 3,
	CLT_Astralathe = 1 << 4,
};
#endif

void LogToConsole(std::string newEntry, ConsoleLogType type = CLT_Normal);
#endif