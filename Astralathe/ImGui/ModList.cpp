#include "ModList.h"
#include "ModFS.h"
#include "misc/cpp/imgui_stdlib.h"

std::vector<std::string> modList = { "My cool mod's dependency", "SomeLib", "SomeDisabledLib", "My cool mod", "OtherDependency", "real beta mod", "bepis", "The j"};

void ModList::DrawDependency(const char* text, ModDependency::VersionCheckResult status)
{
	//0 - Good
	//1 - Loaded but in wrong order
	//2 - Loaded but wrong version
	//3 - In mod list but disabled
	//4 - Not loaded
	std::string tooltipText;
	if (status == ModDependency::VersionCheckResult::GOOD)
	{
		ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(20.f / 255.f, 150.f / 255.f, 20.f / 255.f, 1.0f));
	}
	else if (status == ModDependency::VersionCheckResult::BADORDER)
	{
		tooltipText = "This dependency is in the mod list but is not loaded before this mod. Please adjust the load order or use auto-sort.";
		ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(255.f / 255.f, 233.f / 255.f, 58.f / 255.f, 1.0f));
	}
	else if (status == ModDependency::VersionCheckResult::BADVERSION)
	{
		//tooltipText = "This dependency is in the mod list and loaded in the correct order but does not meet the version requirement. It may or may not function.\n\nHave version: 1.2.3\nRequire version: >=1.2.4";
		tooltipText = "This dependency is in the mod list and loaded in the correct order but does not meet the version requirement. It may or may not function.";
		ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(255.f / 255.f, 233.f / 255.f, 58.f / 255.f, 1.0f));
	}
	else if (status == ModDependency::VersionCheckResult::DISABLED)
	{
		tooltipText = "This dependency is in the mod list but is not enabled!";
		ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(255.f / 255.f, 49.f / 255.f, 38.f / 255.f, 1.0f));
	}
	else if (status == ModDependency::VersionCheckResult::MISSING)
	{
		tooltipText = "This dependency could not be found!";
		ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(255.f / 255.f, 49.f / 255.f, 38.f / 255.f, 1.0f));
	}

	ImGui::TextUnformatted(text);
	ImGui::PopStyleColor();
	if (!tooltipText.empty())
	{
		GenericTooltip(tooltipText.c_str());
	}
}

void ModList::UpdateEnabledStates()
{
	for (ModData* data : g_pModFS->LoadOrder)
	{
		data->Loaded = data->Loaded && data->CheckDependencies() < ModDependency::VersionCheckResult::BADORDER;
	}
}

void ModList::RevertLoadOrder()
{
	if (LastKnownLoadOrder.size() != g_pModFS->LoadOrder.size())
	{
		//??
		LogMessage("%i %i", LastKnownLoadOrder.size(), g_pModFS->LoadOrder.size());
		LogMessage("Load order size mismatch? This shouldn't happen! Tell Jill!\n");
		return;
	}
	g_pModFS->LoadOrder.clear();
	for (size_t i = 0; ModData* mod : LastKnownLoadOrder)
	{
		LogMessage("%s - %s - %s\n", mod->Name.c_str(), mod->Loaded ? "true" : "false", LastKnownStates[i] ? "true" : "false");
		g_pModFS->LoadOrder.push_back(mod);
		mod->Loaded = LastKnownStates[i];
		i++;
	}
	Dirty = false;
}

void ModList::UpdateLastKnownLoadOrder()
{
	LogMessage("Updating last known load order...\n");
	LastKnownLoadOrder.clear();
	LastKnownStates.clear();
	for (ModData* mod : g_pModFS->LoadOrder)
	{
		LogMessage("%s - %s\n", mod->Name.c_str(), mod->Loaded ? "true" : "false");
		LastKnownLoadOrder.push_back(mod);
		LastKnownStates.push_back(mod->Loaded);
	}
	Dirty = false;
}

void ModList::CheckDirtiness()
{
	if (LastKnownLoadOrder.size() != g_pModFS->LoadOrder.size())
	{
		//??
		Dirty = true;
		return;
	}
	for (size_t i = 0; ModData* mod : g_pModFS->LoadOrder)
	{
		if (mod != LastKnownLoadOrder[i] || mod->Loaded != LastKnownStates[i])
		{
			Dirty = true;
			return;
		}
		i++;
	}
	Dirty = false;
}

void ModList::Draw()
{
	const float BUTTON_SPACING = 4.0f;
	
	if (ImGui::BeginPopupModal("Unsaved Changes", nullptr, ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoResize))
	{
		ImVec2 avail = ImGui::GetContentRegionAvail();

		const char* warning1 = "You have unsaved changes to your load order!";
		const char* warning2 = "Are you sure you want to close the mod list? Your changes will be lost upon closing the game.";
		ImGui::SetCursorPosX(ImGui::GetCursorPosX() + (avail.x - ImGui::CalcTextSize(warning1).x) * 0.5f);
		ImGui::Text(warning1);
		ImGui::SetCursorPosX(ImGui::GetCursorPosX() + (avail.x - ImGui::CalcTextSize(warning2).x) * 0.5f);
		ImGui::Text(warning2);

		ImGui::Spacing();

		const char* yesText = "Save and close";
		const char* noText = "Don't close";
		const char* closeText = "Close without saving";

		float yesWidth = ImGui::CalcTextSize(yesText).x + ImGui::GetStyle().FramePadding.x * 2;;
		float noWidth = ImGui::CalcTextSize(noText).x + ImGui::GetStyle().FramePadding.x * 2;
		float closeWidth = ImGui::CalcTextSize(closeText).x + ImGui::GetStyle().FramePadding.x * 2;

		float totalWidth = yesWidth + noWidth + closeWidth + (ImGui::GetStyle().ItemSpacing.x + BUTTON_SPACING * 2);

		ImGui::SetCursorPosX(ImGui::GetCursorPosX() + (avail.x - totalWidth) * 0.5f);

		if (ImGui::Button(yesText))
		{
			g_pModFS->WriteModLoadOrder();
			UpdateLastKnownLoadOrder();
			CloseOkay = true;
			ImGui::CloseCurrentPopup();
		}

		ImGui::SameLine(0.0f, ImGui::GetStyle().ItemSpacing.x + BUTTON_SPACING);

		if (ImGui::Button(noText))
		{
			bEnabled = true;
			ImGui::CloseCurrentPopup();
		}

		ImGui::SameLine(0.0f, ImGui::GetStyle().ItemSpacing.x + BUTTON_SPACING);

		if (ImGui::Button(closeText))
		{
			CloseOkay = true;
			ImGui::CloseCurrentPopup();
		}

		ImGui::EndPopup();
	}

	if (!ShouldDraw())
	{
		if (IsDirty() && !CloseOkay && !ImGui::IsPopupOpen("Unsaved Changes"))
		{
			ImGui::OpenPopup("Unsaved Changes");
		}
	}
	else
	{
		CloseOkay = false;
	}

	if (ShouldDraw() || ImGui::IsPopupOpen("Unsaved Changes"))
	{
		if (FirstOpen)
		{
			UpdateLastKnownLoadOrder();
			FirstOpen = false;
		}

		ImGui::SetNextWindowSize(ImVec2(500, 500), ImGuiCond_FirstUseEver);
		const char* windowTitle = "Mod List";
		const char* windowTitleDirty = "Mod List*";
		char title[128];
		memset(title, 0, 128);
		snprintf(title, 128, "%s###modList", Dirty ? windowTitleDirty : windowTitle);
		if (!ImGui::Begin(title, &bEnabled))
		{
			ImGui::End();
			return;
		}

		if (ImGui::BeginTable("ModListLayoutTable", 2))
		{
			ImGui::TableNextRow();
			ImGui::TableNextColumn();

			ImGui::Text("Load order"); ImGui::SameLine();
			ImGui::TextDisabled("(?)");
			if (ImGui::BeginItemTooltip())
			{
				ImGui::PushTextWrapPos(ImGui::GetFontSize() * 35.0f);
				ImGui::TextUnformatted("Load order determines which mod's files will take priority when multiple mods overwrite the same files.\n\nMods at the top are loaded first and have the lowest priority while mods at the bottom are loaded last and have the highest.");
				ImGui::PopTextWrapPos();
				ImGui::EndTooltip();
			}
			ImGui::Spacing();
			ImGui::Spacing();
			static int curSelection = 0;
			if (ImGui::BeginListBox("Detected Mods", ImVec2(-FLT_MIN, ImGui::GetContentRegionAvail().y)))
			{
				//for (unsigned int i = 0; i < modList.size(); i++)
				int i = 0;
				for ( const auto& mod : g_pModFS->LoadOrder )
				{
					//ModData data = mod.second;
					const bool selected = (curSelection == i);
					ImGui::PushStyleColor(ImGuiCol_Text, mod->Loaded ? ImGui::GetStyle().Colors[ImGuiCol_Text] : ImGui::GetStyle().Colors[ImGuiCol_TextDisabled]);
					if (ImGui::Selectable(mod->Name.c_str(), selected))
					{
						curSelection = i;
					}
					ImGui::PopStyleColor();

					if (selected)
						ImGui::SetItemDefaultFocus();
					i++;
				}
				ImGui::EndListBox();
			}

			ImGui::TableNextColumn();

			if (ImGui::BeginChild("Mod Info Pane", ImVec2(-FLT_MIN, ImGui::GetContentRegionAvail().y)))
			{
				if (g_pModFS->LoadOrder.size() < 1)
				{
					ImGui::Text("No mods loaded");
				}
				else
				{
					ImGui::BeginDisabled(curSelection == 0);
					if (ImGui::Button("Move Up"))
					{
						std::swap(g_pModFS->LoadOrder[curSelection], g_pModFS->LoadOrder[curSelection - 1]);
						UpdateEnabledStates();
						CheckDirtiness();
						curSelection--;
					}
					GenericTooltip("Move mod up in the load order (lower priority)");
					ImGui::SameLine();
					ImGui::EndDisabled();

					ModDependency::VersionCheckResult dependStatus = g_pModFS->LoadOrder[curSelection]->CheckDependencies();
					bool curIsGood = dependStatus == ModDependency::VersionCheckResult::GOOD;
					ImGui::BeginDisabled(!curIsGood);
					if (ImGui::Button("Toggle"))
					{
						g_pModFS->LoadOrder[curSelection]->Loaded = !g_pModFS->LoadOrder[curSelection]->Loaded;
						UpdateEnabledStates();
						CheckDirtiness();
					}
					ImGui::EndDisabled();
					if (curIsGood)
					{
						GenericTooltip("Toggle whether the mod is enabled or disabled");
					}
					else
					{
						static const char* errorResults[] = {
							"A hard dependency is missing.",
							"A hard dependency is disabled.",
							"A hard dependency is being loaded in the wrong order.",
						};
						int errorString;
						switch (dependStatus)
						{
						case ModDependency::VersionCheckResult::BADORDER:
							errorString = 2;
							break;
						case ModDependency::VersionCheckResult::MISSING:
							errorString = 0;
							break;
						case ModDependency::VersionCheckResult::DISABLED:
							errorString = 1;
							break;
						}
						std::string tooltopString = std::format("This mod cannot be enabled: {}", errorResults[errorString]);
						GenericTooltip(tooltopString.c_str());
					}
					ImGui::SameLine();

					ImGui::BeginDisabled(curSelection == g_pModFS->LoadOrder.size() - 1);
					if (ImGui::Button("Move Down"))
					{
						std::swap(g_pModFS->LoadOrder[curSelection], g_pModFS->LoadOrder[curSelection + 1]);
						UpdateEnabledStates();
						CheckDirtiness();
						curSelection++;
					}
					GenericTooltip("Move mod down in the load order (higher priority)");
					ImGui::SameLine();
					ImGui::EndDisabled();

					ModData* curMod = g_pModFS->LoadOrder[curSelection];
					ImGui::Separator();
					ImGui::PushTextWrapPos();
					ImGui::Text("Status: %s", curMod->Loaded ? "Enabled" : "Disabled");
					ImGui::PopTextWrapPos();
					ImGui::Spacing();
					ImGui::PushTextWrapPos();
					ImGui::Text("ID: %s", curMod->ID.c_str());
					ImGui::PopTextWrapPos();
					ImGui::Spacing();
					ImGui::PushTextWrapPos();
					ImGui::Text("Name: %s", curMod->Name.c_str());
					ImGui::PopTextWrapPos();
					ImGui::Spacing();
					ImGui::Text("Version: %s", curMod->Version.GetString().c_str());
					ImGui::Spacing();
					ImGui::PushTextWrapPos();
					ImGui::Text("Author(s):\n%s", curMod->Author.c_str());
					ImGui::PopTextWrapPos();
					ImGui::Spacing();
					ImGui::PushTextWrapPos();
					ImGui::Text("Description:\n%s", curMod->Description.c_str());
					ImGui::PopTextWrapPos();
					ImGui::Spacing();
					ImGui::PushTextWrapPos();
					ImGui::Text("Dependencies:\n");
					for (const ModDependency& dep : curMod->Dependencies)
					{
						DrawDependency(dep.ToString().c_str(), dep.GetStatus(curMod->ID));
					}
					if (curMod->Dependencies.size() < 1)
					{
						ImGui::Text("None");
					}
					ImGui::PopTextWrapPos();	
				}

				if (ImGui::BeginPopupModal("Auto-Sort Warning", nullptr, ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoResize))
				{
					ImVec2 avail = ImGui::GetContentRegionAvail();

					const char* warning1 = "Auto-sort will completely re-arrange the ordering of your mod list.";
					const char* warning2 = "It is somewhat naive, and is not guaranteed to accurately sort mods in every case!";
					ImGui::SetCursorPosX(ImGui::GetCursorPosX() + (avail.x - ImGui::CalcTextSize(warning1).x) * 0.5f);
					ImGui::Text(warning1);
					ImGui::SetCursorPosX(ImGui::GetCursorPosX() + (avail.x - ImGui::CalcTextSize(warning2).x) * 0.5f);
					ImGui::Text(warning2);

					ImGui::Spacing();

					const char* yesText = "Got it, sort away";
					const char* noText = "Never mind!";

					float yesWidth = ImGui::CalcTextSize(yesText).x + ImGui::GetStyle().FramePadding.x * 2;;
					float noWidth = ImGui::CalcTextSize(noText).x + ImGui::GetStyle().FramePadding.x * 2;

					float totalWidth = yesWidth + noWidth + (ImGui::GetStyle().ItemSpacing.x + BUTTON_SPACING);

					ImGui::SetCursorPosX(ImGui::GetCursorPosX() + (avail.x - totalWidth) * 0.5f);

					if (ImGui::Button(noText))
					{
						ImGui::CloseCurrentPopup();
					}

					ImGui::SameLine(0.0f, ImGui::GetStyle().ItemSpacing.x + BUTTON_SPACING);

					if (ImGui::Button(yesText))
					{
						g_pModFS->RunAutoSort();
						CheckDirtiness();
						ImGui::CloseCurrentPopup();
					}

					ImGui::EndPopup();
				}

				ImVec2 avail = ImGui::GetContentRegionAvail();

				float saveWidth = ImGui::CalcTextSize("Save").x + ImGui::GetStyle().FramePadding.x * 2;;
				float autoSortWidth = ImGui::CalcTextSize("Auto-sort").x + ImGui::GetStyle().FramePadding.x * 2;
				float revertWidth = ImGui::CalcTextSize("Auto-sort").x + ImGui::GetStyle().FramePadding.x * 2;
				float totalWidth = saveWidth + autoSortWidth + revertWidth + (ImGui::GetStyle().ItemSpacing.x + (BUTTON_SPACING*2));

				ImGui::SetCursorPosY(ImGui::GetCursorPosY() + avail.y - ImGui::GetFrameHeight());
				ImGui::SetCursorPosX(ImGui::GetCursorPosX() + (avail.x - totalWidth));

				ImGui::BeginDisabled(!IsDirty());
				if (ImGui::Button("Revert"))
				{
					RevertLoadOrder();
				}
				ImGui::EndDisabled();
				ImGui::SameLine(0.0f, ImGui::GetStyle().ItemSpacing.x + BUTTON_SPACING);
				if (ImGui::Button("Auto-sort"))
				{
					ImGui::OpenPopup("Auto-Sort Warning", ImGuiPopupFlags_NoOpenOverExistingPopup);
				}
				ImGui::SameLine(0.0f, ImGui::GetStyle().ItemSpacing.x + BUTTON_SPACING);
				ImGui::BeginDisabled(!IsDirty());
				if (ImGui::Button("Save"))
				{
					g_pModFS->WriteModLoadOrder();
					UpdateLastKnownLoadOrder();
				}
				ImGui::EndDisabled();
				ImGui::EndChild();
			}

			ImGui::EndTable();
		}
		ImGui::End();
	}
}
