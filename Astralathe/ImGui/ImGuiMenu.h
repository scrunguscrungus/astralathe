#pragma once

#include "imgui.h"

class ImGuiMenu abstract
{
public:
	bool bEnabled;

	virtual void Init() {};

	virtual void Draw() = 0;
	virtual bool ShouldDraw() { return bEnabled; }

	virtual void OnJustShown() {};
	virtual void OnPreClose() {};

	void GenericTooltip(const char* msg)
	{
		if (ImGui::BeginItemTooltip())
		{
			ImGui::PushTextWrapPos(ImGui::GetFontSize() * 35.0f);
			ImGui::TextUnformatted(msg);
			ImGui::PopTextWrapPos();
			ImGui::EndTooltip();
		}
	}
	
};