#include "Logging.h"
#include "AstralatheImGui.h"
#ifdef USE_IMGUI
#include "AstralatheConfig.h"
#include "ModFS.h"
#include "Psychonauts/PsychoTypes.h"

#include "AL_VERSION.h"
#include "imgui.h"
#include "misc/cpp/imgui_stdlib.h"

#endif
void LogToConsole(std::string newEntry, ConsoleLogType type)
{
#ifdef USE_IMGUI
	AstralatheImGui::GetConsole().LogPush(newEntry, type);
#endif
}

#ifdef USE_IMGUI
void AstralatheImGui::OnImGuiReady()
{
	m_Console.Init();
	m_ModList.Init();
}

void AstralatheImGui::Draw()
{
	ImGui::NewFrame();

	if (ImGui::BeginMainMenuBar())
	{
		ImGui::MenuItem("Mod List", nullptr, &m_ModList.bEnabled);
		ImGui::MenuItem("Settings", nullptr, &m_Settings.bEnabled);
		if (ImGui::BeginMenu("Developer Tools"))
		{
			ImGui::MenuItem("Console", nullptr, &m_Console.bEnabled);
#if DEBUG
			ImGui::MenuItem("Scratchpad", nullptr, &m_JADS.bEnabled);
#endif

			ImGui::EndMenu();
		}

		const char* VERSION_TEXT = "Astralathe v%s built " __DATE__;
		char vText[128];
		memset(vText, 0, 128);
		sprintf_s(vText, 128, VERSION_TEXT, AstralatheVersion::CURRENT_VERSION.GetString().c_str());
		ImGui::SameLine(ImGui::GetContentRegionAvail().x - (ImGui::CalcTextSize(vText).x), 0.0);
		ImGui::Text(vText);

		ImGui::EndMainMenuBar();
	}

	if (m_bJustShown && m_Console.bEnabled)
	{
		m_Console.OnJustShown();
	}

	m_bJustShown = false;

	m_Settings.Draw();
	m_ModList.Draw();
	m_Console.Draw();
	m_JADS.Draw();

	ImGui::EndFrame();

	ImGui::Render();
}
void AstralatheImGui::OnJustShown()
{
	m_bJustShown = true;
}
void AstralatheImGui::OnPreClose()
{
	m_ModList.OnPreClose();
	m_Console.OnPreClose();
}

#endif