#pragma once

#include "Logging.h"

#include "ImGuiMenu.h"
#include "imgui.h"

#include <vector>
#include <cstdlib>
#include <string>

enum ConsoleLogType
{
	None = 0,
	CLT_UserEntry = 1 << 0,
	CLT_Normal = 1 << 1,
	CLT_StackTrace = 1 << 2,
	CLT_Error = 1 << 3,
	CLT_Astralathe = 1 << 4,
};

struct ConsoleEntry
{
public:
	std::string entryString;
	ConsoleLogType type;
};

class DebugConsole : public ImGuiMenu, public ILogger
{
private:
	ImVec4 LogColourNormal;
	ImVec4 LogColourUser;
	ImVec4 LogColourST;
	ImVec4 LogColourError;
	ImVec4 LogColourAstralathe;

	int currentConsoleHistoryIdx = -1;
	std::vector<std::string> ConsoleHistory;
	std::vector<ConsoleEntry> ConsoleEntries;

	ConsoleLogType currentFilter = (ConsoleLogType)(CLT_Normal | CLT_StackTrace | CLT_Error);

	bool bAutoscroll = true;
	bool bCommandMode = false;

	bool bShouldFocusTextBoxNow = false;

	void AddConsoleHistory(std::string newEntry);

public:
	int ConsoleCallback(ImGuiInputTextCallbackData* data);

	void LogPush(std::string newEntry, ConsoleLogType type);

	virtual void Init() override;

	virtual void Draw() override;

	virtual void OnJustShown() override;
	virtual void OnPreClose() override;

	virtual void LogAstralatheMessage(std::string msg) override;
};