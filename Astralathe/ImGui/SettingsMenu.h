#pragma once

#include "ImGuiMenu.h"
#include "AstralatheConfig.h"

struct OptionState
{
	bool isBool;
	bool ValueBool;
	float ValueFloat;
};

//In-game config
class SettingsMenu : public ImGuiMenu
{
private:
	bool OldOptionStatesSet[CONFIG_COUNT];
	OptionState OldOptionStates[CONFIG_COUNT];
	bool NotifyRestart = false;

	bool CheckDirty();
	void BoolBox(ConfigOption option, const char* label);
	void FloatBox(ConfigOption option, const char* label, float min=-FLT_MAX, float max=FLT_MAX);
public:
	virtual void Draw() override;
};
