#pragma once

#include "ImGuiMenu.h"

//JADS (Jill's Astralathe Debug Scratchpad)
class JADS : public ImGuiMenu
{
public:
	virtual void Draw() override;
};