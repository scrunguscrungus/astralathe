#pragma once

#include "Logging.h"

#include "ImGuiMenu.h"
#include "imgui.h"

#include "ModData.h"

#include <vector>
#include <cstdlib>
#include <string>


class ModList : public ImGuiMenu
{
private:
	std::vector<bool> LastKnownStates;
	std::vector<ModData*> LastKnownLoadOrder;

	void DependencyTooltip(const char* text);
	void DrawDependency(const char* text, ModDependency::VersionCheckResult status);
	void UpdateEnabledStates();

	void RevertLoadOrder();
	void UpdateLastKnownLoadOrder();
	void CheckDirtiness();
	bool IsDirty() { return Dirty; }

	bool Dirty = false;
	bool CloseOkay = false;
	bool FirstOpen = true;
public:
	virtual void Draw() override;

	virtual bool ShouldDraw() override { return bEnabled; }
};