#include "ModData.h"
#include "ModFS.h"

ModDependency::VersionCheckResult ModDependency::GetStatus(std::string dependent) const
{
	auto it = g_pModFS->AllModDatas.find(ID);
	if (it == g_pModFS->AllModDatas.end())
	{
		return MISSING;
	}

	//The loaded version of this dependency
	ModData* data = &it->second;
	if (!data->Loaded)
	{
		return DISABLED;
	}

	ModData* dependentData = &g_pModFS->AllModDatas[dependent];

	auto us = std::find(g_pModFS->LoadOrder.begin(), g_pModFS->LoadOrder.end(), data);
	auto them = std::find(g_pModFS->LoadOrder.begin(), g_pModFS->LoadOrder.end(), dependentData);

	if (us > them)
	{
		return BADORDER;
	}

	switch (VersionReq) {
	case EQUAL:
		if (data->Version != Version) return BADVERSION;
		break;
	case GREATER:
		if (data->Version <= Version) return BADVERSION;
		break;
	case LESS:
		if (data->Version >= Version) return BADVERSION;
		break;
	case GEQ:
		if (data->Version < Version) return BADVERSION;
		break;
	case LEQ:
		if (data->Version > Version) return BADVERSION;
		break;
	}

	return GOOD;
}

std::string ModDependency::ToString() const
{
	std::string name = ID;
	auto it = g_pModFS->AllModDatas.find(name);
	if ( it != g_pModFS->AllModDatas.end())	
	{
		name = it->second.Name;
	}
	return std::format("{}{}{}", name, VerReqString(), Version.GetString());
}
