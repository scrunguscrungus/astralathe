#pragma once

#include <string>

class GameApp;
struct DiscordRichPresence;

constexpr const char* pszDiscordApplicationID = "1282222880427479051";

class DiscordManager
{
	const char* appID;
	bool m_bDisablePresence = false;

public:
	DiscordManager(const char* ID);
	~DiscordManager();

	void Startup();
	void Shutdown();

	void Enable() {
		m_bDisablePresence = false;
	};
	void Disable() {
		m_bDisablePresence = true;
	}

	void ConfigUpdated();
	bool HandleSpecialCase(std::string levelName, std::string presenceString, DiscordRichPresence* presence);
	void SetRichPresence(GameApp* pGameApp, std::string presenceString);
};