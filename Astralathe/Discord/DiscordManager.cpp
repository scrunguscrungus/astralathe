/*
* Discord rich presence module for Astralathe
* 
* To use this, build with USE_DISCORD defined. Or undefine USE_DISCORD to build without Discord rich presence support.
*/

#include <algorithm>
#include <string>
#include <format>
#include <memory>
#include "Logging.h"
#include "DiscordManager.h"

#ifdef USE_DISCORD

#ifndef _DEBUG
#pragma comment(lib, "thirdparty/discord/lib/discord-rpc.lib")
#else
#pragma comment(lib, "thirdparty/discord/lib/discord-rpc-debug.lib")
#endif

#include "thirdparty/discord/include/discord_rpc.h"
#include "Psychonauts/Proxying/GameAppProxy.h"
#endif

DiscordManager::DiscordManager(const char* ID)
	: appID(ID)
{
}

DiscordManager::~DiscordManager()
{
	Shutdown();
}

void DiscordManager::Startup()
{
#ifdef USE_DISCORD
	if (m_bDisablePresence)
		return;

	DiscordEventHandlers handlers;
	memset(&handlers, 0, sizeof(handlers));

	LogMessage("Initialising Discord...\n");
	Discord_Initialize(appID, &handlers, 1, "3830");
	LogMessage("Done!\n");
#endif
}

void DiscordManager::Shutdown()
{
#ifdef USE_DISCORD
	Discord_Shutdown();
#endif
}

void DiscordManager::ConfigUpdated()
{
	//TODO: This will be merged when we have our in-game config menu up and running
#ifdef USE_DISCORD

#endif
}

bool DiscordManager::HandleSpecialCase(std::string levelName, std::string presenceString, DiscordRichPresence* presence)
{
	if (levelName == "stmu")
	{
		presence->largeImageKey = "stmu";
		presence->details = "Start Menu";
		return true;
	}
	
	if (presenceString == "LevelSelect")
	{
		presence->largeImageKey = "levelselect";
		presence->details = "Level Select";
		return true;
	}

	return false;
}

void DiscordManager::SetRichPresence(GameApp* pGameApp, std::string presenceString)
{
#ifdef USE_DISCORD
	if (m_bDisablePresence)
		return;

	if (!pGameApp)
		return;

	GameAppProxy gameApp(pGameApp);

	std::string levelName = "";
	char* pszLevelName = gameApp.m_pszCurrentLevelFull.GetPointer();
	if (pszLevelName != nullptr)
	{
		levelName = std::format("{}", pszLevelName);
		std::transform(levelName.begin(), levelName.end(), levelName.data(), [](unsigned char c) {return std::tolower(c); });
	}

	DiscordRichPresence presence;
	memset(&presence, 0, sizeof(presence));

	std::string topText;
	std::string bottomText;
	LogMessage("Updating rich presence: %s\n", presenceString.c_str());
	if (!HandleSpecialCase(levelName, presenceString, &presence))
	{
		if (presenceString.empty() || m_bDisablePresence)
		{
			Discord_ClearPresence();
			return;
		}

		if (pszLevelName != nullptr)
		{
			presence.largeImageKey = levelName.c_str();
		}

		//This is a bit of a hack, the vanilla game sends rich presence strings in the form of
		//[level]: [subarea]
		//which we want to split into the top and bottom text for Discord.

		size_t delimiterPos = presenceString.find(": ");
		if (delimiterPos == std::string::npos)
		{
			topText = presenceString;
			bottomText = "";
		}
		else
		{
			topText = presenceString.substr(0, delimiterPos);
			bottomText = presenceString.erase(0, delimiterPos + 2);
		}

		if (topText == "Nightmare in the Brain Tumbler")
		{
			topText = "Brain Tumbler Experiment";
		}
		presence.details = topText.c_str();
		presence.state = bottomText.c_str();
	}

	Discord_UpdatePresence(&presence);
#endif
}