#ifndef MOD_MOUNTING_H
#define MOD_MOUNTING_H

#include <vector>
#include <map>
#include <unordered_map>
#include <string>
#include <filesystem>
#ifdef _WIN32
#include <Windows.h>
#endif
#include "ModData.h"

class ModFile
{
public:
	//TODO: Would this be useful?
	//ModData* pOwningMod;
	int iSize;
	char pszName[ MAX_PATH ];
	char* pData;

	~ModFile()
	{
		if ( pData )
		{
			delete pData;
		}
	}

	void SetSize(int size)
	{
		iSize = size;

		if ( pData )
		{
			delete pData;
		}

		pData = new char[ iSize ]();
	}
};

//TODO: Update all of this awful bullshit to use std::path

class ModFS
{
private:
	bool TryLoadModJSON(std::filesystem::directory_entry dir, std::string modName, std::filesystem::path modJsonPath);
	void LoadAllModData();
	void InitLoadOrder();

	bool widescreenMounted = false;

public:
	std::unordered_map<std::string, std::string> AllModsByFolder; //Maps all folder names to mod IDs
	std::unordered_map<std::string, std::string> AllFoldersByMod; //Maps mod IDs to their on-disk folder names

	std::unordered_map<std::string, ModData> AllModDatas; //Maps mod IDs for all mods to ModData
	std::vector<ModData*> LoadOrder; //Ordered list of every mod, in the desired order.

	class IForEachInSearchPath
	{
	public:
		virtual void Operate( std::filesystem::path Path ) = 0;
	};

	//Inits ModFS (Inits PHYSFS, sets up base+write dir, calls MountModPaths())
	static bool Init();

	void RunAutoSort();
	void WriteModLoadOrder();

	//Reads mods from astralathe_mods.txt and mounts them
	void MountModPaths();

	void FixSlashes( const char* pszFilename, char pBuf[MAX_PATH] );

	//Check if file exists in any mods
	bool ModFileExists( const char* pszFilename );

	//Check if the file exists in native_assets (DoFile workaround)
	//This should ONLY be used in the case that ModFileExists has already been checked
	//and returned false.
	//Because this function uses a hack and just appends native_assets to the search
	//path temporarily. Meaning if any mods override the native asset, those end up
	//being returned first.
	//Same goes for the bIncludeNativeAssets param on LoadFile.
	bool NativeAssetExists(const char* pszFilename);

	//Loads data from mod file pszFilename into pBuf, returns true on success
	bool LoadFile(const char* pszFilename, void* pBuf);
	//Loads data from mod file pszFilename into the ModFile struct modFile, returns true on success
	bool LoadFile(const char* pszFilename, ModFile* modFile, bool bIncludeNativeAssets = false);

	//Gets size of mod file pszFilename, returns -1 on failure
	int GetSize( const char* pszFilename );

	bool LoadScriptClassFile( const char* pszClassname, ModFile* pFile);
	bool LoadScriptClassFile( const char* pszClassname, void* pBuf );

	void ClassnameToPath( char* pszPath, const char* pszClassname );

	const char* AfterFirstElement( const char* pszPath );

	bool GetModFilePath( const char* pszPath, char pBuf[MAX_PATH] );

	void ForEachInSearchPaths( std::filesystem::path File, IForEachInSearchPath& op );

	std::string SanitizeIntoModData(const char* pszPath);

	void HandleWidescreenMounting();
};

extern ModFS* g_pModFS;
#endif
