	#pragma once

#ifdef WIN32
#include "Windows.h"
#endif

#include "thirdparty/hooking/Hooking.Patterns.h"
#include "polyhook2/Detour/x86Detour.hpp"

#ifdef WIN32
typedef HMODULE HookModule;
#else
typedef void* HookModule;
#endif

struct HookSearchParams
{
public:
	hook::pattern Pattern;
	unsigned int SearchIndex;
	HookModule Module;

	HookSearchParams() :
		Pattern(""),
		SearchIndex(0),
		Module(nullptr)
	{}

	HookSearchParams(std::string_view pszPattern, unsigned int searchIdx = 0, HookModule module = nullptr) :
		Pattern(pszPattern),
		SearchIndex(searchIdx),
		Module(module)
	{
		if (Module != nullptr)
		{
			Pattern = hook::make_module_pattern(Module, pszPattern);
		}
	}

	HookSearchParams(hook::pattern pattern, unsigned int searchIdx = 0) :
		SearchIndex(searchIdx),
		Module(nullptr)
	{
		Pattern = pattern;
	}
};

class HookDef
{
	std::string Name;
	HookSearchParams Params;

	void* Callback;
	PLH::x86Detour* Detour;

	unsigned long long Trampoline;

	void Log(std::string_view message);

public:

	HookDef() :
		Name("Mysterious nameless hook"),
		Params(),
		Callback(nullptr),
		Detour(nullptr),
		Trampoline(0)
	{}

	HookDef(std::string_view name, const HookSearchParams& parms, void* callback) :
		Name(name),
		Params(parms),
		Callback(callback),
		Detour(nullptr),
		Trampoline(0)
	{};

	void Hook();
	void UnHook();

	template<typename T>
	T Func(T f);

	~HookDef()
	{
		if (Detour != nullptr)
		{
			UnHook();
		}
	}
};

template<typename T>
inline T HookDef::Func(T f)
{
	return (T)Trampoline;
}
