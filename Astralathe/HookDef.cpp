#include "Logging.h"
#include "HookDef.h"

void HookDef::Log(std::string_view message)
{
	std::string msgResult = std::format("[{}] {}\n", Name, message);
	LogMessage(msgResult.c_str());
}

void HookDef::Hook()
{
	if (Detour != nullptr)
	{
		Log("Rehooking...");
		Detour->reHook();
		return;
	}
	Log("Hooking...");
	if (Params.Pattern.size() - 1 < Params.SearchIndex)
	{
		Log("Couldn't find pattern, or search index was out of range!");
		return;
	}

	char* pAddr = Params.Pattern.get(Params.SearchIndex).get<char>(0);
	if (!pAddr)
	{
		Log("Failed to get pattern!");
		return;
	}
	Log(std::format("Found at {:X}", (size_t)pAddr));

	Detour = new PLH::x86Detour((uint64_t)pAddr, (uint64_t)Callback, (uint64_t*)&Trampoline);
	Detour->hook();
	Log("Hooked!");
}

void HookDef::UnHook()
{
	Log("Unhooking!");

	Detour->unHook();

	delete Detour;
	Detour = nullptr;
}