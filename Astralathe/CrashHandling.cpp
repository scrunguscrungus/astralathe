#include "CrashHandling.h"
#include "Logging.h"

#ifdef WIN32
#include "windows.h"
#include "dbghelp.h"
#endif

#include <ctime>
#include <filesystem>

#define DUMP_OUT_FOLDER "Astralathe_CrashLogs"
#define DUMP_FILENAME_FMT "astralathe_crash_%s"


#ifdef WIN32

#endif

void InitCrashHandling()
{
	//LogMessage( "---INIT CRASH HANDLER---\n" );
	
	if ( !std::filesystem::exists( DUMP_OUT_FOLDER ) || !std::filesystem::is_directory( DUMP_OUT_FOLDER ) )
	{
	//	std::filesystem::create_directory( DUMP_OUT_FOLDER );
	}

#ifdef WIN32

#endif

	//LogMessage( "---FINISHED INITIALISING CRASH HANDLER---\n" );
}