#pragma once

#include <map>
#include "HookDef.h"

enum eHook
{
	//Globals
	Hook_TraceMessageV,
	Hook_TraceMessage,
	Hook_GetKeyboardInput,
	Hook_Lua_DoCallStackDump,
	Hook_Lua_ErrorHandler,

	Hook_Lua_openwithcontrol,

	//ISACT Globals
	Hook_ISACTFileOpen,

	Hook_EPrefs_ReadPrefs,

	//GameApp
	Hook_GameApp_StartUp,
	Hook_GameApp_RenderFrame,
	Hook_initUIMenu,
	Hook_GameApp_UpdateCheatCodes,
	Hook_GameApp_PlayVideo,
	Hook_GameApp_LoadLocalizedTextBank,
	Hook_GameApp_InitLua,
	Hook_GameApp_SetRichPresence,

	//EMainpage
	Hook_EMainPage_RenderMenu,

	//ETutorialBox I guess...?
	Hook_ETutorialBoxManager_AddTutuorialBox, //sic

	//DFConsole
	Hook_DFConsole_AddString,

	//EScriptVM
	Hook_EScriptVM_Initialise,
	Hook_EScriptVM_GetBaseClass,
	Hook_EScriptVM_UnloadingLevel,
	Hook_EScriptVM_SendStateMessageToAllScripts,
	Hook_EScriptVM_DoFile,

	Hook_EGameTextureManager_ReadPackFileTextures,
	Hook_EMeshManager_ReadPackFile,
	Hook_EScriptVM_ReadPackFile,
	Hook_EAnimManager_ReadPackFile,

	Hook_lua_dobuffer,
	Hook_EStream_StreamString2b,

	Hook_EDomain_LoadDomain,
	Hook_EDomain_LoadSingleMesh,

	Hook_EHashTableUint_Find,

	Hook_EHashBase_Set,

	Hook_ECamera_WorldCoordsToScreen,
	
	Hook_ERenderer_GetDisplayWidth,
	Hook_ERenderer_GetDisplayHeight,

	Hook_EFileManager_Exists,
	Hook_EFileManager_GetFullPath,
	Hook_EFileManager_AddReadPath,

	Hook_EDebugLineManager_AddLine,
	Hook_EDebugLineManager_AddArrow,
	Hook_EDebugLineManager_AddBox,
	Hook_EDebugLineManager_AddCircle,
	Hook_EDebugLineManager_AddCylinder,
	Hook_EDebugLineManager_AddSphere,
	Hook_EDebugLineManager_DrawAll,
	Hook_EDebugLineManager_ResetFrame,

	Hook_EDebugTriManager_AddTri,
	Hook_EDebugTriManager_DrawAll,
	Hook_EDebugTriManager_ResetFrame,

	HOOK_COUNT
};

extern std::unordered_map<eHook, HookDef> Hooks;