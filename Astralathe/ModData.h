#ifndef MODDATA_H
#define MODDATA_H

#include "AL_VERSION.h"
#include <string>

class ModDependency
{
public:
	enum VersionRequirement
	{
		NONE,
		EQUAL,
		GREATER,
		LESS,
		GEQ,
		LEQ
	};

	//In order of severity.
	//i.e. anything >= BADORDER means the mod cannot load if it's a hard dependency
	//We'll let bad versions slide with a warning
	enum VersionCheckResult
	{
		GOOD,
		BADVERSION,
		BADORDER,
		DISABLED,
		MISSING
	};

	std::string ID;
	AstralatheVersion::VersionInfo Version;
	VersionRequirement VersionReq;
	bool Soft;

	ModDependency(std::string id, bool soft) : ID(id), Soft(soft)
	{
		Version = AstralatheVersion::ERROR_VERSION;
		VersionReq = NONE;

		//Parse version
		size_t atIdx = id.find('@');
		if (atIdx != std::string::npos)
		{
			ID = id.substr(0, atIdx);
			
			std::string verString = id.substr(atIdx+1);
			if (verString.empty()) return;
			
			std::string version;

			auto first = verString.begin();
			switch (*first)
			{
			case '>':
				++first;
				if (first != verString.end() && *first == '=')
				{
					VersionReq = GEQ;
					++first;
				}
				else
				{
					VersionReq = GREATER;
				}
				break;
			case '<':
				++first;
				if (first != verString.end() && *first == '=')
				{
					VersionReq = LEQ;
					++first;
				}
				else
				{
					VersionReq = LESS;
				}
				break;
			case '=':
				++first;
			default: //Fallthrough
				VersionReq = EQUAL;
			}

			Version = AstralatheVersion::ParseVersion(std::string(first, verString.end()));
		}
	}

	const char* VerReqString() const
	{
		switch (VersionReq)
		{
		default:
		case NONE:
			return "";
		case EQUAL:
			return "@";
		case GREATER:
			return "@>";
		case LESS:
			return "@<";
		case GEQ:
			return "@>=";
		case LEQ:
			return "@<=";
		}
	}

	VersionCheckResult GetStatus(std::string dependent) const;

	std::string ToString() const;
};

class ModData
{
public:
	enum DependencyCheckResult
	{
		GOOD,
		SOFTMISSING,
		HARDBADORDER,
		HARDDISABLED,
		HARDMISSING,
	};

	std::string ID;
	std::string Name;
	std::string Author;
	std::string Description;
	AstralatheVersion::VersionInfo Version;
	std::vector<ModDependency> Dependencies;
	
	bool Loaded;

	ModData() : Name("Unnamed Mod"), Author(""), Description(""), Version()
	{}

	ModDependency::VersionCheckResult CheckDependencies()
	{
		bool missingSoft = false;
		for (const ModDependency& dep : Dependencies)
		{
			ModDependency::VersionCheckResult status = dep.GetStatus(ID);
			if (status >= ModDependency::VersionCheckResult::BADORDER)
			{
				if (dep.Soft)
					missingSoft = true;
				else
					return status;
			}
		}
		return ModDependency::VersionCheckResult::GOOD;
		//return missingSoft ? SOFTMISSING : GOOD;
	}
};

#endif