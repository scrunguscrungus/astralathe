#include "pch.h"

#include <iostream>
#include <stdio.h>
#include <cstdint>
#include <stdint.h>
#include <string>
#include <format>

#include "CrashHandling.h"
#include "Astralathe.h"
#include "AstralatheConfig.h"
#include "Hooks.h"
#include "Logging.h"
#include "ModFS.h"
#include "Psychonauts/PsychoTypes.h"

#include "Psychonauts/DebugDraw/EDebugLineManager.h"
#include "Psychonauts/DebugDraw/EDebugTriManager.h"
#include "Psychonauts/Proxying/EDebugLineManagerProxy.h"
#include "Psychonauts/Proxying/EDebugTriManagerProxy.h"

#include "Psychonauts/Proxying/GameAppProxy.h"

#include "LuaSecurity.h"
#include "Steam/SteamHooks.h"

#include "DXHooks.h"
#include "ImGui/AstralatheImGui.h"

#include "Psychonauts/Proxying/EEntityManagerProxy.h"
#include "Psychonauts/Proxying/CallablesHelper.h"

//#include "libfmemopen.h"
#include "json.hpp"
using json = nlohmann::json;

#include "injector/assembly.hpp"
#include "injector/hooking.hpp"
#include "injector/utility.hpp"

#include "polyhook2/Detour/x86Detour.hpp"

#include "Utility.h"

//TODO: Rework this into something less shite, this is super inefficient
#define FROM_PATTERN_IDX(idx, pat) hook::pattern(pat).get(idx).get<char>(0)
#define FROM_PATTERN_MODULE(mod, pat) hook::make_module_pattern( mod, pat ).get( 0 ).get<char>( 0 )

#define pLua_tostring "55 8B EC 83 EC 08 8B 45 ? 50 8B 4D ? 51 E8 ? ? ? ? 83 C4 08 89 45 ? 83 7D ? 00 74 ? 8B 55 ? 83 3A 03 74 ? 8B 45 ? 50 8B 4D ? 51 E8 ? ? ? ? 83 C4 08 85 C0 75 ? 8B 55 ? 8B 42 ? 83 C0 14"
typedef const char* ( *tLua_tostring )( void* L, int index );
tLua_tostring lua_tostring;

#define pMakeClassNameCanonical "55 8B EC 8B 45 ? 50 8B 4D ? 51 8B 55 ? 52 FF 15 ? ? ? ?"
typedef void ( *tMakeClassNameCanonical )( char*, char*, unsigned int );
tMakeClassNameCanonical MakeClassNameCanonical;

#define pClassNameToFileName "55 8B EC 81 EC 18 01 00 00 68 04 01 00 00"
typedef unsigned int ( *tClassNameToFileName )( char*, char*, char*, bool );
tClassNameToFileName ClassNameToFileName;

#define pClassNameToConstructorName "55 8B EC 83 EC 08 8B 45 ? 89 45 ? 6A 06"
typedef void( *tClassNameToConstructorName )( char*, char*, unsigned int );
tClassNameToConstructorName ClassNameToConstructorName;

#define pECamera_WorldCoordsToScreen "55 8B EC 83 EC 74 89 4D ? 8B 45 ? D9 00"

#define pISACTShortStringToCharString "57 8B 7C 24 ? 0F B7 0F"
typedef int( *tISACTShortStringToCharString )( unsigned short*, char* );
tISACTShortStringToCharString ISACTShortStringToCharString;

#define pISACTCharStringToShortString "8B 54 24 ? 8A 0A"
typedef int( *tISACTCharStringToShortString )( char*, unsigned short* );
tISACTCharStringToShortString ISACTCharStringToShortString;

HMODULE g_hISACT;

unsigned char* c_dfDIKeyboard;
//GameApp* g_pGameApp;
EFileManager* g_pFileManager;

class EPrefs
{};

bool g_bDoDebugConsoleLogging = false;

unsigned char g_KeysPressed[ 256 ];
unsigned char g_KeysDown[ 256 ];

EEntityManager* gEntities = nullptr;
EEntityManagerProxy* gEntities_Proxy = nullptr;

//For widescreen
//https://github.com/ThirteenAG/WidescreenFixesPack/blob/master/source/Psychonauts.WidescreenFix/dllmain.cpp#L3
struct Screen
{
	int32_t nWidth;
	int32_t nHeight;
	float fWidth;
	float fHeight;
	float fFieldOfView;
	float fAspectRatio;
	int32_t nWidth43;
	float fWidth43;
	float fHudScale;
	float fHudScale2;
	float fHudOffset;
	float fHudOffsetReal;
	float fHudOffsetWide;
} ScreenInfo;


void FatalError( const char* msg, ... )
{
	va_list args;
	va_start( args, msg );

	char outmsg[ 2048 ];
	vsprintf_s( outmsg, msg, args );
	LogMessageRaw( "!!!ASTRALATHE FATAL!!!" );
	LogMessageRaw( outmsg );
	LogMessageRaw( "\n" );
	LogMessageRaw( "!!!ASTRALATHE FATAL!!!" );

	va_end( args );
	exit( -1 );
}

char* stristr( const char* str1, const char* str2 )
{
	const char* p1 = str1;
	const char* p2 = str2;
	const char* r = *p2 == 0 ? str1 : 0;

	while ( *p1 != 0 && *p2 != 0 )
	{
		if ( tolower( (unsigned char)*p1 ) == tolower( (unsigned char)*p2 ) )
		{
			if ( r == 0 )
			{
				r = p1;
			}

			p2++;
		}
		else
		{
			p2 = str2;
			if ( r != 0 )
			{
				p1 = r + 1;
			}

			if ( tolower( (unsigned char)*p1 ) == tolower( (unsigned char)*p2 ) )
			{
				r = p1;
				p2++;
			}
			else
			{
				r = 0;
			}
		}

		p1++;
	}

	return *p2 == 0 ? (char*)r : 0;
}

void RunLuaCmd( const char* cmd )
{
	EScriptVM* pScriptVM = GetScriptVM();

	if ( pScriptVM )
	{
		//printf( "Running Lua code \"%s\"\n", cmd );
		EScriptVM_RunLuaCommands( pScriptVM, cmd, nullptr, nullptr, 0 );
	}
}

void RunDbgCmd( const char* cmd )
{
	char cmdString[ 1024 ];

	sprintf_s( cmdString, "DebugCommand(\"%s\")", cmd );
	RunLuaCmd( cmdString );
}

void __fastcall TraceMessageV( char* ecx, char* edx, DWORD traceType, char* param_2, va_list param_3 )
{
	if ( AstralatheConfig::GetConfigValue( CONFIG_DEBUGOUTPUT ) )
	{
		//TODO: needed?
		if ( ecx[ traceType * 8 + 64 ] == 0 )
			ecx[ traceType * 8 + 64 ] = 1;
	}

	Hooks[Hook_TraceMessageV].Func(TraceMessageV)(ecx, edx, traceType, param_2, param_3);
}

void __fastcall TraceMessage( char* ecx, char* edx, char* msg )
{
	if ( AstralatheConfig::GetConfigValue( CONFIG_DEBUGOUTPUT ) )
	{
		LogMessageRaw( "%s\n", msg );
	}
	
	Hooks[Hook_TraceMessage].Func(TraceMessage)( ecx, edx, msg );
}

void __fastcall DFConsole_AddString( char* ecx, char* edx, char* msg )
{
	if ( AstralatheConfig::GetConfigValue( CONFIG_DEBUGOUTPUT ) )
	{
		if ( g_bDoDebugConsoleLogging )
		{
			LogMessageRaw( "%s\n", msg );
		}
	}

	//Slight hack, this var is only set when we're dumping the Lua call stack after an error, so we use it to set log type here
	if (!g_bDoDebugConsoleLogging)
	{
		LogToConsole(msg);
	}
	else
	{
		LogToConsole(msg, CLT_StackTrace);
	}
	Hooks[Hook_DFConsole_AddString].Func(DFConsole_AddString)( ecx, edx, msg );
}

void Lua_DoCallStackDump( lua_State* L, int param_1, int param_2 )
{
	g_bDoDebugConsoleLogging = true;
	Hooks[Hook_Lua_DoCallStackDump].Func(Lua_DoCallStackDump)( L, param_1, param_2 );
	g_bDoDebugConsoleLogging = false;
}

void Lua_ErrorHandler( void* L )
{
	if ( AstralatheConfig::GetConfigValue( CONFIG_DEBUGOUTPUT ) )
	{
		const char* pErrorString = lua_tostring( L, -1 );
		if (pErrorString)
		{
			LogMessageRaw("--Lua ERROR: %s\n", pErrorString);
			LogToConsole(std::format("--Lua ERROR: {}\n", pErrorString), CLT_Error);
		}
	}

	Hooks[Hook_Lua_ErrorHandler].Func(Lua_ErrorHandler)( L );
	Lua_DoCallStackDump( (lua_State*)L, 0, 0 );
}

void __stdcall GetKeyboardInput( void )
{
	Hooks[Hook_GetKeyboardInput].Func(GetKeyboardInput)( );

	for ( int i = 0; i < 256; i++ )
	{
		bool bDown = c_dfDIKeyboard[ i ] & 0x80;
		if ( bDown )
		{
			if ( !g_KeysPressed[ i ] && !g_KeysDown[i] )
			{
				g_KeysPressed[ i ] = true;
				g_KeysDown[ i ] = true;
			}
			else if ( g_KeysPressed[ i ] && g_KeysDown[ i ] )
			{
				g_KeysPressed[ i ] = false;
			}
		}
		else
		{
			g_KeysPressed[ i ] = false;
			g_KeysDown[ i ] = false;
		}
	}
}

enum class EHashBaseBlocker
{
	None = -1,
	EGameTextureManager,
	EMeshManager,
	EScriptVM,
	EAnimManager
};
static bool g_bBlockModFileHashBase = false;
static EHashBaseBlocker g_iHashBaseBlockSource = EHashBaseBlocker::None;
static void* g_pBlocker = nullptr;
static char g_sLastRequestedDoFileName[ MAX_PATH ];

const char* GetBlockSourceString()
{
	using enum EHashBaseBlocker;
	switch (g_iHashBaseBlockSource)
	{
	case EGameTextureManager:
		return "EGameTextureManager";
	case EMeshManager:
		return "EMeshManager";
	case EScriptVM:
		return "EScriptVM";
	case EAnimManager:
		return "EAnimManager";
	case None:
		return "No Blocker";
	default:
		return "???";
	}
}

static void BlockHashBase( EHashBaseBlocker source, void* pBlocker )
{
	g_bBlockModFileHashBase = true;
	g_iHashBaseBlockSource = source;
	g_pBlocker = pBlocker;
	LogMessage( CONFIG_VERBOSEHASHBASESPEW, "==HashBase::Set blocked by %s==\n", GetBlockSourceString());
}
static void UnblockHashbase()
{
	LogMessage( CONFIG_VERBOSEHASHBASESPEW, "==Unblocking HashBase::Set==\n" );
	g_bBlockModFileHashBase = false;
	g_pBlocker = nullptr;
	g_iHashBaseBlockSource = EHashBaseBlocker::None;
}

void __fastcall EGameTextureManager_ReadPackFileTextures( void* pThis, void* edx, void* pFileReader, unsigned short param_2, unsigned short param_3 )
{
	BlockHashBase( EHashBaseBlocker::EGameTextureManager, pThis );
	Hooks[Hook_EGameTextureManager_ReadPackFileTextures].Func(EGameTextureManager_ReadPackFileTextures)( pThis, edx, pFileReader, param_2, param_3 );
	UnblockHashbase();
}

void __fastcall EMeshManager_ReadPackFile( void* pThis, void* edx, void* pFileReader )
{
	BlockHashBase( EHashBaseBlocker::EMeshManager, pThis );
	Hooks[Hook_EMeshManager_ReadPackFile].Func(EMeshManager_ReadPackFile)( pThis, edx, pFileReader );
	UnblockHashbase();
}

void __fastcall EScriptVM_ReadPackFile( void* pThis, void* edx, void* pFileReader )
{
	BlockHashBase( EHashBaseBlocker::EScriptVM, pThis );
	Hooks[Hook_EScriptVM_ReadPackFile].Func(EScriptVM_ReadPackFile)( pThis, edx, pFileReader );
	UnblockHashbase();
}

void __fastcall EAnimManager_ReadPackFile(void* pThis, void* edx, void* pFileReader, bool bIsCommon)
{
	BlockHashBase(EHashBaseBlocker::EAnimManager, pThis);
	Hooks[Hook_EAnimManager_ReadPackFile].Func(EAnimManager_ReadPackFile)(pThis, edx, pFileReader, bIsCommon);
	UnblockHashbase();
}


ModFile DoFileModFile; //Global because having it inside the function mangles the stack or something. This took me 10 months to fix.

int _cdecl lua_dobuffer( lua_State* L, const char* buff, int size, const char* name )
{
	if ( g_bBlockModFileHashBase && g_iHashBaseBlockSource == EHashBaseBlocker::EScriptVM )
	{
		if ( g_sLastRequestedDoFileName[ 0 ] != '\0' )
		{
			if ( g_pModFS->ModFileExists( g_sLastRequestedDoFileName ) )
			{
				if ( g_pModFS->LoadFile( g_sLastRequestedDoFileName, &DoFileModFile) )
				{
					return Hooks[Hook_lua_dobuffer].Func(lua_dobuffer)( L, DoFileModFile.pData, DoFileModFile.iSize, name );
				}
			}
			else
			{
				//It wasn't in a mod, but there's a good chance it's hanging around in native_assets
				//For dofiles the game doesn't normally check its normal search paths
				if (g_pModFS->NativeAssetExists(g_sLastRequestedDoFileName))
				{
					if (g_pModFS->LoadFile(g_sLastRequestedDoFileName, &DoFileModFile, true))
					{
						return Hooks[Hook_lua_dobuffer].Func(lua_dobuffer)(L, DoFileModFile.pData, DoFileModFile.iSize, name);
					}
				}
			}
			g_sLastRequestedDoFileName[ 0 ] = '\0';
		}
	}
	return Hooks[Hook_lua_dobuffer].Func(lua_dobuffer)( L, buff, size, name );
}

//Nasty!
void __fastcall EStream_StreamString2b( void* pThis, void* edx, char* pOutBuf, int size )
{
	Hooks[Hook_EStream_StreamString2b].Func(EStream_StreamString2b)( pThis, edx, pOutBuf, size );
	if ( g_bBlockModFileHashBase && g_iHashBaseBlockSource == EHashBaseBlocker::EScriptVM )
	{
		strcpy_s( g_sLastRequestedDoFileName, pOutBuf );
	}
}

//TODO: EDomain::LoadDomainMem?
//"55 8B EC 81 EC C8 00 00 00 89 8D ? ? ? ? 8B 45 ? 50"

void* __fastcall EDomain_LoadDomain( void* pThis, void* edx, const char* pszDomain, int iDatatype )
{
	const char* pszFind = pszDomain;
	if ( stristr( pszFind, "WorkResource" ) )
	{
		pszFind = g_pModFS->AfterFirstElement( pszFind );
	}
	void* pRet = Hooks[Hook_EDomain_LoadDomain].Func(EDomain_LoadDomain)( pThis, edx, pszFind, iDatatype );
	return pRet;
}
void* __fastcall EDomain_LoadSingleMesh( void* pThis, void* edx, char* pszMesh, int param_2 )
{
	LogMessage( CONFIG_DEBUGLOADSINGLEMESH, "Loading mesh: %s\n", pszMesh );
	void* pRet = Hooks[Hook_EDomain_LoadSingleMesh].Func(EDomain_LoadSingleMesh)( pThis, edx, pszMesh, param_2 );
	if ( pRet == nullptr && AstralatheConfig::GetConfigValue( CONFIG_DEBUGLOADSINGLEMESH ) )
	{
		LogMessage( CONFIG_DEBUGLOADSINGLEMESH, "!! Mesh load FAILED: %s\n", pszMesh);
	}
	return pRet;
}

unsigned int __fastcall EHashTableUint_Find( void* pThis, void* edx, char* pszKey, unsigned int param_2 )
{
	LogMessage( CONFIG_VERBOSEHASHBASESPEW, "(%s) EHashTable<unsigned int>::Find(%s, 0x%p)\n", GetBlockSourceString(), pszKey, param_2 );
	unsigned int realResult = Hooks[Hook_EHashTableUint_Find].Func(EHashTableUint_Find)( pThis, edx, pszKey, param_2 );
	if ( g_bBlockModFileHashBase )
	{
		if ( stristr( pszKey, ".dds" ) && g_iHashBaseBlockSource != EHashBaseBlocker::EGameTextureManager )
		{
			//Once the texture manager is done loading textures, everything after it should be allowed to know that the textures blocked aren't actually loaded
			return realResult;
		}
		char pResourcePath[MAX_PATH];

		if (g_iHashBaseBlockSource == EHashBaseBlocker::EScriptVM)
		{
			//ScriptVM uses class names as keys
			//So we have to fix up the search key to an actual path instead
			
			g_pModFS->ClassnameToPath(pResourcePath, pszKey);
			pszKey = pResourcePath;
		}

		char pszFixedSlashes[ MAX_PATH ];
		g_pModFS->FixSlashes( pszKey, pszFixedSlashes );

		if ( pszFixedSlashes != nullptr )
		{
			if ( g_pModFS->ModFileExists( pszFixedSlashes ) )
			{
				char pFullPath[ MAX_PATH ];
				g_pModFS->GetModFilePath( pszFixedSlashes, pFullPath );

				LogMessage( CONFIG_VERBOSEHASHBASESPEW, "(%s) Blocking EHashBase::Find for %s\n", GetBlockSourceString(), pszFixedSlashes );

				return 1; //Tell the game "yep, the asset you are checking for does exist" so it doesn't load it
			}
			else
			{
				LogMessage( CONFIG_VERBOSEHASHBASESPEW, "(%s) HashBase blocking enabled but file isn't in any mod\n", GetBlockSourceString() );
			}
		}
	}
	return realResult;
}
unsigned int __fastcall EHashBase_Set( EHashBase* pThis, void* edx, char* pszKey, void* param_2 )
{
	return Hooks[Hook_EHashBase_Set].Func(EHashBase_Set)( pThis, edx, pszKey, param_2 );
}

static inline unsigned int GetDisplayWidth()
{
	if (AstralatheConfig::GetConfigValue(CONFIG_FIX_FX))
	{
		ERendererProxy pRend(g_pGameAppProxy->m_pRenderer.GetValue());
		if (pRend.IsValid())
		{
			return (float)pRend.m_iWidth.GetValue() * AstralatheConfig::GetConfigValue<float>(CONFIG_FIX_FX_SCALE);
		}
	}
	return 640;
}
static inline unsigned int GetDisplayHeight()
{
	if (AstralatheConfig::GetConfigValue(CONFIG_FIX_FX))
	{
		ERendererProxy pRend(g_pGameAppProxy->m_pRenderer.GetValue());
		if (pRend.IsValid())
		{
			return (float)pRend.m_iHeight.GetValue() * AstralatheConfig::GetConfigValue<float>(CONFIG_FIX_FX_SCALE);
		}
	}
	return 480;
}

unsigned int __fastcall ERenderer_GetDisplayWidth(void* pThis, void* edx)
{
	return GetDisplayWidth();
}
unsigned int __fastcall ERenderer_GetDisplayHeight(void* pThis, void* edx)
{
	return GetDisplayHeight();
}

char* __fastcall GameApp_StartUp( GameApp* ecx, char* edx, void* param_1 )
{
	//Should really find this in InitGlobals but inheritance is a bitch
	g_pGameApp = ecx;
	g_pGameAppProxy = new GameAppProxy(g_pGameApp);

#ifdef USE_DISCORD
	Astralathe::DiscordManager().Startup();
#endif

	return Hooks[Hook_GameApp_StartUp].Func(GameApp_StartUp)( ecx, edx, param_1 );
}

unsigned int __fastcall GameApp_RenderFrame( GameApp* ecx, char* edx )
{
	unsigned int retval = Hooks[Hook_GameApp_RenderFrame].Func(GameApp_RenderFrame)( ecx, edx );

	if ( AstralatheConfig::GetConfigValue( CONFIG_DEBUGMENU ) )
	{
		if ( g_KeysPressed[ DIK_F9 ] )
		{
			//Debug menu
			EUIMenu* pUIMenu = ecx->pUIMenu;

			if ( pUIMenu )
			{
				EInGamePage* pIGP = *(EInGamePage**)( pUIMenu + 464 );
				if ( pIGP )
				{
					EMenuPage* pDebugMenu = *(EMenuPage**)( pIGP + 348 );

					if ( pDebugMenu )
					{
						//Recreates functionality of EUIMenu::ShowInGame
						*(char*)( pIGP + 516 ) = 1;
						PLH::FnCast( *(void**)( *(int*)pDebugMenu + 56 ), EMenuPage_AssignmentThing )( pDebugMenu );
						*(char**)( pIGP + 256 ) = (char*)pDebugMenu;
					}
				}
			}

		}
	}

	return retval;
}

unsigned int __fastcall GameApp_PlayVideo( GameApp* ecx, void* edx, const char* pszVideo, void* param_2, bool param_3, float param_4, unsigned int param_5, bool param_6 )
{
	if ( AstralatheConfig::GetConfigValue( CONFIG_MAJESCO ) )
	{
		if ( strcmp( pszVideo, "workresource\\cutScenes\\prerendered\\dflogo.bik" ) == 0 )
		{
			//Bit of a hack, but it's just for fun!
			Hooks[Hook_GameApp_PlayVideo].Func(GameApp_PlayVideo)( ecx, edx, "workresource\\cutScenes\\prerendered\\MajescoLogo.bik", param_2, param_3, param_4, param_5, param_6 );
		}
	}

	return Hooks[Hook_GameApp_PlayVideo].Func(GameApp_PlayVideo)( ecx, edx, pszVideo, param_2, param_3, param_4, param_5, param_6 );
}

class LoadLuaLoc : public ModFS::IForEachInSearchPath
{
public:
	virtual void Operate( std::filesystem::path Path ) override
	{
		if ( Path.extension() != ".lua" )
			return;

		EScriptVM* pVM = GetScriptVM();
		if ( pVM )
		{
			std::ifstream file( Path.string().c_str(), std::ios::binary );
			if ( !file.good() )
				return;

			std::vector<char> vecData;

			uintmax_t iFileSize = std::filesystem::file_size( Path );
			if (iFileSize > (std::numeric_limits<std::size_t>::max)())
				return; //Wow that's a big fucking Lua file!
			if ( iFileSize <= 0 )
				return; //RunLuaCommands won't like that
			
			size_t iSize = static_cast<size_t>(iFileSize);

			vecData.reserve( iSize );
			vecData.assign( std::istreambuf_iterator<char>( file ), std::istreambuf_iterator<char>() );

			LogMessage( "Loading Lua loc from %s\n", Path.string().c_str() );
			EScriptVM_RunLuaCommands( pVM, vecData.data(), NULL, NULL, iSize );
		}
	}
};

void lua_getdotted( lua_State* l, const char* kstr )
{
	lua_pushstring( l, kstr );
	lua_gettable( l, -2 );
	lua_remove( l, -2 );
}

class LoadJSONLoc : public ModFS::IForEachInSearchPath
{
private:
	void AddLocString( lua_State* l, const char* pszLinecode, const char* pszString, const char* pszTable )
	{
		EScriptVM* pVM = GetScriptVM();
		if ( pVM )
		{
			std::string command = std::format( "Global.StringTable.{}['{}'] = \"{}\";", pszTable, pszLinecode, pszString );
			RunLuaCmd( command.c_str() );
		}
		else
		{
			LogMessage( "Warning: Couldn't get script VM to load JSON loc file\n" );
		}
	}
public:
	virtual void Operate( std::filesystem::path Path ) override
	{
		std::ifstream file( Path.string().c_str() );
		if ( !file.good() )
			return;

		EScriptVM* pVM = GetScriptVM();
		if ( !pVM )
			return;

		lua_State* l = GetLuaState( pVM );
		if ( !l )
			return;

		json jsData;
		//LogMessage( "Loading %s as a JSON localisation file...\n", Path.string().c_str() );
		try
		{
			jsData = json::parse( file, nullptr, true, true );
		}
		catch (std::exception& e)
		{
			LogMessage( "[ERROR] The JSON file %s could not be parsed! %s\n", Path.string().c_str(), e.what() );
			return;
		}

		bool bGlobal = false;
		if ( jsData.contains( "Global" ) )
		{
			bGlobal = jsData[ "Global" ].get<bool>();
		}

		std::string tableToAddTo = bGlobal ? "Global" : "Level";

		//Load default
		if ( jsData.contains( "Default" ) )
		{
			json defaultLoc = jsData[ "Default" ];

			for ( auto& el : defaultLoc.items() ) {
				const char* pszLinecode = el.key().c_str();
				std::string val = el.value().get<std::string>();
				const char* pszString = val.c_str();
				//LogMessage( "Adding default localisation string - %s: %s\n", pszLinecode, pszString );
				AddLocString( l, pszLinecode, pszString, tableToAddTo.c_str() );
			}
		}

		char* pszLang = g_pGameAppProxy->m_pszCurrentLanguage.GetPointer();
		if ( pszLang )
		{
			//Load current lang
			if ( jsData.contains( pszLang ) )
			{
				json lang = jsData[ pszLang ];

				for ( auto& el : lang.items() ) {
					const char* pszLinecode = el.key().c_str();
					std::string val = el.value().get<std::string>();
					const char* pszString = val.c_str();
					//LogMessage( "Adding %s localisation string - %s: %s\n", pszLang, pszLinecode, pszString );
					AddLocString( l, pszLinecode, pszString, tableToAddTo.c_str() );
				}
			}
		}
	}
};

void LoadModLocalisation(char* pszLevelCode, const char* pszExtension, ModFS::IForEachInSearchPath& loader)
{
	std::filesystem::path p1 = "Localization/";
	std::filesystem::path p2 = pszLevelCode;
	p2 += pszExtension;

	std::filesystem::path fileToFind = p1 / p2;

	g_pModFS->ForEachInSearchPaths( fileToFind, loader );

	char* pszLang = g_pGameAppProxy->m_pszCurrentLanguage.GetPointer();
	if ( pszLang )
	{
		p1 += pszLang;
		p1 += "/";

		fileToFind = p1 / p2;

		g_pModFS->ForEachInSearchPaths( fileToFind, loader );
	}
}

void __fastcall GameApp_LoadLocalizedTextBank( GameApp* ecx, void* edx, char* pszLevelCode )
{
	Hooks[Hook_GameApp_LoadLocalizedTextBank].Func(GameApp_LoadLocalizedTextBank)( ecx, edx, pszLevelCode );

	LoadLuaLoc luaLoader;
	LoadModLocalisation( pszLevelCode, ".lua", luaLoader);

	LoadJSONLoc jsonLoader;
	LoadModLocalisation( pszLevelCode, ".json", jsonLoader );
}

//TODO: Consolidate with LoadLuaLoc
class RunBootLua : public ModFS::IForEachInSearchPath
{
public:
	virtual void Operate( std::filesystem::path Path ) override
	{
		if ( Path.extension() != ".lua" )
			return;

		EScriptVM* pVM = GetScriptVM();
		if ( pVM )
		{
			lua_State* l = GetLuaState(pVM);
			if (!l)
				return;

			std::ifstream file( Path.string().c_str(), std::ios::binary );
			if ( !file.good() )
				return;

			std::vector<char> vecData;

			uintmax_t iFileSize = std::filesystem::file_size(Path);
			if (iFileSize > (std::numeric_limits<std::size_t>::max)())
				return; //Wow that's a big fucking Lua file!
			if (iFileSize <= 0)
				return; //RunLuaCommands won't like that

			size_t iSize = static_cast<size_t>(iFileSize);

			vecData.reserve( iSize );
			vecData.assign( std::istreambuf_iterator<char>( file ), std::istreambuf_iterator<char>() );

			lua_dobuffer(l, vecData.data(), iSize, "Astralathe::RunBootLua");
		}
	}
};
	
void __fastcall GameApp_InitLua( GameApp* ecx, void* edx )
{
	Hooks[Hook_GameApp_InitLua].Func(GameApp_InitLua)( ecx, edx );

	RunBootLua runner;
	//Run Astralathe boot scripts
	
	LogMessage("Installing Astralathe Lua hooks...\n");
	runner.Operate("astralathe_hooks.lua");

	//if (AstralatheConfig::GetConfigValue(CONFIG_FIX_WIDESCREEN))
	{
		LogMessage("Installing Astralathe widescreen Lua hooks...\n");
		runner.Operate("./WidescreenResource/al_ws_hooks.lua");
	}

	//Call every mod's own boot lua
	g_pModFS->ForEachInSearchPaths( "mod_boot.lua", runner );
}

void __fastcall GameApp_initUIMenu( GameApp* ecx, char* edx )
{
	if ( AstralatheConfig::GetConfigValue( CONFIG_LEVELSELECT ) )
	{
#ifdef USE_DISCORD
		Astralathe::DiscordManager().SetRichPresence(ecx, "LevelSelect");
#endif
		EUIMenu_StartUp( ecx->pUIMenu, "OfficialLevelMenu" );
	}
	else
	{
		Hooks[Hook_initUIMenu].Func(GameApp_initUIMenu)( ecx, edx );
	}
}

void __fastcall GameApp_SetRichPresence(GameApp* ecx, void* edx, char* key, char* presenceString)
{
#ifdef USE_DISCORD
	Astralathe::DiscordManager().SetRichPresence(ecx, presenceString != nullptr ? presenceString : "");
#endif
	Hooks[Hook_GameApp_SetRichPresence].Func(GameApp_SetRichPresence)(ecx, edx, key, presenceString);
}

static bool s_bDebugCrapWTS = false;
void __fastcall GameApp_UpdateCheatCodes( GameApp* ecx, void* edx, void* pInput )
{
	if ( g_KeysPressed[ DIK_F3 ] )
	{
		std::string line, text;
		std::ifstream in( "./astralathe_lua.lua" );
		while ( !in.fail() && std::getline( in, line ) )
		{
			text += line + "\n";
		}

		RunLuaCmd( text.c_str() );
	}

	if ( AstralatheConfig::GetConfigValue( CONFIG_CHEATKEYS ) )
	{
		EScriptVM* pScriptVM = &ecx->pScriptVM;
		EEntity* pPlayer = ecx->pPlayer;
		EScriptObject* pPlayerScriptObj = nullptr;
		if ( pPlayer != nullptr )
		{
			pPlayerScriptObj = pPlayer->pScriptObj;
		}

		//Cheat shortcuts
		//Will probably rework this if we ever get ImGui online
		if ( g_KeysPressed[ DIK_F4 ] )
		{
			//Invincibility
			//Differs from the official cheat in that this won't enable Super Oly in Meat Circus
			ecx->m_cRazInvincible = !ecx->m_cRazInvincible;
			LogMessage( "You cheated! %s\n", ecx->m_cRazInvincible ? "Invincibility ON!" : "Invincibility OFF!" );
		}
		if ( g_KeysPressed[ DIK_F5 ] )
		{
			//All powers
			LogMessage( "You cheated! All powers!\n" );
			EScriptObject* pDebug = EScriptVM_FindScriptByName( pScriptVM, "Debug" );
			if ( pDebug != nullptr )
			{
				EScriptObject_SendMessage( pDebug, "DebugMenu", NULL, "powers", 0 );
			}
		}
		if ( g_KeysPressed[ DIK_F6 ] )
		{
			//9999 Lives
			LogMessage( "You cheated! 9999 lives!\n" );
			if ( pPlayerScriptObj )
			{
				EScriptObject_SetTableValue( pPlayerScriptObj, "stats.maxLives", 9999 );
				EScriptObject_SetTableValue( pPlayerScriptObj, "stats.dartLives", 9999 );
			}
		}
		if ( g_KeysPressed[ DIK_F7 ] )
		{
			//Global items
			LogMessage( "You cheated! Global items!\n" );
			EScriptObject* pDebug = EScriptVM_FindScriptByName( pScriptVM, "Debug" );
			if ( pDebug != nullptr )
			{
				EScriptObject_SendMessage( pDebug, "DebugMenu", NULL, "globalitems", 0 );
			}
		}
		if ( g_KeysPressed[ DIK_F8 ] )
		{
			//Rank 101
			LogMessage( "You cheated! Rank 101!\n" );
			if ( pPlayerScriptObj != nullptr )
			{
				EScriptObject_CallMethodf( pPlayerScriptObj, "setRank", "ii", 101, 1 );
			}
		}
	}

	Hooks[Hook_GameApp_UpdateCheatCodes].Func(GameApp_UpdateCheatCodes)( ecx, edx, pInput );
}

void __fastcall EMainPage_RenderMenu( EMainPage* ecx, void* edx )
{
	if ( AstralatheConfig::GetConfigValue( CONFIG_LEVELSELECT ) || AstralatheConfig::GetConfigValue( CONFIG_OLDMENU ) )
	{
		//This flag is needed when using level select or old menu hack
		//Not sure where it gets set normally, but without it set, the game
		//won't accept input in the menu
		ecx->bFlagThatFixesInput = 1;
	}

	if ( AstralatheConfig::GetConfigValue( CONFIG_OLDMENU ) )
	{
		//TODO: I managed to break this at some point and now input doesn't work and trying to select an option
		//dumps you into a void.
		EMainPage_DrawMenu( ecx );
		return;
	}

	Hooks[Hook_EMainPage_RenderMenu].Func(EMainPage_RenderMenu)( ecx, edx );
}

void EScriptObject_GetTableValue(EScriptObject* pThis, const char* pszTarget, int* pOut)
{
	EScriptVM_GetScriptTableValueFromFloat(GetScriptVM(), pThis, pszTarget, pOut);
}

int __fastcall ETutorialBoxManager_AddTutuorialBox(void* pThis, void* edx, char* param_1, unsigned int param_2, char* param_3, char* param_4, EScriptObject* param_5)
{
	LogMessage("Adding tutorial %s\n", param_1);
	//Base game does this check already but also requires bLoadedFromMainMenu to be false which is not what we want
	EScriptObject* pGlobal = EScriptVM_FindScriptByName(GetScriptVM(), "Global");
	if (pGlobal)
	{
		int disableBoxes = 0;
		EScriptObject_GetTableValue(pGlobal, "saved.Global.bDisableTutorialBoxes", &disableBoxes);
		if (disableBoxes == 1)
		{
			return -1;
		}
	}

	return Hooks[Hook_ETutorialBoxManager_AddTutuorialBox].Func(ETutorialBoxManager_AddTutuorialBox)(pThis, edx, param_1, param_2, param_3, param_4, param_5);
}

void __fastcall EScriptVM_Initialise(EScriptVM* pThis, void* edx)
{
	Hooks[Hook_EScriptVM_Initialise].Func(EScriptVM_Initialise)(pThis, edx);
	InstallLuaSecurity(GetLuaState(pThis)); //Install Lua security right after init but before the game has a chance to call anything
}

int __fastcall EScriptVM_GetBaseClass(EScriptVM* pThis, void* edx, char* pszClassname, char* param_2, bool param_3)
{
	bool wasBlocked = false;
	if (g_bBlockModFileHashBase && g_iHashBaseBlockSource == EHashBaseBlocker::EScriptVM)
		wasBlocked = true;
		UnblockHashbase(); //Temporarily let requests through so GetBaseClass sees that the function doesn't exist and loads it from FS.
	
	int ret = Hooks[Hook_EScriptVM_GetBaseClass].Func(EScriptVM_GetBaseClass)(pThis, edx, pszClassname, param_2, param_3);
	if (wasBlocked)
		BlockHashBase(EHashBaseBlocker::EScriptVM, pThis); //If we were blocking before, restore blocking state so ReadPackFile never knows a thing...
	return ret;
}

void __fastcall EScriptVM_UnloadingLevel( EScriptVM* pThis, void* edx )
{
	return Hooks[Hook_EScriptVM_UnloadingLevel].Func(EScriptVM_UnloadingLevel)( pThis, edx );
}

void __fastcall EScriptVM_SendStateMessageToAllScripts( EScriptVM* pThis, void* edx, unsigned int uMsg )
{
	Hooks[Hook_EScriptVM_SendStateMessageToAllScripts].Func(EScriptVM_SendStateMessageToAllScripts)( pThis, edx, uMsg );

	std::string hookName;

	switch ( uMsg )
	{
	case 1:
		hookName = "prebeginlevel";
		break;
	case 2:
		hookName = "beginlevel";
		break;
	case 3:
		hookName = "postbeginlevel";
		break;
	case 4:
		hookName = "endlevel";
		break;
	}

	if ( !hookName.empty() )
		EScriptVM_RunLuaCommands( pThis, std::format( "run_hook(\"{}\")", hookName ).c_str(), nullptr, nullptr, 0 );
}


/*
	This is a slight hack that toggles off the "DVDEnvironment" prefs option before allowing
	EScriptVM::DoFile to be called, then turns it back on.

	This is because this function is essentially stubbed out in the final game when this option
	is turned on.

	In the final game when everything is in a PPF, all the DoFiles in a PPF
	are executed immediately upon being read.

	Hence, calls to DoFile get stubbed because it'd be completely redundant.

	Until now this has been fine - Astralathe has some hacks to make sure DoFiles get executed
	from mods' folders and if a mod isn't overriding a DoFile then it just gets called as normal.

	But with the advent of custom levels, well, custom levels aren't in PPFs. This means that
	DoFiles can't ever be called in custom levels because there's no PPF to load them from
	and any attempts made by the Lua scripts to run them do nothing because this function is
	stubbed.

	I could probably toggle this off globally in EPrefs::ReadPrefs like I do with ProductionLoading,
	but this is also checked in CheckForLocalizedTexture, LoadGameTexture and Lua_LoadNewLevel
	and it could have side effects or play badly with some of Astralathe's other hacks, so that
	needs testing.

	That being said, this might also introduce the side effect of DoFiles being run twice in
	vanilla levels but that's *probably* fine?
*/
void __fastcall EScriptVM_DoFile(void* pThis, void* edx, char* pszFile, bool param_2)
{
	char* gpCharGameApp = (char*)g_pGameApp;
	*((bool*)(gpCharGameApp + 38275)) = false;
	Hooks[Hook_EScriptVM_DoFile].Func(EScriptVM_DoFile)(pThis, edx, pszFile, param_2);
	*((bool*)(gpCharGameApp + 38275)) = true;
}

void __fastcall ECamera_WorldCoordsToScreen( void* pThis, void* edx, void* param_1, void* param_2, bool bRealRes )
{
	//This param seems to explicitly exist to tell the function to adjust the result to account for the screen size being different
	//No idea why it should ever be false
	if ( AstralatheConfig::GetConfigValue( CONFIG_FIX_WORLDTOSCREEN ) )
		bRealRes = true;

	return Hooks[Hook_ECamera_WorldCoordsToScreen].Func(ECamera_WorldCoordsToScreen)( pThis, edx, param_1, param_2, bRealRes );
}

bool __fastcall EFileManager_Exists( EFileManager* pThis, void* edx, char* param_1, int iFileLoc )
{
	g_pFileManager = pThis;

	bool bExists = Hooks[Hook_EFileManager_Exists].Func(EFileManager_Exists)( pThis, edx, param_1, iFileLoc );

	if ( bExists == false && iFileLoc == 3 ) //Didn't find it, but we were looking at an unreliable location that might've missed native_assets
	{
		bool bExistsInOtherLocation = Hooks[Hook_EFileManager_Exists].Func(EFileManager_Exists)( pThis, edx, param_1, 1 );
		if ( bExistsInOtherLocation )
		{
			return true; //Game only checks file loc when checking if file exists. Other calls will find it in any loc, so just returning true here is enough.
		}
	}
	return bExists;
}

char g_pszLastRequestedFullPath[ MAX_PATH ];
char* __fastcall EFileManager_GetFullPath( EFileManager* pThis, void* edx, char* pszPath, int iFileLoc, void* param_3, void* param_4 )
{
	//If it starts with ModResource, then it's a path that's already been processed and thus is already the full path.
	if ( stristr( pszPath, "ModResource" ) )
	{
		return pszPath;
	}

	char pWorkString[ MAX_PATH ];
	int count = strlen( pszPath );

	strncpy_s( pWorkString, pszPath, count );

	hack_remove_workresource( pThis, pWorkString );

	char pszFixedSlashes[ MAX_PATH ];
	g_pModFS->FixSlashes( pWorkString, pszFixedSlashes );
	if ( pszFixedSlashes != nullptr )
	{
		if ( g_pModFS->ModFileExists( pszFixedSlashes ) )
		{
			if ( g_pModFS->GetModFilePath( pszFixedSlashes, g_pszLastRequestedFullPath ) )
			{
				LogMessage( CONFIG_VERBOSEFILEMANAGER, "EFileManager_GetFullPath: Redirecting %s to mod file %s\n", pszPath, g_pszLastRequestedFullPath );

				return g_pszLastRequestedFullPath;
			}
			else
			{
				LogMessage( CONFIG_VERBOSEFILEMANAGER, "[ERROR] EFileManager_GetFullPath failed to get real path for %s\n", pszFixedSlashes );
			}
		}
	}

	return Hooks[Hook_EFileManager_GetFullPath].Func(EFileManager_GetFullPath)( pThis, edx, pszPath, iFileLoc, param_3, param_4 );
}

void __fastcall EFileManager_AddReadPath( EFileManager* pThis, void* edx, const char* pszPath, bool bPrepend )
{
	Hooks[Hook_EFileManager_AddReadPath].Func(EFileManager_AddReadPath)( pThis, edx, pszPath, bPrepend );
	if ( stristr( pszPath, "WorkResource" ) )
	{
		//HACK: Lazy as hell way to make sure that we add native_assets as a read path after WorkResource
		LogMessage( "Registering native_assets as a read path\n" );
		Hooks[Hook_EFileManager_AddReadPath].Func(EFileManager_AddReadPath)( pThis, edx, "native_assets", false );
	}
}

FILE* __cdecl ISACTFileOpen( unsigned short* puszFileName )
{
	char pszFilename[ 1024 ];

	ISACTShortStringToCharString( puszFileName, pszFilename );

	const char* relFileName = g_pModFS->AfterFirstElement( pszFilename );

	if ( relFileName != nullptr )
	{
		char pszFixedSlashes[ MAX_PATH ];

		g_pModFS->FixSlashes( relFileName, pszFixedSlashes );
		if ( g_pModFS->ModFileExists( pszFixedSlashes ) )
		{	

			char pszModFilePath[ MAX_PATH ];
			if ( g_pModFS->GetModFilePath( pszFixedSlashes, pszModFilePath ) )
			{
				unsigned short puszModFilePath[ MAX_PATH ];

				LogMessage( "ISACTFileOpen: Redirecting %s to mod file %s\n", pszFilename, pszModFilePath );

				ISACTCharStringToShortString( pszModFilePath, puszModFilePath );
				return Hooks[Hook_ISACTFileOpen].Func(ISACTFileOpen)( puszModFilePath );
			}
			else
			{
				LogMessage( "[ERROR] ISACTFileOpen failed to get real path for %s\n", pszFixedSlashes );
			}

		}
	}

	return Hooks[Hook_ISACTFileOpen].Func(ISACTFileOpen)( puszFileName );
}

void __fastcall EPrefs_ReadPrefs( EPrefs* pThis, void* edx )
{
	Hooks[Hook_EPrefs_ReadPrefs].Func(EPrefs_ReadPrefs)( pThis, edx );

	LogMessage( "Forcing production loading off\n" );
	*(bool*)( pThis + 16 ) = 0;
}

void InitGlobals()
{
	LogMessage( "---LOCATING GLOBALS---\n" );

	//I dunno if this is the right way to do this but it works!
	hook::pattern patPushDIKeyboard( "68 ? ? ? ? E8 ? ? ? ? 83 C4 0C E8 ? ? ? ?" );
	if ( patPushDIKeyboard.count_hint( 1 ).empty() )
	{
		LogMessage( "Couldn't find pattern for c_dfDIKeyboard assignment!!\n" );
	}
	else
	{
		int* pAddr = (int*)patPushDIKeyboard.get( 0 ).get<char>( 1 );
		c_dfDIKeyboard = (unsigned char*)*pAddr;
		LogMessage( "c_dfDIKeyboard at %p\n", c_dfDIKeyboard );
	}

	hook::pattern patMovgEntities("C7 42 ? ? ? ? ? 8B 45 ? C7 40 ? FF FF FF FF 8B 4D ? 8A 55 ?");

	if (patMovgEntities.count_hint(1).empty())
	{
		LogMessage("Couldn't find pattern for gEntities assignment!!\n");
	}
	else
	{
		int pAddr = *(int*)patMovgEntities.get(0).get<char>(3);
		gEntities = (EEntityManager*)pAddr;
		gEntities_Proxy = new EEntityManagerProxy(gEntities);
		LogMessage("gEntities at %p\n", gEntities);
	}

	LogMessage( "---FINISHED LOCATING GLOBALS---\n" );
}

//Callables - these are functions in the game that we wanna be able to call from Astralathe but don't have any need to reason to actually hook at the moment
void FindFunctions()
{
	LogMessage( "---LOCATING CALLABLES---\n" );
	
	SetupFunction( lua_tostring, FROM_PATTERN( pLua_tostring ) );

	SetupFunction( lua_dofile, FROM_PATTERN( plua_dofile ) );

	SetupFunction( lua_getglobal, FROM_PATTERN( plua_getglobal ) );
	SetupFunction( lua_setglobal, FROM_PATTERN( plua_setglobal ) );

	SetupFunction( lua_type, FROM_PATTERN( plua_type ) );

	SetupFunction( lua_gettop, FROM_PATTERN( plua_gettop ) );
	SetupFunction( lua_settop, FROM_PATTERN( plua_settop ) );

	SetupFunction( lua_gettable, FROM_PATTERN( plua_gettable ) );
	SetupFunction( lua_settable, FROM_PATTERN( plua_settable ) );

	SetupFunction(lua_settag, FROM_PATTERN( plua_settag) );
	SetupFunction(lua_settagmethod, FROM_PATTERN( plua_settagmethod) );

	SetupFunction( lua_pushstring, FROM_PATTERN( plua_pushstring ) );
	SetupFunction( lua_pushlstring, FROM_PATTERN(plua_pushlstring) );
	SetupFunction( lua_pushvalue, FROM_PATTERN( plua_pushvalue ) );
	SetupFunction( lua_pushnumber, FROM_PATTERN( plua_pushnumber ) );
	SetupFunction( lua_pushnil, FROM_PATTERN( plua_pushnil ) );
	SetupFunction(lua_pushusertag, FROM_PATTERN(plua_pushusertag));
	SetupFunction(lua_pushcclosure, FROM_PATTERN(plua_pushcclosure));
	SetupFunction(lua_strlen, FROM_PATTERN(plua_strlen));

	SetupFunction(lua_newuserdata, FROM_PATTERN(plua_newuserdata));
	SetupFunction(lua_touserdata, FROM_PATTERN(plua_touserdata));

	SetupFunction(lua_tonumber, FROM_PATTERN(plua_tonumber));
	SetupFunction(lua_isnumber, FROM_PATTERN(plua_isnumber));


	SetupFunction(luaL_checkstack, FROM_PATTERN(pluaL_checkstack));
	SetupFunction(luaL_check_lstr, FROM_PATTERN(pluaL_check_lstr));
	SetupFunction(lua_error, FROM_PATTERN(plua_error));
	SetupFunction(luaL_argerror, FROM_PATTERN(pluaL_argerror));

	SetupFunction(getfilebyref, FROM_PATTERN(pgetfilebyref));

	SetupFunction(gethandle, FROM_PATTERN(pgethandle));

	SetupFunction(luaL_buffinit, FROM_PATTERN(pluaL_buffinit));
	SetupFunction(luaL_prepbuffer, FROM_PATTERN(pluaL_prepbuffer));
	SetupFunction(luaL_pushresult, FROM_PATTERN(pluaL_pushresult));

	SetupFunction(luaL_opt_number, FROM_PATTERN(pluaL_opt_number));

	SetupFunction( lua_call, FROM_PATTERN( plua_call ) );
	SetupFunction( lua_tag, FROM_PATTERN( plua_tag ) );
	SetupFunction( lua_newtag, FROM_PATTERN( plua_newtag ) );
	SetupFunction( lua_ref, FROM_PATTERN( plua_ref ) );
	SetupFunction( lua_unref, FROM_PATTERN( plua_unref ) );
	SetupFunction( lua_remove, FROM_PATTERN( plua_remove ) );

	SetupFunction( ISACTShortStringToCharString, FROM_PATTERN_MODULE( g_hISACT, pISACTShortStringToCharString ) );
	SetupFunction( ISACTCharStringToShortString, FROM_PATTERN_MODULE( g_hISACT, pISACTCharStringToShortString ) );

	SetupFunction( EArrayString_AddData, FROM_PATTERN( pEArrayString_AddData ) );

	SetupFunction( GameApp_CallFunctionf, FROM_PATTERN( pGameApp_CallFunctionf ) );
	SetupFunction( GameApp_SetShaderDiffuseColour, FROM_PATTERN(pGameApp_SetShaderDiffuseColour) );
	SetupFunction( GameApp_GetMainChannel, FROM_PATTERN(pGameApp_GetMainChannel) );

	SetupFunction( EUIMenu_StartUp, FROM_PATTERN( pEUIMenu_StartUp ) );

	SetupFunction( EScriptVM_FindScriptByName, FROM_PATTERN( pEScriptVM_FindScriptByName ) );
	SetupFunction( EScriptVM_GetScriptTableValueFromFloat, FROM_PATTERN(pEScriptVM_GetScriptTableValueFromFloat) );

	SetupFunction( EScriptObject_SendMessage, FROM_PATTERN( pEScriptObject_SendMessage ) );
	SetupFunction( EScriptObject_SetTableValue, FROM_PATTERN_IDX( pEScriptObject_SetTableValue_idx, pEScriptObject_SetTableValue ) );
	SetupFunction( EScriptObject_CallMethodf, FROM_PATTERN( pEScriptObject_CallMethodf ) );

	SetupFunction( EScriptVM_MarkContainedTablesAsReadOnly, FROM_PATTERN( pEScriptVM_MarkContainedTablesAsReadOnly ) );
	SetupFunction( EScriptVM_RunLuaCommands, FROM_PATTERN( PEScriptVM_RunLuaCommands ) );

	SetupFunction( EHashTableESVMBaseClassEntry_Find, FROM_PATTERN( pEHashTableESVMBaseClassEntry_Find ) );
	SetupFunction( EDataArrayESVMBaseClassEntry_ElementNew, FROM_PATTERN( pEDataArrayESVMBaseClassEntry_ElementNew ) );
	SetupFunction( EDataArrayESVMBaseClassEntry_ElementGet, FROM_PATTERN_IDX( 18, pEDataArrayESVMBaseClassEntry_ElementGet ) );

	SetupFunction( EGameTextureManager_DeleteTexture, FROM_PATTERN( pEGameTextureManager_DeleteTexture ) );
	SetupFunction( EMeshManager_UnloadMesh, FROM_PATTERN( pEMeshManager_UnloadMesh ) );

	SetupFunction( EMainPage_DrawMenu, FROM_PATTERN( pEMainPage_DrawMenu ) );

	SetupFunction( hack_remove_workresource, FROM_PATTERN( phack_remove_workresource ) );

	SetupFunction( MakeClassNameCanonical, FROM_PATTERN( pMakeClassNameCanonical ) );
	SetupFunction( ClassNameToFileName, FROM_PATTERN( pClassNameToFileName ) );
	SetupFunction( ClassNameToConstructorName, FROM_PATTERN( pClassNameToConstructorName ) );

	SetupFunction(_ERenderer::SetRenderState, FROM_PATTERN(_ERenderer::patSetRenderState));
	SetupFunction(_ERenderer::BeginPush, FROM_PATTERN(_ERenderer::patBeginPush));
	SetupFunction(_ERenderer::EndPush, FROM_PATTERN(_ERenderer::patEndPush));

	SetupFunction(_ECamera::BoxVisible, FROM_PATTERN(_ECamera::patBoxVisible));
	SetupFunction(_ECamera::CalculateScreenDiagonal, FROM_PATTERN(_ECamera::patCalculateScreenDiagonal));

	ClassProxyCallablesHelper::InitAllCallables();

	LogMessage( "---FINISHED LOCATING CALLABLES---\n" );
}

//TODO: I'd very much like for this to be defined elsewhere, probably Hooks.cpp
//Unfortunately almost every callback used by the hooks is lumped into this file and thus inaccessible from anywhere else.
//Fixing this would necessitate finally ripping all the shit that's been dumped into this file over the years into other places
//which is a hell of a task that I'm not up for right now.
std::unordered_map<eHook, HookDef> Hooks = {
	{ Hook_TraceMessageV, { "TraceMessageV", {"55 8B EC 81 EC 4C 04 00 00"}, &TraceMessageV } },
	{ Hook_TraceMessage, { "TraceMessage", {"55 8B EC 51 89 4D ? 8B 45 ? 83 38 00 74 ? 8B 4D ? 51 8B 55 ? 8B 02"}, &TraceMessage } },
	{ Hook_GetKeyboardInput, { "GetKeyboardInput", { "55 8B EC 83 EC 14 83 3D ? ? ? ? 00 75 ? E9 ? ? ? ?"}, &GetKeyboardInput } },
	{ Hook_Lua_DoCallStackDump, {"Lua_DoCallStackDump", {pLua_DoCallStackDump}, &Lua_DoCallStackDump } },
	{ Hook_Lua_ErrorHandler, {"Lua_ErrorHandler", {"55 8B EC 83 EC 2C 8B 45 ? 83 78 ? 00"}, &Lua_ErrorHandler } },

	{ Hook_Lua_openwithcontrol, {"openwithcontrol", {"55 8B EC 83 EC 08 6A 10 8B 45 ? 50"}, &LuaSecurity_openwithcontrol } },

	{ Hook_ISACTFileOpen, {"ISACTFileOpen", {"81 EC 04 04 00 00 A1 ? ? ? ?", 0, GetModuleHandle(L"isactwin.dll")}, &ISACTFileOpen}},

	{ Hook_EPrefs_ReadPrefs, {"EPrefs::ReadPrefs", {"55 8B EC 81 EC 68 04 00 00"}, &EPrefs_ReadPrefs}},

	{ Hook_GameApp_StartUp, {"GameApp::StartUp", { "55 8B EC 81 EC 64 01 00 00 89 8D ? ? ? ? 68 ? ? ? ?"}, &GameApp_StartUp}},
	{ Hook_GameApp_RenderFrame, {"GameApp::RenderFrame", {"55 8B EC 83 EC 20 89 4D ? 68 ? ? ? ? FF 15 ? ? ? ? 8B 45 ? 8A 88 ? ? ? ?"}, &GameApp_RenderFrame}},
	{ Hook_initUIMenu, {"GameApp::initUIMenu", {"55 8B EC 83 EC 08 89 4D ? 8B 45 ? 0F B6 88 ? ? ? ? 85 C9 75 ? 8B 55 ? 0F B6 82 ? ? ? ?"}, &GameApp_initUIMenu}},
	{ Hook_GameApp_UpdateCheatCodes, {"GameApp::UpdateCheatCodes", { "55 8B EC 83 EC 08 89 4D ? 8B 45 ? 0F B6 88 ? ? ? ? 85 C9 74 ? E9 ? ? ? ?" }, &GameApp_UpdateCheatCodes}},
	{ Hook_GameApp_PlayVideo, {"GameApp::PlayVideo", { "55 8B EC 81 EC 34 01 00 00 89 8D ? ? ? ? 68 ? ? ? ?" }, &GameApp_PlayVideo}},
	{ Hook_GameApp_LoadLocalizedTextBank, {"GameApp::LoadLocalizedTextBank", { pGameApp_LoadLocalizedTextBank }, &GameApp_LoadLocalizedTextBank}},
	{ Hook_GameApp_InitLua, {"GameApp::InitLua", { pGameApp_InitLua }, &GameApp_InitLua}},
	{ Hook_GameApp_SetRichPresence, {"GameApp::SetRichPresence", { "55 8B EC 83 EC 08 89 4D ? 83 7D ? 00 74 ? 8B 45 ? 89 45 ?" }, &GameApp_SetRichPresence}},

	{ Hook_EMainPage_RenderMenu, {"EMainPage::RenderMenu", {"55 8B EC 83 EC 14 89 4D ? 8B 45 ? 8B 88 ? ? ? ? 3B 4D ? 0F 85 ? ? ? ?"}, &EMainPage_RenderMenu} },

	{ Hook_ETutorialBoxManager_AddTutuorialBox, {"ETutorialBoxManager::AddTutuorialBox", {"55 8B EC 83 EC 4C 89 4D ? A1 ? ? ? ? 05 34 9A 00 00"}, &ETutorialBoxManager_AddTutuorialBox} },
	
	{ Hook_DFConsole_AddString, {"DFConsole::AddString", {"55 8B EC 83 EC 08 89 4D ? 83 7D ? 00 74 ? 8B 45 ? 0F BE 08"}, &DFConsole_AddString} },

	{ Hook_EScriptVM_Initialise, {"EScriptVM::Initialise", {"55 8B EC 83 EC 24 89 4D ? 6A 10"}, &EScriptVM_Initialise} },
	{ Hook_EScriptVM_GetBaseClass, {"EScriptVM::GetBaseClass", {"55 8B EC 81 EC 18 05 00 00"}, &EScriptVM_GetBaseClass} },
	{ Hook_EScriptVM_UnloadingLevel, {"EScriptVM::UnloadingLevel",  {"55 8B EC 83 EC 6C 89 4D ? 6A 00 6A 00"}, &EScriptVM_UnloadingLevel } },
	{ Hook_EScriptVM_SendStateMessageToAllScripts, {"EScriptVM::SendStateMessageToAllScripts",  {pEScriptVM_SendStateMessageToAllScripts}, &EScriptVM_SendStateMessageToAllScripts } },
	{ Hook_EScriptVM_DoFile, {"EScriptVM::DoFile",  {"55 8B EC 81 EC 80 01 00 00 89 8D ? ? ? ? A1 ? ? ? ? 0F B6 88 ? ? ? ?"}, &EScriptVM_DoFile } },

	{ Hook_EGameTextureManager_ReadPackFileTextures, {"EGameTextureManager::ReadPackFileTextures",
	{"55 8B EC 83 EC 38 89 4D ? C7 45 ? 00 00 00 00 EB ? 8B 45 ? 83 C0 01 89 45 ? 0F B7 4D ?"},
	&EGameTextureManager_ReadPackFileTextures } },
	{ Hook_EMeshManager_ReadPackFile, {"EMeshManager::ReadPackFile",  {"55 8B EC 81 EC 2C 01 00 00 89 8D ? ? ? ? 6A 04"}, &EMeshManager_ReadPackFile } },
	{ Hook_EScriptVM_ReadPackFile, {"EScriptVM::ReadPackFile",  {"55 8B EC 81 EC 38 04 00 00 89 8D ? ? ? ?"}, &EScriptVM_ReadPackFile } },
	{ Hook_EAnimManager_ReadPackFile, {"EAnimManager::ReadPackFile",  {"55 8B EC 81 EC 50 02 00 00 89 8D ? ? ? ? 6A 04"}, &EAnimManager_ReadPackFile } },

	{ Hook_lua_dobuffer, {"lua_dobuffer", {plua_dobuffer}, &lua_dobuffer } },

	{ Hook_EStream_StreamString2b,
	{"EStream::StreamString2b",
	{"55 8B EC 83 EC 24 89 4D ? 8B 45 ? 33 C9 83 78 ? 01 0F 94 C1 0F B6 D1 85 D2 74 ? 8B 45 ? 89 45 ? 8B 4D ? 83 C1 01 89 4D ? 8B 55 ? 8A 02 88 45 ? 83 45 ? 01 80 7D ? 00 75 ? 8B 4D ? 2B 4D ? 89 4D ? 8B 55 ? 89 55 ? 8B 45 ? 83 C0 01 66 89 45 ?"},
	&EStream_StreamString2b}},

	{ Hook_EDomain_LoadDomain,
	{"EDomain::LoadDomain",
	{"55 8B EC 83 EC 24 89 4D ? 6A 00 68 00 04 00 00 8B 45 ? 50 8B 0D ? ? ? ? 8B 11 8B 0D ? ? ? ? 8B 02 FF D0 89 45 ? 83 7D ? 00 75 ? 32 C0 EB ? C7 45 ? ? ? ? ? C7 45 ? 00 00 00 00 C6 45 ? 00 C7 45 ? ? ? ? ? 8B 4D ? 89 4D ? BA 01 00 00 00 83 EA 01 F7 DA 1B D2 83 C2 01 88 55 ? C7 45 ? ? ? ? ? 8B 45 ? 50 8D 4D ?"},
	&EDomain_LoadDomain}},
	{ Hook_EDomain_LoadSingleMesh, { "EDomain::LoadSingleMesh",
	{"55 8B EC 83 EC 24 89 4D ? 6A 00 68 00 04 00 00 8B 45 ? 50 8B 0D ? ? ? ? 8B 11 8B 0D ? ? ? ? 8B 02 FF D0 89 45 ? 83 7D ? 00 75 ? 33 C0"},
	&EDomain_LoadSingleMesh}},
	
	{ Hook_EHashTableUint_Find, {"EHashTable<uint>::Find", {"55 8B EC 83 EC 08 89 4D ? 6A 00 8B 45 ? 50 8B 4D ? E8 ? ? ? ? 89 45 ? 83 7D ? 00 75 ? 8B 45 ?"}, &EHashTableUint_Find} },
	{ Hook_EHashBase_Set, { "EHashBase::Set", {pEHashBase_Set}, &EHashBase_Set } },
	
	{ Hook_ECamera_WorldCoordsToScreen, { "ECamera::WorldCoordsToScreen", {pECamera_WorldCoordsToScreen}, &ECamera_WorldCoordsToScreen } },

	{ Hook_ERenderer_GetDisplayWidth, {"ERenderer:GetDisplayWidth", {"55 8B EC 51 89 4D ? B8 80 02 00 00"}, &GetDisplayWidth} },
	{ Hook_ERenderer_GetDisplayHeight, {"ERenderer:GetDisplayHeight", {"55 8B EC 51 89 4D ? B8 E0 01 00 00"}, &GetDisplayHeight} },

	{ Hook_EFileManager_Exists, { "EFileManager::Exists", {"55 8B EC 83 EC 10 89 4D ? C7 45 ? 00 00 00 00 C7 45 ? 00 00 00 00 8D 45 ? 50 8D 4D ?"}, &EFileManager_Exists } },
	{ Hook_EFileManager_GetFullPath, { "EFileManager::GetFullPath", {"55 8B EC 81 EC 54 01 00 00 89 8D ? ? ? ? 8B 45 ? 50"}, &EFileManager_GetFullPath } },
	{ Hook_EFileManager_AddReadPath, { "EFileManager::AddReadPath", {"55 8B EC 83 EC 54 89 4D ? 0F B6 45 ?"}, &EFileManager_AddReadPath } },

	{ Hook_EDebugLineManager_DrawAll, {"EDebugLineManager::DrawAll", {_EDebugLineManager::patGeneralD, _EDebugLineManager::DrawAllIdx}, &EDebugLineManagerProxy::DrawAll } },
	{ Hook_EDebugLineManager_AddLine, {"EDebugLineManager::AddLine", {_EDebugLineManager::patGeneralA, _EDebugLineManager::AddLineIdx}, &EDebugLineManagerProxy::AddLine } },
	{ Hook_EDebugLineManager_AddArrow, {"EDebugLineManager::AddArrow", {_EDebugLineManager::patGeneralA, _EDebugLineManager::AddArrowIdx}, &EDebugLineManagerProxy::AddArrow } },
	{ Hook_EDebugLineManager_AddBox, {"EDebugLineManager::AddBox", {_EDebugLineManager::patGeneralA, _EDebugLineManager::AddBoxIdx}, &EDebugLineManagerProxy::AddBox } },
	{ Hook_EDebugLineManager_AddCircle, {"EDebugLineManager::AddCircle", {_EDebugLineManager::patGeneralA, _EDebugLineManager::AddCircleIdx}, &EDebugLineManagerProxy::AddCircle } },
	{ Hook_EDebugLineManager_AddCylinder, {"EDebugLineManager::AddCylinder", {_EDebugLineManager::patAddCylinder, _EDebugLineManager::AddCylinderIdx}, &EDebugLineManagerProxy::AddCylinder } },
	{ Hook_EDebugLineManager_AddSphere, {"EDebugLineManager::AddSphere", {_EDebugLineManager::patAddSphere, _EDebugLineManager::AddSphereIdx}, &EDebugLineManagerProxy::AddSphere } },
	{ Hook_EDebugLineManager_ResetFrame, {"EDebugLineManager::ResetFrame", {_EDebugLineManager::patGeneralD, _EDebugLineManager::ResetFrameIdx}, &EDebugLineManagerProxy::ResetFrame } },
	
	{ Hook_EDebugTriManager_AddTri, {"EDebugTriManager::AddTri", {_EDebugTriManager::patAddTri, _EDebugTriManager::AddTriIdx}, &EDebugTriManagerProxy::AddTri} },
	{ Hook_EDebugTriManager_DrawAll, {"EDebugTriManager::DrawAll", {_EDebugTriManager::patGeneric, _EDebugTriManager::DrawAllIdx}, &EDebugTriManagerProxy::DrawAll} },
	{ Hook_EDebugTriManager_ResetFrame, {"EDebugTriManager::ResetFrame", {_EDebugTriManager::patGeneric, _EDebugTriManager::ResetFrameIdx}, &EDebugTriManagerProxy::ResetFrame} },
};

//https://github.com/ThirteenAG/WidescreenFixesPack/blob/master/source/Psychonauts.WidescreenFix/dllmain.cpp#L42
struct MemFloat
{
	float a01 = 1.0f;
	float a02 = 0.0f;
	float a03 = 0.0f;
	float a04 = 0.0f;
	float a05 = 0.0f;
	float a06 = 1.0f;
	float a07 = 0.0f;
	float a08 = 0.0f;
	float a09 = 0.0f;
	float a10 = 0.0f;
	float a11 = 1.0f;
	float a12 = 0.0f;
	float a13 = 0.0f;
	float a14 = 0.0f;
	float a15 = 0.0f;
	float a16 = 1.0f;
} flt_7933E0;
//https://github.com/ThirteenAG/WidescreenFixesPack/blob/master/source/Psychonauts.WidescreenFix/dllmain.cpp#L20
class CRect
{
public:
	float m_fLeft;
	float m_fBottom;
	float m_fRight;
	float m_fTop;

	bool operator==( const CRect& rect ) {
		return ( ( *(uint32_t*)&this->m_fLeft == *(uint32_t*)&rect.m_fLeft ) && ( *(uint32_t*)&this->m_fBottom == *(uint32_t*)&rect.m_fBottom ) &&
			( *(uint32_t*)&this->m_fRight == *(uint32_t*)&rect.m_fRight ) && ( *(uint32_t*)&this->m_fTop == *(uint32_t*)&rect.m_fTop ) );
	}

	inline CRect() {}
	inline CRect( float a, float b, float c, float d )
		: m_fLeft( a ), m_fBottom( b ), m_fRight( c ), m_fTop( d )
	{}
	inline CRect( uint32_t a, uint32_t b, uint32_t c, uint32_t d )
		: m_fLeft( *(float*)&a ), m_fBottom( *(float*)&b ), m_fRight( *(float*)&c ), m_fTop( *(float*)&d )
	{}
};

#define MAKE_INJECTOR_HOOK(name, pat) LogMessage(#name "\n"); \
										  pattern = hook::pattern(pat); \
										  struct name

class InstallHooksForClass : public ModFS::IForEachInSearchPath
{
public:
	virtual void Operate(std::filesystem::path Path) override
	{
		//LogMessage("wowee look at me I'm hooking the functions from %s!\n", Path.string().c_str());

		LogMessage("Loading hooks from path: %s\n", Path.string().c_str());

		lua_State* L = GetLuaState();
		int stack = lua_gettop(L);

		//Hooks function is expected to be named [classname]_hooks
		//Accepts 1 argument: the prototype of the hooked script class

		if (lua_dofile(L, Path.string().c_str()))
		{
			lua_settop(L, stack);
			return;
		}

		std::string hookFuncName = Path.stem().string();

		lua_getglobal(L, hookFuncName.c_str());
		lua_pushvalue(L, -2);
		lua_call(L, 1, 0);

		lua_pushnil(L);
		lua_setglobal(L, hookFuncName.c_str());

		lua_settop(L, stack);
	}
};

static bool bWidescreenForceAllowFullscreen = false; //HACK

//TODO: need to consolidate this with our other hooks system
void ApplyInjectorHooks()
{
	LogMessage( "---APPLYING INJECTOR HOOKS---\n" );

	hook::pattern pattern;

	//NEW LUA HOOKING SYSTEM
	//Takes advantage of the fact script entities are initially loaded as base class prototypes
	//These 2 hooks intercept the game just before the 2 code paths where it adds these protos
	//to the internal base class list. Mods can directly modify the prototypes before they
	//get added.
	//EScriptVM::ReadPackFile
	struct BaseClassHooker1
	{
		void operator()(injector::reg_pack& regs)
		{
			std::string classname = (char*)(regs.ebp - 0x110);
			std::replace(classname.begin(), classname.end(), '.', '/');
			classname.append("_hooks.lua");
			std::filesystem::path hookPath = std::filesystem::path("Scripts/") / classname;

			InstallHooksForClass installer;
			g_pModFS->ForEachInSearchPaths(hookPath, installer);

			//mov    eax,DWORD PTR [ebp-0x438]
			regs.eax = *(uint32_t*)(regs.ebp - 0x438);
		}
	};

	//EScriptVM::GetBaseClass
	struct BaseClassHooker2
	{
		void operator()(injector::reg_pack& regs)
		{
			std::string classname = (char*)(regs.ebp - 0x400);
			std::replace(classname.begin(), classname.end(), '.', '/');
			classname.append("_hooks.lua");
			std::filesystem::path hookPath = std::filesystem::path("Scripts/") / classname;

			InstallHooksForClass installer;
			g_pModFS->ForEachInSearchPaths(hookPath, installer);

			//mov    edx,DWORD PTR [ebp-0x518]
			regs.edx = *(uint32_t*)(regs.ebp - 0x518);
		}
	};
	pattern = hook::pattern("8B 85 ? ? ? ? 8B 88 ? ? ? ? E8 ? ? ? ? 50");
	injector::MakeInline<BaseClassHooker1>(pattern.get_first(0), pattern.get_first(6));
	pattern = hook::pattern("8B 95 ? ? ? ? 8B 8A ? ? ? ? E8 ? ? ? ? 50 8B 85 ? ? ? ?");
	injector::MakeInline<BaseClassHooker2>(pattern.get_first(0), pattern.get_first(6));

	//TODO config option
	pattern = hook::pattern("C7 84 24 ? ? ? ? 01 00 00 00 0F B6 84 24 ? ? ? ? 85 C0 0F 84 ? ? ? ?");
	struct FreeCamHook
	{
		void operator()(injector::reg_pack& regs)
		{
			uint32_t showFlyText = g_pGameAppProxy->m_bHideHUD.GetValue() ? 0x0 : 0x1;
			*(uint32_t*)(regs.esp + 0x424) = showFlyText;
		};
	}; injector::MakeInline<FreeCamHook>(pattern.get_first(0), pattern.get_first(11));

	if (AstralatheConfig::GetConfigValue(CONFIG_FIX_FX))
	{
		struct FrameTexHook1
		{
			void operator()(injector::reg_pack& regs)
			{
				regs.edx = GetDisplayWidth();
			}
		};
		struct FrameTexHook2
		{
			void operator()(injector::reg_pack& regs)
			{
				regs.edx = GetDisplayHeight();
			}
		};

		pattern = hook::pattern("BA 80 02 00 00");
		injector::MakeInline<FrameTexHook1>(pattern.get_first(0), pattern.get_first(5));

		pattern = hook::pattern("BA E0 01 00 00");
		injector::MakeInline<FrameTexHook2>(pattern.get_first(0), pattern.get_first(5));
	}

	//See #10
	//tl;dr emo baggage uses screen res to centre the spinny thing but the hud is always 640x480
	//so it's inadvertently always assuming screen res is 640x480 and needs to be for the code to work
	//so we just ignore the result of GetRenderWidth/Height and hardcode 640x480
	//TODO: Can probably replace the calls to GetRenderWidth/Height entirely.
	LogMessage("EmoBaggageHook1\n");
	pattern = hook::pattern("89 85 ? ? ? ? C7 85 ? ? ? ? 00 00 00 00 DF AD ? ? ? ? DC 0D ? ? ? ? D9 9D ? ? ? ? 8B 95 ? ? ? ?");
	struct EmoBaggageHook1
	{
		void operator()(injector::reg_pack& regs)
		{
			regs.eax = 640;
			*(uint32_t*)(regs.ebp - 0x170) = regs.eax;
		}
	}; injector::MakeInline<EmoBaggageHook1>(pattern.get_first(0), pattern.get_first(6)); //0x50f4cb

	LogMessage("EmoBaggageHook2\n");
	pattern = hook::pattern("89 85 ? ? ? ? C7 85 ? ? ? ? 00 00 00 00 DF AD ? ? ? ? DC 0D ? ? ? ? D9 9D ? ? ? ? D9 85 ? ? ? ?");
	struct EmoBaggageHook2
	{
		void operator()(injector::reg_pack& regs)
		{
			regs.eax = 480;
			*(uint32_t*)(regs.ebp - 0x178) = regs.eax;
		}
	}; injector::MakeInline<EmoBaggageHook2>(pattern.get_first(0), pattern.get_first(6)); //0x50f4fe

	//if ( AstralatheConfig::GetConfigValue( CONFIG_FIX_WIDESCREEN ) )
	{
		//https://github.com/ThirteenAG/WidescreenFixesPack/blob/master/source/Psychonauts.WidescreenFix/dllmain.cpp
		//Quite frankly, my brain is too smooth to understand a lot of what the original is doing
		LogMessage( "ResHook\n" );
		pattern = hook::pattern( "8B 82 C8 02 00 00 89 81 94 05 00 00 8B 4D F8" );
		struct ResHook
		{
			void operator()(injector::reg_pack& regs)
			{
				*(int*)(regs.ecx + 0x594) = *(int*)(regs.edx + 0x2C8);
				ScreenInfo.nWidth = *(int*)(regs.ecx + 0x590);
				ScreenInfo.nHeight = *(int*)(regs.ecx + 0x594);

				ScreenInfo.fWidth = static_cast<float>(ScreenInfo.nWidth);
				ScreenInfo.fHeight = static_cast<float>(ScreenInfo.nHeight);
				ScreenInfo.fAspectRatio = ScreenInfo.fWidth / ScreenInfo.fHeight;
				ScreenInfo.nWidth43 = static_cast<int>( ScreenInfo.fHeight * ( 4.0f / 3.0f ) );
				ScreenInfo.fWidth43 = static_cast<float>( ScreenInfo.nWidth43 );
				ScreenInfo.fHudOffset = ( ( 480.0f * ScreenInfo.fAspectRatio ) - 640.0f ) / 2.0f;
				ScreenInfo.fHudOffsetReal = ( ScreenInfo.fWidth - ScreenInfo.fHeight * ( 4.0f / 3.0f ) ) / 2.0f;
				ScreenInfo.fHudScale = 1.0f / ( ( ( 4.0f / 3.0f ) ) / ( ScreenInfo.fAspectRatio ) );
				ScreenInfo.fHudScale2 = 640.0f / ( ( ( 4.0f / 3.0f ) ) / ( ScreenInfo.fAspectRatio ) );
				ScreenInfo.fHudOffsetWide = AstralatheConfig::GetConfigValue<float>(CONFIG_WIDESCREEN_HUDOFFSET);
				if ( ScreenInfo.fAspectRatio < ( 16.0f / 9.0f ) )
					ScreenInfo.fHudOffsetWide = AstralatheConfig::GetConfigValue<float>(CONFIG_WIDESCREEN_HUDOFFSET) / ( ( ( 16.0f / 9.0f ) / ( ScreenInfo.fAspectRatio ) ) * 1.5f );
			
				g_pModFS->HandleWidescreenMounting();
			}
		}; injector::MakeInline<ResHook>( pattern.get_first( 0 ), pattern.get_first( 12 ) ); //0x67F958
		
		LogMessage( "HudHook\n" );
		pattern = hook::pattern( "8B 0D ? ? ? ? 89 4D E0 8B 15 ? ? ? ? 89 55 E4 D9 05" );
		struct HudHook
		{
			void operator()( injector::reg_pack& regs )
			{
				if (Astralathe::Get().ShouldRunWidescreen())
				{
					*(float*)(regs.ebp - 0x20) = ScreenInfo.fHudOffset; //x
					*(float*)(regs.ebp - 0x1C) = 0.0f;              //y
					*(float*)(regs.ebp - 0x14) = ScreenInfo.fHudScale2; //w
					*(float*)(regs.ebp - 0x10) = 480.0f;            //h
				}
				else
				{
					*(float*)(regs.ebp - 0x20) = 0.0f; //x
					*(float*)(regs.ebp - 0x1C) = 0.0f; //y
					*(float*)(regs.ebp - 0x14) = 640.0f; //w
					*(float*)(regs.ebp - 0x10) = 480.0f; //h
				}
			}
		};
		injector::MakeInline<HudHook>( pattern.get_first( 0 ), pattern.get_first( 36 ) ); //0x47D5B8, 0x47D5DC

		static auto dw_61F2EF = hook::get_pattern( "EB ? 8B 85 14 FF FF FF 8B 08 E8", 0 );// 0x0061F2EF;
		static auto dw_61EA8F = hook::get_pattern( "8B 95 14 FF FF FF 8B 4A 1C E8 ? ? ? ? 8B", 0 ); // 0x0061EA8F
		static auto dw_616FB7 = hook::get_pattern( "8B 45 C4 83 C0 01 89 45 C4 8B 4D C8", 0 ); // 0x00616FB7
		static auto dw_4FD37D = hook::get_pattern( "8B 8D 28 FA FF FF 83 79 18 00 74", 0 ); // 0x004FD37D
		static auto dw_61EAC4 = hook::get_pattern( "8B 8D 14 FF FF FF 8B 51 10 8B 85 14", 0 ); // 0x0061EAC4
		static auto dw_61EADD = hook::get_pattern( "8B 8D 14 FF FF FF 8B 51 0C 8B 85 14", 0 ); // 0x0061EADD
		static auto dw_61EAF6 = hook::get_pattern( "C7 45 D8 00 00 00 00 8B 0D ? ? ? ? 8B 91 8C", 0 ); // 0x0061EAF6
		static auto dw_61F2FE = hook::get_pattern( "8B 85 14 FF FF FF 8B 08 E8 ? ? ? ? 8B E5 5D C3", 13 ); // 0x0061F2FE
		static auto dw_673413 = hook::get_pattern( "D9 41 08 D9 1C 24 8B 4D FC E8 ? ? ? ? 8B E5 5D C3", 14 ); // 0x00673413
		static auto dw_61F19D = hook::get_pattern( "8B 95 14 FF FF FF 8B 4A 28 E8", 0 ); // 0x0061F19D

		//For emo baggage collection
		//0x50f69d
		static auto dw_requestRenderPsychedelic_start = hook::get_pattern("D9 45 ? D8 65 ? D9 45 ? DC 0D ? ? ? ?", 0);
		//0x50f927
		static auto dw_requestRenderPsychedelic_end = hook::get_pattern("D9 45 ? D8 65 ? D9 45 ? DC 05 ? ? ? ? D9 9D ? ? ? ? D9 85 ? ? ? ? DC 0D ? ? ? ? D9 C9 DD 9D ? ? ? ? E8 ? ? ? ? D9 9D ? ? ? ? D9 85 ? ? ? ? DC 8D ? ? ? ? D8 45 ? D9 5D ? D9 45 ? DC 05 ? ? ? ? D9 9D ? ? ? ? D9 85 ? ? ? ? 51 D9 1C 24 E8 ? ? ? ? 83 C4 04 D9 5D ? D9 45 ? D8 4D ? D9 9D ? ? ? ? D9 45 ? D9 9D ? ? ? ? 6A 00 6A 00 6A 04 6A 00 51 D9 45 ? D9 1C 24 8D 8D ? ? ? ?", 0xE5);


		LogMessage( "flt_7933E0\n" );
		pattern = hook::pattern( "BE ? ? ? ? 8D BC 24 00 01 00 00 F3 A5 83 7D 20 00" );
		injector::WriteMemory( pattern.get_first( 1 ), &flt_7933E0, true ); //0x47CC71 + 1

		LogMessage( "VaultHook1\n" );
		pattern = hook::pattern( "89 8D ? ? ? ? A1 ? ? ? ? 8B 88 ? ? ? ? 83 E9 01 51 8B 0D ? ? ? ? 81 C1 0C 02 00 00 E8 ? ? ? ? 89 85 ? ? ? ? 8B 95 ? ? ? ? 8B 42 ? 89 45 ? C7 45 ? 00 00 00 00" );
		struct VaultHook1
		{
			void operator()( injector::reg_pack& regs )
			{
				*(uint32_t*)( regs.ebp - 0x12c ) = regs.ecx;

				bWidescreenForceAllowFullscreen = true; //HACK but wasn't sure how else to achieve this. Input welcome
			}
		}; injector::MakeInline<VaultHook1>( pattern.get_first( 0 ), pattern.get_first(6) );

		LogMessage( "VaultHook2\n" );
		pattern = hook::pattern( "8B 8D ? ? ? ? E8 ? ? ? ? 89 45 ? 8B 8D ? ? ? ? E8 ? ? ? ? 89 85 ? ? ? ? 8B 95 ? ? ? ?" );
		struct VaultHook2
		{
			void operator()( injector::reg_pack& regs )
			{
				regs.ecx = *(uint32_t*)(regs.ebp - 0x12c);

				bWidescreenForceAllowFullscreen = false;
			}
		}; injector::MakeInline<VaultHook2>( pattern.get_first( 0 ), pattern.get_first( 6 ) );

		//AddSprite
		LogMessage( "HudHook2\n" );
		pattern = hook::pattern( "89 8C 24 AC 01 00 00 83 7D 08 00" );
		struct HudHook2
		{
			const CRect fs = CRect( 0.0f, 0.0f, 640.0f, 480.0f ); // fullscreen images
			const CRect fl = CRect( 0.0f, 0.0f, 20.0f, 15.0f );   // strike flash
			const CRect bvfl = CRect( 0.0f, 0.0f, 2.5f, 1.875f );   // strike flash (black velvetopia)

			void operator()( injector::reg_pack& regs )
			{
				*(uint32_t*)( regs.esp + 0x1AC ) = regs.ecx;

				flt_7933E0.a01 = 1.0f;
				flt_7933E0.a13 = 0.0f;

				if (!Astralathe::Get().ShouldRunWidescreen())
					return;

				auto ptr = *(uint32_t*)( regs.ebp + 0x24 );
				if ( ptr )
				{
					CRect* r = (CRect*)( ptr - 0x08 );

					if ( *r == fs || *r == fl || *r == bvfl || bWidescreenForceAllowFullscreen )
					{
						flt_7933E0.a01 = ScreenInfo.fHudScale;
						flt_7933E0.a13 = -ScreenInfo.fHudOffset;
					}
					else
					{
						//This block tries to figure out where we are in terms of HUD rendering by figuring out who
						//called into AddSprite by checking the return address.
						static auto idx = 0; // seems like 4 in debug build and 3 in release
						static void* stack[ 6 ];
						CaptureStackBackTrace( 0, 6, stack, NULL );

						auto it = std::find_if(std::begin(stack), std::end(stack), [](void* n) { return n >= dw_requestRenderPsychedelic_start && n <= dw_requestRenderPsychedelic_end; });
						if (it != std::end(stack))
						{
							//Emo baggage sequence should render fullscreen
							flt_7933E0.a01 = ScreenInfo.fHudScale;
							flt_7933E0.a13 = -ScreenInfo.fHudOffset;
						}

						it = std::find_if( std::begin( stack ), std::end( stack ), []( void* n ) { return n == dw_61F2EF || n == dw_61F2FE; } );
						if ( !idx && it != std::end( stack ) )
							idx = std::distance( std::begin( stack ), it );

						if ( idx )
						{
							if ( stack[ idx ] == dw_61F2EF || stack[ idx + 1 ] == dw_61F2EF || stack[ idx ] == dw_61F2FE || stack[ idx + 1 ] == dw_61F2FE )
							{
								flt_7933E0.a13 += ScreenInfo.fHudOffsetWide;
							}
							else
							{
								if ( stack[ idx ] == dw_61EA8F || stack[ idx ] == dw_616FB7 || stack[ idx ] == dw_4FD37D )
								{
									flt_7933E0.a13 -= ScreenInfo.fHudOffsetWide;
								}
								else
								{
									if ( stack[ idx ] == dw_61EAC4 || stack[ idx ] == dw_61EAF6 || stack[ idx ] == dw_61EADD || stack[ idx ] == dw_673413 || stack[ idx ] == dw_61F19D )
									{
										flt_7933E0.a13 += ScreenInfo.fHudOffsetWide;
									}
								}
							}
						}
					}
				}
			}
		}; injector::MakeInline<HudHook2>( pattern.get_first( 0 ), pattern.get_first( 7 ) ); //0x47C7CE, 0x47C7CE+7

		//TODO: this is half baked but could be a more robust fix for screenfades
		/*
		pattern = hook::pattern("C7 85 ? ? ? ? 80 02 00 00 C7 45 ? E0 01 00 00");
		struct ScreenFadeHook
		{
			void operator()( injector::reg_pack& regs )
			{
				*(uint32_t*)(regs.ebp - 0x94) = ScreenInfo.nWidth;
				*(uint32_t*)(regs.ebp - 0x4) = ScreenInfo.nHeight;
			}
		};
		injector::MakeInline<ScreenFadeHook>(pattern.get_first(0), pattern.get_first(17));
		*/

		LogMessage( "MoviesHook\n" );
		//FMVs
		pattern = hook::pattern( "8B 8D 68 FF FF FF E8 ? ? ? ? 6A 00 6A 00 8B 85 68 FF FF FF" );
		struct MoviesHook
		{
			void operator()( injector::reg_pack& regs )
			{
				regs.ecx = *(uint32_t*)( regs.ebp - 0x98 );
				if (!Astralathe::Get().ShouldRunWidescreen())
					return;

				**(float**)( regs.esp + 0x10 ) = 1.0f / ScreenInfo.fHudScale;  //w
				**(float**)( regs.esp + 0x0C ) = ScreenInfo.fHudOffsetReal;    //x
			}
		}; injector::MakeInline<MoviesHook>( pattern.get_first( 0 ), pattern.get_first( 6 ) ); //0x4891EF
	}

	LogMessage( "---FINISHED APPLYING INJECTOR HOOKS---\n" );
}

void Astralathe::Startup()
{
	SetupConsole();

	g_hISACT = GetModuleHandle( L"isactwin.dll" );

	LogInit( "astralathe.log" );

	LogMessage( "Starting up!\n" );

	InitCrashHandling();

	AstralatheConfig::Init();

	memset( g_KeysPressed, 0, 256 );
	memset( g_KeysDown, 0, 256 );
	InitGlobals();
	FindFunctions();

	SetupHooks();
	ApplyInjectorHooks();
	ApplyDXHooks();
	InstallSteamHooks();

	if (!AstralatheConfig::GetConfigValue(CONFIG_ENHANCEMENT_DISCORDRP))
		m_DiscordManager.Disable();

	ModFS::Init();

#ifdef _DEBUG
	std::system( "pause" );
#endif
}

void Astralathe::Shutdown()
{
	m_DiscordManager.Shutdown();
	
	for (auto h : Hooks)
	{
		h.second.UnHook();
	}
}

//Returns true if the game is running in a widescreen or above resolution (or should be treated as such)
bool Astralathe::ShouldRunWidescreen()
{
	if (!AstralatheConfig::GetConfigValue(CONFIG_WIDESCREEN_AUTODETECT))
		return AstralatheConfig::GetConfigValue(CONFIG_FIX_WIDESCREEN);

	if (g_pGameAppProxy == nullptr || !g_pGameAppProxy->IsValid())
		return false;

	ERendererProxy pRend(g_pGameAppProxy->m_pRenderer.GetValue());
	if (!pRend.IsValid())
		return false;

	const float widescreenRatio = 16.0f / 9.0f;

	float ratio = (float)pRend.m_iWidth.GetValue() / (float)pRend.m_iHeight.GetValue();
	return ratio >= widescreenRatio;
}

void Astralathe::SetupConsole()
{
	InitConsole();

	SetWindowText( GetConsoleWindow(), L"Astralathe Output" );
}

void Astralathe::SetupHooks()
{
	LogMessage( "---HOOKING FUNCTIONS---\n" );
	for ( auto& h : Hooks )
	{
		h.second.Hook();
	}
	LogMessage( "---FUNCTIONS HOOKED---\n" );
}