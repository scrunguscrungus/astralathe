#pragma once
void InstallSteamHooks();
void AstralatheSteam_TimelineEvent(const char* pchIcon,
	const char* pchTitle,
	const char* pchDescription,
	float flStartedOffset,
	float flLength,
	bool bFeatured);