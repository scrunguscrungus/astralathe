/*
* On Windows, when USE_STEAM is defined, this incredibly cursed file will intercept the game's attempts to use the Steam API
* to instead call functions inside "AstralatheSteam.dll".
* 
* AstralatheSteam.dll itself is just a vanilla redist binary from a newer version of the Steamworks SDK (v1.60 at the time of writing)
* with features that aren't available in v1.16 (which is 12 years old and is the version used by Psychonauts by default).
* 
* Doing it this way lets us essentially graft the new API DLL onto the game without making any permanent alterations.
* (And without having to link against Steam ourselves, which is a nice bonus)
* 
* Surprisingly, Valve nailed the backwards compatibility such that handing the new interfaces to the game when it
* tries to get them just kind of works.
* 
* This code will do nothing under certain conditions:
* - AstralatheSteam.dll is not found.
* - steam_api.dll does not exist (likely running the GOG or MS Store release)
* - You build Astralathe yourself without USE_STEAM or outside of Windows.
* 
* In the first 2 cases, relevant functions will exit early.
* In the third case, the functions defined in SteamHooks.h will become stubs with no functionality.
* 
* You don't need to do anything special to build with Steam support - you just need AstralatheSteam.dll which is included
* in this repo.
*/

#if defined(USE_STEAM) && defined(WIN32)

#include "windows.h"

#include <polyhook2/Detour/x86Detour.hpp>
#include <polyhook2/PE/IatHook.hpp>

#include "logging.h"
#include "AstralatheConfig.h"
#include "SteamHooks.h"

HMODULE OurSteamModule;

#define TrySetup(func, receiver) {\
if (OurSteamModule == NULL) { return; }\
FARPROC function = GetProcAddress(OurSteamModule, func); \
if ( !receiver ) { \
receiver = PLH::FnCast((char*)function, receiver); \
if ( !receiver ) { return; } \
} \
} \

#define TrySetup2(func, receiver) {\
if (OurSteamModule == NULL) { return false; }\
FARPROC function = GetProcAddress(OurSteamModule, func); \
if ( !receiver ) { \
receiver = PLH::FnCast((char*)function, receiver); \
if ( !receiver ) { return false; } \
} \
} \

#define TrySetup3(func, receiver) {\
if (OurSteamModule == NULL) { return nullptr; }\
FARPROC function = GetProcAddress(OurSteamModule, func); \
if ( !receiver ) { \
receiver = PLH::FnCast((char*)function, receiver); \
if ( !receiver ) { return nullptr; } \
} \
} \

namespace SteamCallables
{
	typedef int(__cdecl* tSteamAPI_Init)(void);
	tSteamAPI_Init SteamAPI_Init2;

	typedef int(__cdecl* tSteamAPI_InitSafe)(void);
	tSteamAPI_InitSafe SteamAPI_InitSafe;

	typedef void(__cdecl* tSteamAPI_RegisterCallback)(void*, int);
	tSteamAPI_RegisterCallback SteamAPI_RegisterCallback;

	typedef void(__cdecl* tSteamAPI_UnregisterCallback)(void*);
	tSteamAPI_UnregisterCallback SteamAPI_UnregisterCallback;

	typedef void(__cdecl* tSteamAPI_RunCallbacks)(void);
	tSteamAPI_RunCallbacks SteamAPI_RunCallbacks;

	typedef void(__cdecl* tSteamAPI_WriteMiniDump)(int, void*, int);
	tSteamAPI_WriteMiniDump SteamAPI_WriteMiniDump;

	typedef void(__cdecl* tSteamAPI_Shutdown)(void);
	tSteamAPI_Shutdown SteamAPI_Shutdown;

	typedef void* (*tSteamAPI_GetInterface)(void);
	tSteamAPI_GetInterface SteamAPI_SteamFriends_v017;
	tSteamAPI_GetInterface SteamAPI_SteamUserStats_v012;
	tSteamAPI_GetInterface SteamAPI_SteamRemoteStorage_v016;

	class ISteamTimeline;

	typedef ISteamTimeline* (*tSteamAPI_SteamTimeline_v001)(void);
	tSteamAPI_SteamTimeline_v001 SteamAPI_SteamTimeline_v001;

	typedef void(*tSteamAPI_ISteamTimeline_AddTimelineEvent)(ISteamTimeline*, const char*, const char*, const char*, int, float, float, int);
	namespace SteamTimeline
	{
		tSteamAPI_ISteamTimeline_AddTimelineEvent AddTimelineEvent;
	}
}

bool __cdecl AstralatheSteamAPI_Init()
{
	TrySetup2("SteamAPI_InitSafe", SteamCallables::SteamAPI_InitSafe);
	return SteamCallables::SteamAPI_InitSafe();
}

void __cdecl AstralatheSteamAPI_RegisterCallback(void* pCallback, int iCallback)
{
	TrySetup("SteamAPI_RegisterCallback", SteamCallables::SteamAPI_RegisterCallback);

	SteamCallables::SteamAPI_RegisterCallback(pCallback, iCallback);
}

void __cdecl AstralatheSteamAPI_UnregisterCallback(void* pCallback)
{
	TrySetup("SteamAPI_UnregisterCallback", SteamCallables::SteamAPI_UnregisterCallback);

	SteamCallables::SteamAPI_UnregisterCallback(pCallback);
}

void __cdecl AstralatheSteamAPI_RunCallbacks()
{
	TrySetup("SteamAPI_RunCallbacks", SteamCallables::SteamAPI_RunCallbacks);

	SteamCallables::SteamAPI_RunCallbacks();
}

void __cdecl AstralatheSteamAPI_WriteMiniDump(int uStructuredExceptionCode, void* pvExceptionInfo, int uBuildID)
{
	TrySetup("SteamAPI_WriteMiniDump", SteamCallables::SteamAPI_WriteMiniDump);

	SteamCallables::SteamAPI_WriteMiniDump(uStructuredExceptionCode, pvExceptionInfo, uBuildID);
}

void* AstralatheSteamFriends()
{
	TrySetup3("SteamAPI_SteamFriends_v017", SteamCallables::SteamAPI_SteamFriends_v017);

	return SteamCallables::SteamAPI_SteamFriends_v017();
}

void* AstralatheSteamUserStats()
{
	TrySetup3("SteamAPI_SteamUserStats_v012", SteamCallables::SteamAPI_SteamUserStats_v012);

	return SteamCallables::SteamAPI_SteamUserStats_v012();
}

void* AstralatheSteamRemoteStorage()
{
	TrySetup3("SteamAPI_SteamRemoteStorage_v016", SteamCallables::SteamAPI_SteamRemoteStorage_v016);

	return SteamCallables::SteamAPI_SteamRemoteStorage_v016();
}

void __cdecl AstralatheSteamAPI_Shutdown()
{
	TrySetup("SteamAPI_Shutdown", SteamCallables::SteamAPI_Shutdown);

	SteamCallables::SteamAPI_Shutdown();
}

#define TryHook(theHook) if (theHook->hook() ) { \
LogMessage(#theHook " success!\n"); \
} else { \
LogMessage(#theHook " failed!\n"); \
} \

#endif

void InstallSteamHooks()
{
#if defined(USE_STEAM) && defined(WIN32)

	if (!AstralatheConfig::GetConfigValue(CONFIG_USE_STEAM))
	{
		LogMessage("Steam support disabled by CONFIG_USE_STEAM.\n");
		return;
	}

	if (!std::filesystem::exists("steam_api.dll"))
	{
		LogMessage("Steam support disabled.");
		return;
	}

	LogMessage("Installing Steam hooks...\n");
	OurSteamModule = LoadLibraryA("AstralatheSteam.dll");
	if (OurSteamModule == NULL)
	{
		LogMessage("Failed to load AstralatheSteam.dll!");
		return;
	}

	uint64_t discard;

	PLH::IatHook* initHook = new PLH::IatHook("steam_api.dll", "SteamAPI_Init", (char*)&AstralatheSteamAPI_Init, &discard, L"Psychonauts.exe");
	TryHook(initHook);

	PLH::IatHook* registerCallbackHook = new PLH::IatHook("steam_api.dll", "SteamAPI_RegisterCallback", (char*)&AstralatheSteamAPI_RegisterCallback, &discard, L"Psychonauts.exe");
	TryHook(registerCallbackHook);

	PLH::IatHook* unregisterCallbackHook = new PLH::IatHook("steam_api.dll", "SteamAPI_UnregisterCallback", (char*)&AstralatheSteamAPI_UnregisterCallback, &discard, L"Psychonauts.exe");
	TryHook(unregisterCallbackHook);

	PLH::IatHook* runCallbacksHook = new PLH::IatHook("steam_api.dll", "SteamAPI_RunCallbacks", (char*)&AstralatheSteamAPI_RunCallbacks, &discard, L"Psychonauts.exe");
	TryHook(runCallbacksHook);

	PLH::IatHook* steamFriendsHook = new PLH::IatHook("steam_api.dll", "SteamFriends", (char*)&AstralatheSteamFriends, &discard, L"Psychonauts.exe");
	TryHook(steamFriendsHook);

	PLH::IatHook* steamRemoteStorageHook = new PLH::IatHook("steam_api.dll", "SteamRemoteStorage", (char*)&AstralatheSteamRemoteStorage, &discard, L"Psychonauts.exe");
	TryHook(steamRemoteStorageHook);

	PLH::IatHook* steamUserStatsHook = new PLH::IatHook("steam_api.dll", "SteamUserStats", (char*)&AstralatheSteamUserStats, &discard, L"Psychonauts.exe");
	TryHook(steamUserStatsHook);
		
	PLH::IatHook* shutdownHook = new PLH::IatHook("steam_api.dll", "SteamAPI_Shutdown", (char*)&AstralatheSteamAPI_Shutdown, &discard, L"Psychonauts.exe");
	TryHook(shutdownHook);

	LogMessage("Done!\n");
#endif
}


void AstralatheSteam_TimelineEvent(const char* pchIcon,
	const char* pchTitle,
	const char* pchDescription,
	float flStartedOffset,
	float flLength,
	bool bFeatured)
{
#if defined(USE_STEAM) && defined(WIN32)
	TrySetup("SteamAPI_SteamTimeline_v001", SteamCallables::SteamAPI_SteamTimeline_v001);
	TrySetup("SteamAPI_ISteamTimeline_AddTimelineEvent", SteamCallables::SteamTimeline::AddTimelineEvent);

	SteamCallables::ISteamTimeline* pTimeline = SteamCallables::SteamAPI_SteamTimeline_v001();
	if (pTimeline)
	{
		SteamCallables::SteamTimeline::AddTimelineEvent(pTimeline,
			pchIcon,
			pchTitle,
			pchDescription,
			0,
			flStartedOffset,
			flLength,
			bFeatured ? 3 : 2);
	}
#endif
}