#pragma once

#include "polyhook2/Detour/x86Detour.hpp"
#include "Logging.h"
#include "thirdparty/hooking/Hooking.Patterns.h"

#define FROM_PATTERN(pat) hook::pattern( pat ).get( 0 ).get<char>( 0 )
#define SetupFunction(func, addr) func = PLH::FnCast( addr, func ); LogMessage("Found %s at %p\n", #func, func);