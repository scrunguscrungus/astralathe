#ifndef LUASECURITY_H
#define LUASECURITY_H

#include "Psychonauts/Lua.h"

//Lua security cod
//Contains functions and code responsible for locking down Psychonauts' Lua VM
//to be more secure.
//
//Not infallible, never infallible, but it is better than nothing.

void InstallSecurity_Execute(lua_State* L);
void InstallSecurity_Exit(lua_State* L);
void InstallSecurity_getenv(lua_State* L);

void InstallLuaSecurity(lua_State* L);

int secure_io_open(lua_State* L);
int secure_io_fromto(lua_State* L, int inout, const char* mode);
int secure_io_appendto(lua_State* L);

static int io_close(lua_State* L);

void LuaSecurity_openwithcontrol(lua_State* L);

#endif