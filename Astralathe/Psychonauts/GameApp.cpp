#include "GameApp.h"

GameApp* g_pGameApp = nullptr;
GameAppProxy* g_pGameAppProxy = nullptr;

tGameApp_CallFunctionf GameApp_CallFunctionf;
tGameApp_SetShaderDiffuseColour GameApp_SetShaderDiffuseColour;
tGameApp_GetMainChannel GameApp_GetMainChannel;