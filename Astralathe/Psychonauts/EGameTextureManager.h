#ifndef EGAMETEXTUREMANAGER_H
#define EGAMETEXTUREMANAGER_H

class EGameTextureManager
{};

#define pEGameTextureManager_DeleteTexture "55 8B EC 83 EC 14 89 4D ? 8B 45 ? 50 8B 4D ? 8B 49 ? E8 ? ? ? ? 89 45 ? 83 7D ? 00 75 ? E9 ? ? ? ?"
typedef void( __thiscall* tEGameTextureManager_DeleteTexture )( EGameTextureManager*, unsigned int, bool );
extern tEGameTextureManager_DeleteTexture EGameTextureManager_DeleteTexture;

#endif