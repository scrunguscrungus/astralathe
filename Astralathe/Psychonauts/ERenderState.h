#pragma once

class ERenderState
{};

namespace _ERenderState
{
	constexpr const char* patConstructor = "55 8B EC 51 89 4D ? 8B 45 ? C6 00 01 8B 4D ? C7 41 ? 03 00 00 00";

	typedef void(__thiscall* tConstruct)(ERenderState*);
	extern tConstruct Construct;
}