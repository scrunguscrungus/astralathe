#pragma once

#include "PsychoMaths.h"

class ECamera
{};

namespace _ECamera
{
	constexpr const char* patBoxVisible = "55 8B EC 83 EC 28 89 4D ? 8B 45 ? 8A 88 ? ? ? ?";
	typedef bool(__thiscall* tBoxVisible)(ECamera*, EBox3 box, void* pVisCache, bool param_3);
	extern tBoxVisible BoxVisible;

	constexpr const char* patCalculateScreenDiagonal = "55 8B EC 83 EC 44 89 4D ? 8B 45 ? 8B 48 ? 89 4D ? C7 45 ? 00 00 00 00";
	typedef float(__thiscall* tCalculateScreenDiagonal)(ECamera*, EBox3* pBox);
	extern tCalculateScreenDiagonal CalculateScreenDiagonal;
}