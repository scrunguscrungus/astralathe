#include "EScriptObject.h"
#include "EScriptVM.h"
#include "GameApp.h"

tEScriptVM_FindScriptByName EScriptVM_FindScriptByName;
tEScriptVM_GetScriptTableValueFromFloat EScriptVM_GetScriptTableValueFromFloat;
tEScriptVM_RunLuaCommands EScriptVM_RunLuaCommands;
tEScriptVM_MarkContainedTablesAsReadOnly EScriptVM_MarkContainedTablesAsReadOnly;

tEDataArrayESVMBaseClassEntry_ElementNew EDataArrayESVMBaseClassEntry_ElementNew;
tEDataArrayESVMBaseClassEntry_ElementGet EDataArrayESVMBaseClassEntry_ElementGet;
tEHashTableESVMBaseClassEntry_Find EHashTableESVMBaseClassEntry_Find;

EScriptVM* GetScriptVM()
{
	if (g_pGameApp)
	{
		return (EScriptVM*)(((char*)g_pGameApp) + 39476);
	}
	return nullptr;
}

lua_State* GetLuaState(EScriptVM* pScriptVM /*= nullptr*/)
{
	if (pScriptVM == nullptr)
	{
		pScriptVM = GetScriptVM();
		if (pScriptVM == nullptr)
			return nullptr;
	}
	return *(lua_State**)(pScriptVM + 8);
}