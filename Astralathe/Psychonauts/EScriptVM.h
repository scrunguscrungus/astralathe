#ifndef SCRIPTVM_H
#define SCRIPTVM_H

#include "Lua.h"

#define pEScriptVM_FindScriptByName "55 8B EC 83 EC 30 89 4D ? 0F B6 05 ? ? ? ?"
#define PEScriptVM_RunLuaCommands "55 8B EC 83 EC 14 89 4D ? 8B 45 ? 8B 48 ? 8B 51 ? 89 55 ? 8B 45 ?"
	
#define pEScriptVM_SendStateMessageToAllScripts "55 8B EC 83 EC 28 89 4D ? 8B 45 ? 05 14 01 00 00 89 45 ? 33 C9 75 ? 8B 55 ? 89 55 ? 8B 45 ? 8B 48 ? 89 4D ? EB ? 8B 55 ? 89 55 ? 8B 45 ? 8B 48 ? 89 4D ? 8B 55 ? 89 55 ? 33 C0 83 7D ? 00 0F 95 C0 0F B6 C8 85 C9 74 ? 8B 55 ? 8B 42 ? 50"

#define pEScriptVM_GetScriptTableValueFromFloat "55 8B EC 83 EC 10 89 4D ? C6 45 ? 00 8B 45 ? 8B 48 ? 51 E8 ? ? ? ? 83 C4 04 89 45 ? 8B 55 ? 0F BF 42 ? 50 8B 4D ? 8B 51 ? 52 E8 ? ? ? ? 83 C4 08 6A 01 8B 45 ? 50 8B 4D ? E8 ? ? ? ? 0F B6 C8 85 C9 74 ?"

class EScriptVM
{

};

class ESVMBaseClassEntry
{

};
class EDataArrayESVMBaseClassEntry
{

};
class EHashTableESVMBaseClassEntry
{

};
class EHashBase
{

};

typedef EScriptObject*( __thiscall* tEScriptVM_FindScriptByName )( EScriptVM*, const char* );
extern tEScriptVM_FindScriptByName EScriptVM_FindScriptByName;

typedef bool(__thiscall* tEScriptVM_GetScriptTableValueFromFloat)(EScriptVM*, EScriptObject* pObj, const char* pszTarget, int* ret);
extern tEScriptVM_GetScriptTableValueFromFloat EScriptVM_GetScriptTableValueFromFloat;

typedef unsigned int( __thiscall* tEScriptVM_RunLuaCommands )( EScriptVM*, const char*, EScriptObject*, float*, unsigned int);
extern tEScriptVM_RunLuaCommands EScriptVM_RunLuaCommands;

#define pEScriptVM_MarkContainedTablesAsReadOnly "55 8B EC 83 EC 08 89 4D ? 8B 45 ? 8B 48 ? 51 E8 ? ? ? ?"
typedef void( __thiscall* tEScriptVM_MarkContainedTablesAsReadOnly )( EScriptVM* pThis, bool param_1 );
extern tEScriptVM_MarkContainedTablesAsReadOnly EScriptVM_MarkContainedTablesAsReadOnly;

//EDataArray<ESVMBaseClassEntry>

#define pEDataArrayESVMBaseClassEntry_ElementNew "55 8B EC 83 EC 2C 89 4D ? 8B 45 ? 8B 4D ? 8B 50 ?"
typedef unsigned int ( __thiscall* tEDataArrayESVMBaseClassEntry_ElementNew )( EDataArrayESVMBaseClassEntry* );
extern tEDataArrayESVMBaseClassEntry_ElementNew EDataArrayESVMBaseClassEntry_ElementNew;

#define pEDataArrayESVMBaseClassEntry_ElementGet "55 8B EC 83 EC 08 89 4D ? 8B 45 ? 50 8B 4D ? E8 ? ? ? ? 89 45 ? 8B 45 ? 8B E5 5D C2 04 00"
typedef ESVMBaseClassEntry* ( __thiscall* tEDataArrayESVMBaseClassEntry_ElementGet )( EDataArrayESVMBaseClassEntry*, unsigned int index );
extern tEDataArrayESVMBaseClassEntry_ElementGet EDataArrayESVMBaseClassEntry_ElementGet;

//EHashTable<ESVMBaseClassEntry>
#define pEHashTableESVMBaseClassEntry_Find "55 8B EC 83 EC 08 89 4D ? 6A 00 8B 45 ? 50 8B 4D ? E8 ? ? ? ? 89 45 ? 83 7D ? 00 75 ? 8B 45 ?"
typedef ESVMBaseClassEntry* ( __thiscall* tEHashTableESVMBaseClassEntry_Find )( EHashTableESVMBaseClassEntry* pThis, char* pszClassname, ESVMBaseClassEntry* param_2 );
extern tEHashTableESVMBaseClassEntry_Find EHashTableESVMBaseClassEntry_Find;

//EHashBase

#define pEHashBase_Set "55 8B EC 83 EC 48 89 4D ? 8D 45 ? 50 8B 4D ?"


EScriptVM* GetScriptVM();
lua_State* GetLuaState(EScriptVM* pScriptVM = nullptr);
#endif