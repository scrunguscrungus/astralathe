#pragma once

class EEntityIter {};

class EDomain;

namespace _EEntityIter
{
	constexpr const char* patConstructor = "55 8B EC 83 EC 10 89 4D ? 8B 45 ? 8B 4D ? 89 08 8B 55 ? C7 42 ? ? ? ? ?";
	constexpr const char* patOperatorIterate = "55 8B EC 83 EC 10 89 4D ? 8B 45 ? 8B 48 ? 83 C1 01";

	typedef void(__thiscall* tConstruct)(EEntityIter*, EDomain*, bool, bool);
	extern tConstruct Construct;

	typedef EEntityIter&(__thiscall* tOperatorIterate)(EEntityIter*);
	extern tOperatorIterate OperatorIterate;
}