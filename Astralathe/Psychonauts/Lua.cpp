#include "Lua.h"
#include <string.h>

tlua_dofile lua_dofile;
tlua_getglobal lua_getglobal;
tlua_type lua_type;
tlua_gettop lua_gettop;
tlua_settop lua_settop;
tlua_setglobal lua_setglobal;
tlua_gettable lua_gettable;
tlua_settable lua_settable;
tlua_settag lua_settag;
tlua_settagmethod lua_settagmethod;
tlua_pushstring lua_pushstring;
tlua_pushlstring lua_pushlstring;
tlua_pushvalue lua_pushvalue;
tlua_pushnil lua_pushnil;
tlua_pushnumber lua_pushnumber;
tlua_pushusertag lua_pushusertag;
tlua_pushcclosure lua_pushcclosure;
tlua_strlen lua_strlen;
tlua_call lua_call;
tlua_tag lua_tag;
tlua_newtag lua_newtag;
tlua_ref lua_ref;
tlua_unref lua_unref;
tlua_remove lua_remove;
tlua_newuserdata lua_newuserdata;
tlua_touserdata lua_touserdata;
tlua_tonumber lua_tonumber;
tlua_isnumber lua_isnumber;
tluaL_check_lstr luaL_check_lstr;
tgetfilebyref getfilebyref;
tgethandle gethandle;

tluaL_buffinit luaL_buffinit;
tluaL_prepbuffer luaL_prepbuffer;
tluaL_pushresult luaL_pushresult;

tluaL_opt_number luaL_opt_number;

tluaL_checkstack luaL_checkstack;
tlua_error lua_error;
tluaL_argerror luaL_argerror;

const char* luaL_opt_lstr(lua_State* L, int narg, const char* def, size_t* len) {
    if (lua_isnull(L, narg)) {
        if (len)
            *len = (def ? strlen(def) : 0);
        return def;
    }
    else return luaL_check_lstr(L, narg, len);
}