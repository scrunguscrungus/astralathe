#pragma once

#include "PsychoMaths.h"

class ERenderer {};
class ERenderState;

namespace _ERenderer
{
	constexpr const char* patSetRenderState = "55 8B EC 83 EC 08 89 4D ? 8B 45 ? 8A 88 ? ? ? ?";
	typedef void (__thiscall* tSetRenderState)(ERenderer*, ERenderState*);
	extern tSetRenderState SetRenderState;

	constexpr const char* patBeginPush = "55 8B EC 83 EC 0C 89 4D ? 33 C9";
	typedef unsigned int* (__thiscall* tBeginPush)(ERenderer*, unsigned int drawMode, unsigned int size);
	extern tBeginPush BeginPush;

	constexpr const char* patEndPush = "55 8B EC 83 EC 4C 89 4D ? 8B 45 ? 83 78 ? 00";
	typedef unsigned int* (__thiscall* tEndPush)(ERenderer*, unsigned int* pPushBuffer);
	extern tEndPush EndPush;
}

extern ERenderer* g_pCurrentRenderer;