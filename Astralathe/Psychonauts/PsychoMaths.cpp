#include "PsychoMaths.h"

//I was too lazy to make this a callable
unsigned int ColorVectorToRGBA(EVec4* pVec)
{
	float fVar1;
	float fVar2;
	float fVar3;
	float fVar4;

	if (((((pVec->X < 0.0) || (1.0 < pVec->X)) || (pVec->Y < 0.0)) ||
		((1.0 < pVec->Y || (pVec->Z < 0.0)))) ||
		((1.0 < pVec->Z || ((pVec->W < 0.0 || (1.0 < pVec->W)))))) {
		return 0;
	}
	fVar1 = pVec->X * 255.0f;
	if (fVar1 <= 0.0f) {
		fVar1 = fVar1 - 0.5f;
	}
	else {
		fVar1 = fVar1 + 0.5f;
	}
	fVar2 = pVec->Y * 255.0f;
	if (fVar2 <= 0.0f) {
		fVar2 = fVar2 - 0.5f;
	}
	else {
		fVar2 = fVar2 + 0.5f;
	}
	fVar3 = pVec->Z * 255.0f;
	if (fVar3 <= 0.0f) {
		fVar3 = fVar3 - 0.5f;
	}
	else {
		fVar3 = fVar3 + 0.5f;
	}
	fVar4 = pVec->W * 255.0f;
	if (fVar4 <= 0.0f) {
		fVar4 = fVar4 - 0.5f;
	}
	else {
		fVar4 = fVar4 + 0.5f;
	}
	return (int)fVar2 << 8 | (int)fVar4 << 0x18 | (int)fVar1 << 0x10 | (int)fVar3;
}

float SIGN_float(float f)
{
	if (f == 0.0f)
	{
		return 0.0f;
	}
	else if (f <= 0.0f)
	{
		return -1.0f;
	}
	else
	{
		return 1.0f;
	}
}

float SafeDivide(float a, float b)
{
	if (b == 0.0f)
	{
		float sign = SIGN_float(a);
		return sign * 1e09f;
	}
	else
	{
		return a / b;
	}
}

bool Equal(float val, float A, float tolerance)
{
	float diffValA = val - A;
	diffValA *= diffValA;

	float toleranceThresh = tolerance * tolerance;

	return diffValA <= toleranceThresh;
}

float Cosine(float deg)
{
	return cos(deg * DEG2RAD);
}
float Sine(float deg)
{
	return sin(deg * DEG2RAD);
}

EMat4 MatrixRotateEuler(const EEuler& euler)
{
	float cosX = Cosine(euler.X);
	float sinX = Sine(euler.X);

	float cosY = Cosine(euler.Y);
	float sinY = Sine(euler.Y);

	float cosZ = Cosine(euler.Z);
	float sinZ = Sine(euler.Z);

	return { cosY * cosZ,cosY * sinZ,-sinY,0.0,
			   sinX * sinY * cosZ - cosX * sinZ,cosX * cosZ + sinX * sinY * sinZ,
			   sinX * cosY,0.0,sinX * sinZ + cosX * sinY * cosZ,
			   cosX * sinY * sinZ - sinX * cosZ,cosX * cosY,0.0,0.0,0.0,0.0,1.0 };
}

EMat4 cg_mIdentity = {
	1.0, 0.0, 0.0, 0.0,
	0.0, 1.0, 0.0, 0.0,
	0.0, 0.0, 1.0, 0.0,
	0.0, 0.0, 0.0, 1.0
};

EVec4 cg_vWhite = { 1.0f, 1.0f, 1.0f, 1.0f };
EVec3 cg_vDefaultUpVector = { 0.0f, 1.0f, 0.0f };

EVec3 EVec3::RotateByEulerAngles(EVec3* pEuler)
{
	EEuler euler(pEuler);
	EMat4 matrix = MatrixRotateEuler(euler);
	return matrix.Mul3Vec3(this);
}
