#ifndef GAMEAPP_H
#define GAMEAPP_H

#include "EEntity.h"
#include "EUIMenu.h"
#include "EScriptVM.h"
#include "PsychoMaths.h"

#define pGameApp_InitUIMenu 0xFA2E0
#define pGameApp_UpdateCheatCodes 0x106CB0

#define pGameApp_LoadLocalizedTextBank "55 8B EC 81 EC C0 00 00 00 89 8D ? ? ? ? 8B 85 ? ? ? ? 0F B6 88 ? ? ? ?"

#define pGameApp_InitLua "55 8B EC 51 89 4D ? 8B 4D ? 81 C1 34 9A 00 00"

class GameApp {
public:

	//0
	char field_0[ 62 ]; //33164

	//62
	char m_cRazInvincible; //1

	//63
	char field_63[ 33101 ];

	//33164
	EEntity* pPlayer; //4

	//33168
	char field_33168[ 2772 ]; //2772

	//35940
	EUIMenu* pUIMenu; //4

	//35944
	char field_35944[ 973 ]; //973
	
	//36917
	bool m_bStartupComplete; //1

	//36918
	char field_36918[ 2558 ]; //2558

	//39476
	EScriptVM pScriptVM; //Unknown
};

#define pGameApp_CallFunctionf "55 8B EC 83 EC 08 8D 45 ? 89 45 ? 6A 00"
typedef bool( __fastcall* tGameApp_CallFunctionf )( GameApp*, void* edx, EScriptVM*, const char*, const char*, ... );
extern tGameApp_CallFunctionf GameApp_CallFunctionf;

constexpr const char* pGameApp_SetShaderDiffuseColour = "55 8B EC 51 89 4D ? 8B 45 ? 8B 88 ? ? ? ? 51 8B 55 ? 8B 8A ? ? ? ?";
typedef void(__thiscall* tGameApp_SetShaderDiffuseColour)(GameApp*, EVec4*);
extern tGameApp_SetShaderDiffuseColour GameApp_SetShaderDiffuseColour;

class ECamera;
constexpr const char* pGameApp_GetMainChannel = "55 8B EC 51 89 4D ? 6A 00 8B 4D ? E8 ? ? ? ? 8B 00";
typedef ECamera* (__thiscall* tGameApp_GetMainChannel)(GameApp*);
extern tGameApp_GetMainChannel GameApp_GetMainChannel;

extern GameApp* g_pGameApp;

class GameAppProxy;
extern GameAppProxy* g_pGameAppProxy;

#endif