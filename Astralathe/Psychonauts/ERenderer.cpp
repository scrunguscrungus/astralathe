#include "ERenderer.h"
#include "Proxying/ERendererProxy.h"

namespace _ERenderer
{
	tSetRenderState SetRenderState;
	tBeginPush BeginPush;
	tEndPush EndPush;
}

ERenderer* g_pCurrentRenderer = nullptr;