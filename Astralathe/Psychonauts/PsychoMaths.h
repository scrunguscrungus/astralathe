#pragma once

/* In here we have helpers, imitations and reimplementations of the game's various mathematical and geometrical functions and classes.
* Unless specified, all of the structs and such here are designed to be 1:1 with the ones used by the game, so that the game's native
* functions can all understand them.
* 
* Functions provided are often not identical in signature or implementation, but this is fine as they are intended to only be called
* by Astralathe code.
*/

#undef min
#undef max
#include <algorithm>

const float DEG2RAD = 0.0174533f;
const float RAD2DEG = 57.2958f;

//Fuzzy check for float equality
bool Equal(float val, float A, float tolerance);

//DEGREES cosine
float Cosine(float deg);
//DEGREES sine
float Sine(float deg);

struct EVec3;
struct EVec4;
struct EEuler;
class EMat4;

extern EMat4 MatrixRotateEuler(const EEuler& euler);
extern unsigned int ColorVectorToRGBA(EVec4* pVec);

extern float SIGN_float(float f);

template <typename T>
T PIN(T val, T min, T max)
{
	if ((val <= max) && (max = val, val < min))
	{
		max = min;
	}
	return max;
}

template <typename T>
void SWAP(T* A, T* B)
{
	T temp = *A;
	*A = *B;
	*B = temp;
}

extern float SafeDivide(float a, float b);

extern EVec4 cg_vWhite;
extern EVec3 cg_vDefaultUpVector;


struct EVec3
{
public:
	float X;
	float Y;
	float Z;

	EVec3() :
		X(0.0f),
		Y(0.0f),
		Z(0.0f)
	{}

	EVec3(EVec3* rExisting) :
		X(rExisting->X),
		Y(rExisting->Y),
		Z(rExisting->Z)
	{}

	EVec3(const EVec3& rExisting) :
		X(rExisting.X),
		Y(rExisting.Y),
		Z(rExisting.Z)
	{}

	EVec3(float x, float y, float z) :
		X(x),
		Y(y),
		Z(z)
	{}

	void operator=(EVec3* newVec)
	{
		X = newVec->X;
		Y = newVec->Y;
		Z = newVec->Z;
	}

	EVec3 operator/(EVec3* pOther)
	{
		return Cross(pOther);
	}

	float Normalize()
	{
		float squareLength = this->Z * this->Z + this->Y * this->Y + this->X * this->X;
		if (squareLength > 0.0f)
		{
			if (Equal(squareLength, 1.0f, 1e-05f))
			{
				return 1.0f;
			}
			float sqrt = sqrtf(squareLength);
			squareLength = 1.0f / sqrt;

			this->X *= squareLength;
			this->Y *= squareLength;
			this->Z *= squareLength;
			return sqrt;
		}
		else
		{
			return 0.0f;
		}
	}

	EVec3 Cross(EVec3* pVec)
	{
		return EVec3(pVec->Z * this->Y - pVec->Y * this->Z, pVec->X * this->Z - pVec->Z * this->X,
			pVec->Y * this->X - pVec->X * this->Y);
	}

	EVec3 operator/(EVec3 other)
	{
		return Cross(&other);
	}
	EVec3 GetNormal(float* pLen)
	{
		float squareLength = this->Z * this->Z + this->Y * this->Y + this->X * this->X;
		if (squareLength > 0.0f)
		{
			float sqrt = sqrtf(squareLength);
			float fNorm = 1.0f / sqrt;

			if (pLen != nullptr)
			{
				*pLen = sqrt;
			}
			return EVec3(this->X * fNorm, this->Y * fNorm, this->Z * fNorm);
		}
		else
		{
			if (pLen != nullptr)
			{
				*pLen = 0.0f;
			}
			return EVec3(*this);
		}
	}

	EVec3 CalculateRightVector(EVec3* pUp)
	{
		EVec3 cross = this->Cross(pUp);

		float rightLen = cross.Normalize();

		if (rightLen < 1e-05f)
		{
			EVec3 local_28;
			if (0.5f < pUp->X)
			{
				local_28 = EVec3(0.0f, 1.0f, 0.0f);
			}
			else
			{
				local_28 = EVec3(1.0f, 0.0f, 0.0f);
			}

			EVec3 right = this->Cross(&local_28);
			right.Normalize();
			return right;
		}
		return cross;
	}

	float Dot(EVec3 other)
	{
		return other.Z * this->Z + other.Y * this->Y + other.X * this->X;
	}

	EVec3 operator-(const EVec3& other)
	{
		return EVec3(this->X - other.X,
			this->Y - other.Y,
			this->Z - other.Z);
	}

	EVec3 operator*(float scalar)
	{
		return EVec3(this->X * scalar, this->Y * scalar, this->Z * scalar);
	}

	EVec3 operator+(const EVec3& otherVec)
	{
		return EVec3(this->X + otherVec.X, this->Y + otherVec.Y, this->Z + otherVec.Z);
	}

	EVec3 RotateByEulerAngles(EVec3* pEuler);

	static void Min(EVec3* pDest, EVec3* A, EVec3* B)
	{
		pDest->X = std::min(A->X, B->X);
		pDest->Y = std::min(A->Y, B->Y);
		pDest->Z = std::min(A->Z, B->Z);
	}
	static void Max(EVec3* pDest, EVec3* A, EVec3* B)
	{
		pDest->X = std::max(A->X, B->X);
		pDest->Y = std::max(A->Y, B->Y);
		pDest->Z = std::max(A->Z, B->Z);
	}
};

struct EEuler
{
public:
	float X;
	float Y;
	float Z;

	EEuler() :
		X(0.0f),
		Y(0.0f),
		Z(0.0f)
	{}

	EEuler(EEuler* rExisting) :
		X(rExisting->X),
		Y(rExisting->Y),
		Z(rExisting->Z)
	{}

	EEuler(const EEuler& rExisting) :
		X(rExisting.X),
		Y(rExisting.Y),
		Z(rExisting.Z)
	{}

	EEuler(float x, float y, float z) :
		X(x),
		Y(y),
		Z(z)
	{}

	EEuler(EVec3* fromVec) :
		X(fromVec->X),
		Y(fromVec->Y),
		Z(fromVec->Z)
	{}

	void operator=(EVec3* newVec)
	{
		X = newVec->X;
		Y = newVec->Y;
		Z = newVec->Z;
	}
};


struct EVec4
{
public:
	float X;
	float Y;
	float Z;
	float W;

	EVec4() :
		X(0.0f),
		Y(0.0f),
		Z(0.0f),
		W(0.0f)
	{}

	EVec4(float x, float y, float z, float w) :
		X(x),
		Y(y),
		Z(z),
		W(w)
	{}

	EVec4(EVec3* pVec3, float w) :
		X(pVec3->X),
		Y(pVec3->Y),
		Z(pVec3->Z),
		W(w)
	{}
};

//Shapes

struct ESphere
{
public:
	float X;
	float Y;
	float Z;
	float Radius;
};

class ETri3
{
public:
	EVec3 Vert1;
	EVec3 Vert2;
	EVec3 Vert3;

	ETri3() :
		Vert1(),
		Vert2(),
		Vert3()
	{}


	ETri3(EVec3* vert1, EVec3* vert2, EVec3* vert3) :
		Vert1(vert1),
		Vert2(vert2),
		Vert3(vert3)
	{
	}

	EVec3 GetFaceNormal()
	{
		EVec3 A = Vert2 - Vert1;
		EVec3 B = Vert3 - Vert1;
		EVec3 cross = A / B;
		return cross.GetNormal(nullptr);
	}
};

struct EBox3
{
public:
	EVec3 mins;
	EVec3 maxs;

	EBox3() :
		mins(),
		maxs()
	{}

	EBox3(const EVec3& rMins, const EVec3 rMaxs) :
		mins(rMins),
		maxs(rMaxs)
	{}

	EBox3(ESphere* pSphere) :
		mins(),
		maxs()
	{
		EVec3 radVec(pSphere->Radius, pSphere->Radius, pSphere->Radius);

		EVec3 diff = *(EVec3*)pSphere - radVec;
		mins.X = diff.X;
		mins.Y = diff.Y;
		mins.Z = diff.Z;

		diff = *(EVec3*)pSphere + radVec;
		maxs.X = diff.X;
		maxs.Y = diff.Y;
		maxs.Z = diff.Z;
	}

	EBox3(EVec3* pVec, int iCount):
		mins(),
		maxs()
	{
		Set(pVec, iCount);
	}

	void Set(EVec3* pVec, int iCount)
	{
		if (iCount == 0)
		{
			//Assert
			return;
		}

		mins.X = pVec->X;
		mins.Y = pVec->Y;
		mins.Z = pVec->Z;

		maxs.X = pVec->X;
		maxs.Y = pVec->Y;
		maxs.Z = pVec->Z;

		for (int i = 0; i < iCount; i++)
		{
			EVec3 newMins;
			EVec3 newMaxs;

			EVec3::Min(&newMins, &this->mins, pVec + i);
			mins.X = newMins.X;
			mins.Y = newMins.Y;
			mins.Z = newMins.Z;

			EVec3::Max(&newMaxs, &this->maxs, pVec + i);
			maxs.X = newMaxs.X;
			maxs.Y = newMaxs.Y;
			maxs.Z = newMaxs.Z;
		}
	}
};

class EMat4 {
public:
	float M00;
	float M10;
	float M20;
	float M30;
	float M01;
	float M11;
	float M21;
	float M31;
	float M02;
	float M12;
	float M22;
	float M32;
	float M03;
	float M13;
	float M23;
	float M33;

	EMat4* operator=(EMat4* v)
	{
		if (v != this)
		{
			memcpy(this, v, sizeof(EMat4));
		}
		return this;
	}

	EMat4& operator=(const EMat4& v)
	{
		if (&v != this)
		{
			memcpy(this, &v, sizeof(EMat4));
		}
		return *this;
	}

	EVec3 MulVec3(EVec3* pVec)
	{
		return EVec3(this->M03 + this->M02 * pVec->Z + this->M01 * pVec->Y + this->M00 * pVec->X,
			this->M13 + this->M12 * pVec->Z + this->M11 * pVec->Y + this->M10 * pVec->X,
			this->M23 + this->M22 * pVec->Z + this->M21 * pVec->Y + this->M20 * pVec->X);
	}

	EVec3 Mul3Vec3(EVec3* pVec)
	{
		return EVec3(this->M02 * pVec->Z + this->M01 * pVec->Y + this->M00 * pVec->X,
			this->M12 * pVec->Z + this->M11 * pVec->Y + this->M10 * pVec->X,
			this->M22 * pVec->Z + this->M21 * pVec->Y + this->M20 * pVec->X);
	}

	void Translate(float x, float y, float z)
	{
		this->M03 = this->M02 * z + this->M01 * y + this->M00 * x + this->M03;
		this->M13 = this->M12 * z + this->M11 * y + this->M10 * x + this->M13;
		this->M23 = this->M22 * z + this->M21 * y + this->M20 * x + this->M23;
		this->M33 = this->M32 * z + this->M31 * y + this->M30 * x + this->M33;
	}

	static void MulVec4(EVec4* pDest, EMat4* pMat, EVec4* pVec)
	{
		pDest->X = pVec->W * pMat->M03 +
			pVec->Z * pMat->M02 + pVec->Y * pMat->M01 + pVec->X * pMat->M00;
		pDest->Y = pVec->W * pMat->M13 +
			pVec->Z * pMat->M12 + pVec->Y * pMat->M11 + pVec->X * pMat->M10;
		pDest->Z = pVec->W * pMat->M23 +
			pVec->Z * pMat->M22 + pVec->Y * pMat->M21 + pVec->X * pMat->M20;
		pDest->W = pVec->W * pMat->M33 +
			pVec->Z * pMat->M32 + pVec->Y * pMat->M31 + pVec->X * pMat->M30;
	}

	static void TransformPointPerspective(EVec3* pDest, EMat4* pMat, EVec3* pVec)
	{
		EVec4 toVec4 = EVec4(pVec, 1.0f);

		EVec4 mult;
		MulVec4(&mult, pMat, &toVec4);
		float wDiv = SafeDivide(1.0f, mult.W);
		pDest->X = mult.X * wDiv;
		pDest->Y = mult.Y * wDiv;
		pDest->Z = mult.Z * wDiv;
	}
};

extern EMat4 cg_mIdentity;