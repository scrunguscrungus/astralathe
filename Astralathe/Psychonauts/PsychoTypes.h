#ifndef PSYCHOTYPES_H
#define PSYCHOTYPES_H

#define pEArrayString_AddData "55 8B EC 83 EC 4C 89 4D ? 6A 00 6A 01 6A 0C"
using tEArrayString_AddData = int ( __thiscall* )( void*, const char* );
extern tEArrayString_AddData EArrayString_AddData;

#include "DFDInput.h"
#include "EEntity.h"
#include "EMainPage.h"
#include "EScriptObject.h"
#include "EScriptVM.h"
#include "ERenderer.h"
#include "EUIMenu.h"
#include "GameApp.h"
#include "EGameTextureManager.h"
#include "EMeshManager.h"
#include "EFileManager.h"
#include "Lua.h"

#endif