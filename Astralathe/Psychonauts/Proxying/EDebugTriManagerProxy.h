#pragma once

/*
*	EDebugLineManager proxy built for re-implementing debug line drawing.
*	Kind of a hybrid of custom stuff and what's left of the in-game functionality.
*
*	Since basically all the functionality is stubbed out in the actual game,
*	it gives us the freedom to reimplement it basically however we like.
*/

#include "ClassProxy.h"
#include "Psychonauts/ERenderer.h"
#include "GameAppProxy.h"
#include "ERendererProxy.h"
#include "ERenderStateProxy.h"
#include "ECameraProxy.h"
#include "Psychonauts/DebugDraw/EDebugTriManager.h"
#include "Logging.h"
#include "Hooks.h"
#include "Psychonauts/PsychoMaths.h"
#include "Psychonauts/DebugShapes.h"


class EDebugTriManagerProxy : public ClassProxy<EDebugTriManager>
{
	/*
	* A field like this does exist in the actual class but we're not VarProxying it because
	* I don't know to what extent EDebugLine actually still exists.
	*
	* Proxying it would also mean having to go through a bunch of effort to work with
	* EDataArray, when it would just be easier to do this ourself.
	*/
	static std::vector<EDebugTri> m_Tris;
	static int m_TriCounts[3];

public:
	//VarProxy<EEntityInfo, EEntity*> Entity;

	EDebugTriManagerProxy(EDebugTriManager* d) : ClassProxy<EDebugTriManager>(d)
	{}

	//Hooks
	//None of these need to call the originals, as the originals are stubs that do nothing.
	static void __fastcall DrawAll(EDebugTriManager* pThis, void* edx)
	{
		if (m_Tris.size() < 1)
			return;

		GameAppProxy gameApp(g_pGameApp);

		gameApp.SetModelToWorld(&cg_mIdentity);

		for (int iPass = 0; iPass < 3; iPass++)
		{
			unsigned int remaining = m_TriCounts[iPass] * 3;

			if (remaining != 0)
			{
				unsigned int totalTriSizes = m_TriCounts[iPass] * 12;
				unsigned int align = totalTriSizes / 0x7f8 + 1;

				int triIdx = 0;

				//Create ERenderState
				ERenderStateProxy renderState;
				renderState.m_bUnkBool1.SetValue(true);
				if (iPass == 1)
				{
					renderState.m_bUnkBool2.SetValue(false);
				}
				else if (iPass == 2)
				{
					renderState.m_ShadeMode.SetValue(1);
					renderState.m_FillMode.SetValue(2);
					renderState.m_bUnkBool3.SetValue(false);
					renderState.m_WireframeOffset.SetValue(-6.0f);
				}
				renderState.m_CullMode.SetValue(1);
				ERenderer* pRenderer = gameApp.m_pRenderer.GetValue();
				ERendererProxy renderer(pRenderer);

				g_pCurrentRenderer = pRenderer;
				_ERenderer::SetRenderState(pRenderer, renderState.AsBase());
				GameApp_SetShaderDiffuseColour(g_pGameApp, &cg_vWhite);

				unsigned int pushBufferSize = totalTriSizes + align + 4;
				unsigned int* pPushBuffer = _ERenderer::BeginPush(pRenderer, 3, pushBufferSize);

				while (align != 0)
				{
					unsigned int iVar1;
					if (align - 1 == 0)
					{
						iVar1 = remaining;
					}
					else
					{
						iVar1 = 0x1fe;
					}

					//RenderPushHeader(&pPushBuffer, iVar1 << 2);
					unsigned int local_14 = iVar1 / 3;
					while (local_14 != 0)
					{
						EDebugTri& curTri = m_Tris.at(triIdx);
						triIdx++;

						unsigned int sortKey = curTri.GetSortKey();
						if (sortKey == iPass)
						{
							RendererPushData<EVec3>(pRenderer, &pPushBuffer, &curTri.Vert1.Position);
							RendererPushData<unsigned int>(pRenderer, &pPushBuffer, &curTri.Vert1.Colour);

							RendererPushData<EVec3>(pRenderer, &pPushBuffer, &curTri.Vert2.Position);
							RendererPushData<unsigned int>(pRenderer, &pPushBuffer, &curTri.Vert2.Colour);

							RendererPushData<EVec3>(pRenderer, &pPushBuffer, &curTri.Vert3.Position);
							RendererPushData<unsigned int>(pRenderer, &pPushBuffer, &curTri.Vert3.Colour);

							local_14--;
						}
					}
					remaining -= iVar1;
					align--;
				}
				_ERenderer::EndPush(pRenderer, pPushBuffer);
			}
		}
	}

	static void __fastcall AddTri(EDebugTriManager* pThis, void* edx, ETri3* pTri, EVec4* pCol, unsigned int flags)
	{
		EBox3 box(&pTri->Vert1, 3);

		ECamera* pCamera = GameApp_GetMainChannel(g_pGameApp);
		if (!_ECamera::BoxVisible(pCamera, box, nullptr, false))
		{
			return;
		}
		

		if (m_Tris.size() >= 16384)
			return;
		
		GameAppProxy gameApp(g_pGameApp);

		ECameraProxy renderCamera = ECameraProxy(gameApp.m_pRenderCamera.GetValue());
		
		EVec3 camPos = renderCamera.m_vecPos.GetValue();

		EVec3 diff = camPos - pTri->Vert1;

		EVec3 norm = pTri->GetFaceNormal();

		float dot = diff.Dot(norm);

		bool flag = false;
		if (dot < 0.0f)
		{
			if ((flags & 1) == 0)
				return;
			flag = true;
		}

		if (pCol == nullptr)
		{
			pCol = &cg_vWhite;
		}

		EDebugTri newTri;

		//This flag check seems to be backwards, but it looks better...?
		if (flag)
		{
			newTri.Vert1.Position.X = pTri->Vert1.X;
			newTri.Vert1.Position.Y = pTri->Vert1.Y;
			newTri.Vert1.Position.Z = pTri->Vert1.Z;

			newTri.Vert2.Position.X = pTri->Vert2.X;
			newTri.Vert2.Position.Y = pTri->Vert2.Y;
			newTri.Vert2.Position.Z = pTri->Vert2.Z;

			newTri.Vert3.Position.X = pTri->Vert3.X;
			newTri.Vert3.Position.Y = pTri->Vert3.Y;
			newTri.Vert3.Position.Z = pTri->Vert3.Z;

			int colour = ColorVectorToRGBA(pCol);
			newTri.Vert1.Colour = colour;
			if ((flags & 4) == 0)
			{
				colour = ColorVectorToRGBA(pCol + 1);
				newTri.Vert2.Colour = colour;

				colour = ColorVectorToRGBA(pCol + 2);
				newTri.Vert3.Colour = colour;
			}
			else
			{
				newTri.Vert2.Colour = colour;
				newTri.Vert3.Colour = colour;
			}
		}
		else
		{
			newTri.Vert1.Position.X = pTri->Vert2.X;
			newTri.Vert1.Position.Y = pTri->Vert2.Y;
			newTri.Vert1.Position.Z = pTri->Vert2.Z;

			newTri.Vert2.Position.X = pTri->Vert1.X;
			newTri.Vert2.Position.Y = pTri->Vert1.Y;
			newTri.Vert2.Position.Z = pTri->Vert1.Z;

			newTri.Vert3.Position.X = pTri->Vert3.X;
			newTri.Vert3.Position.Y = pTri->Vert3.Y;
			newTri.Vert3.Position.Z = pTri->Vert3.Z;

			int colour = ColorVectorToRGBA(pCol);
			newTri.Vert2.Colour = colour;
			if ((flags & 4) == 0)
			{
				colour = ColorVectorToRGBA(pCol + 1);
				newTri.Vert1.Colour = colour;

				colour = ColorVectorToRGBA(pCol + 2);
				newTri.Vert3.Colour = colour;
			}
			else
			{
				newTri.Vert1.Colour = colour;
				newTri.Vert3.Colour = colour;
			}
		}

		newTri.Flags = flags;

		m_Tris.push_back(newTri);

		m_TriCounts[newTri.GetSortKey()]++;
	}

	static void __fastcall ResetFrame(EDebugTriManager* pThis, void* edx)
	{
		m_Tris.clear();
		m_TriCounts[0] = 0;
		m_TriCounts[1] = 0;
		m_TriCounts[2] = 0;
	}
};