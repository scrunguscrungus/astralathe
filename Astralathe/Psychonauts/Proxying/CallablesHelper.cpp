#include "CallablesHelper.h"

#include "EEntityIterProxy.h"
#include "ERenderStateProxy.h"

void ClassProxyCallablesHelper::InitAllCallables()
{
	EEntityIterProxy::InitCallables();
	ERenderStateProxy::InitCallables();
}
