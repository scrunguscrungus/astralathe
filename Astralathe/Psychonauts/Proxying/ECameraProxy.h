#pragma once

#include "ClassProxy.h"
#include "Psychonauts/ECamera.h"

class ECameraProxy : public ClassProxy<ECamera>
{
public:
	VarProxy<ECamera, EVec3> m_vecPos;

	ECameraProxy(ECamera* d) : ClassProxy<ECamera>(d),
		m_vecPos(*this, 0x8)
	{}
};