#pragma once

#include "ClassProxy.h"
#include "Psychonauts/EEntity.h"
#include "Psychonauts/EEntityInfo.h"

class EEntityProxy : public ClassProxy<EEntity>
{
public:
	VarProxy<EEntity, char*> Name;

	VarProxy<EEntity, unsigned int> UnknownFlags;

	EEntityProxy(EEntity* d) : ClassProxy(d),
		Name(*this, 0x20),
		UnknownFlags(*this, 0x64) //100
	{}

	bool IsDomainSleeping()
	{
		return (UnknownFlags.GetValue() >> 5 & 1) != 0;
	}
};