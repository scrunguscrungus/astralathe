#pragma once

/*
*	EDebugLineManager proxy built for re-implementing debug line drawing.
*	Kind of a hybrid of custom stuff and what's left of the in-game functionality.
* 
*	Since basically all the functionality is stubbed out in the actual game,
*	it gives us the freedom to reimplement it basically however we like.
*/

//TODO: Does not currently support AddDeferredSphere.

#include "ClassProxy.h"
#include "Psychonauts/ERenderer.h"
#include "GameAppProxy.h"
#include "ERendererProxy.h"
#include "ERenderStateProxy.h"
#include "Psychonauts/DebugDraw/EDebugLineManager.h"
#include "Logging.h"
#include "Hooks.h"
#include "Psychonauts/PsychoMaths.h"
#include "Psychonauts/ECamera.h"
#include "Psychonauts/DebugShapes.h"


class EDebugLineManagerProxy : public ClassProxy<EDebugLineManager>
{
	/*
	* A field like this does exist in the actual class but we're not VarProxying it because
	* I don't know to what extent EDebugLine actually still exists.
	*
	* Proxying it would also mean having to go through a bunch of effort to work with
	* EDataArray, when it would just be easier to do this ourself.
	*/
	static std::vector<EDebugLine> m_Lines;
	//Ditto
	static int m_LineCounts[2];	
	
public:
	//VarProxy<EEntityInfo, EEntity*> Entity;

	EDebugLineManagerProxy(EDebugLineManager* d) : ClassProxy<EDebugLineManager>(d)
	{}

	//Hooks
	//None of these need to call the originals, as the originals are stubs that do nothing.
	static void __fastcall DrawAll(EDebugLineManager* pThis, void* edx)
	{
		//TODO: Add any queued up deferred sphere

		if (m_Lines.size() < 1)
			return;

		GameAppProxy gameApp(g_pGameApp);

		gameApp.SetModelToWorld(&cg_mIdentity);

		for (int iPass = 0; iPass < 2; iPass++)
		{
			unsigned int remaining = m_LineCounts[iPass] * 2;

			if (remaining != 0)
			{
				unsigned int totalLineSizes = m_LineCounts[iPass] << 3;
				unsigned int align = totalLineSizes / 0x7f8 + 1;

				int lineIdx = 0;

				//Create ERenderState
				ERenderStateProxy renderState;
				renderState.m_CullMode.SetValue(1);
				if (iPass == 1)
				{
					renderState.m_ZBufferMode.SetValue(8);
				}
				ERenderer* pRenderer = gameApp.m_pRenderer.GetValue();
				ERendererProxy renderer(pRenderer);

				g_pCurrentRenderer = pRenderer;
				_ERenderer::SetRenderState(pRenderer, renderState.AsBase());
				GameApp_SetShaderDiffuseColour(g_pGameApp, &cg_vWhite);

				unsigned int pushBufferSize = totalLineSizes + align + 4;
				unsigned int* pPushBuffer = _ERenderer::BeginPush(pRenderer, 1, pushBufferSize);

				while ( align != 0 )
				{
					unsigned int iVar1;
					if (align - 1 == 0)
					{
						iVar1 = remaining;
					}
					else
					{
						iVar1 = 0x1fe;
					}

					//RenderPushHeader(&pPushBuffer, iVar1 << 2);
					unsigned int local_14 = iVar1 / 2;
					while (local_14 != 0)
					{
						EDebugLine& curLine = m_Lines.at(lineIdx);
						lineIdx++;

						unsigned int sortKey = curLine.GetSortKey();
						if (sortKey == iPass)
						{
							RendererPushData<EVec3>(pRenderer, &pPushBuffer, &curLine.Start.Position);
							RendererPushData<unsigned int>(pRenderer , &pPushBuffer, &curLine.Start.Colour);

							RendererPushData<EVec3>(pRenderer, &pPushBuffer, &curLine.End.Position);
							RendererPushData<unsigned int>(pRenderer, &pPushBuffer, &curLine.End.Colour);

							local_14--;
						}
					}
					remaining -= iVar1;
					align--;
				}
				_ERenderer::EndPush(pRenderer, pPushBuffer);
			}
		}
	}

	static void __fastcall AddLine(EDebugLineManager* pThis, void* edx, EVec3* vecStart, EVec3* vecEnd, EVec4* vecCol, unsigned int flags)
	{
		if ((flags & 1) != 1)
		{
			EVec3 mins;
			EVec3 maxs;

			EVec3::Min(&mins, vecStart, vecEnd);
			EVec3::Max(&maxs, vecStart, vecEnd);

			EBox3 box(mins, maxs);

			ECamera* pCamera = GameApp_GetMainChannel(g_pGameApp);
			if (!_ECamera::BoxVisible(pCamera, box, nullptr, false))
			{
				return;
			}
		}

		EDebugLine newLine;
		unsigned int colour;
		if (vecCol != nullptr)
		{
			colour = ColorVectorToRGBA(vecCol);
		}
		else
		{
			colour = 0xffffffff;
		}

		newLine.Start.Position = vecStart;
		newLine.Start.Colour = colour;

		newLine.End.Position = vecEnd;
		newLine.End.Colour = colour;

		newLine.Flags = flags;

		m_Lines.push_back(newLine);

		if ((flags & 2) == 0)
		{
			m_LineCounts[0]++;
		}
		else
		{
			m_LineCounts[1]++;
		}
	}

	static void __fastcall AddArrow(EDebugLineManager* pThis, void* edx, EVec3* vecStart, EVec3* vecEnd, EVec4* vecCol, unsigned int flags)
	{
		//First line - straight ahead
		AddLine(pThis, edx, vecStart, vecEnd, vecCol, flags);


		EVec3 direction = *vecEnd - *vecStart;
		float normalDirectionLength = direction.Normalize();
		float local_10 = normalDirectionLength / 15.0f;

		EVec3 normal = direction.GetNormal(nullptr);
		EVec3 vecRight = normal.CalculateRightVector(&cg_vDefaultUpVector);

		EVec3 crossRightDir = vecRight.Cross(&direction);

		EVec3 dirXlocal_10 = direction * local_10;
		EVec3 rightXlocal_10 = vecRight * local_10;

		EVec3 rightA = *vecEnd + rightXlocal_10;
		EVec3 rightEnd = rightA - dirXlocal_10;
		//Second line - right
		AddLine(pThis, edx, vecEnd, &rightEnd, vecCol, flags);

		EVec3 leftA = *vecEnd - rightXlocal_10;
		EVec3 leftEnd = leftA - dirXlocal_10;

		//Final line - left
		AddLine(pThis, edx, vecEnd, &leftEnd, vecCol, flags);
	}

	static void __fastcall AddBox(EDebugLineManager* pThis, void* edx, EBox3* pBox, EMat4* pMatrix, EVec4* pColour, unsigned int flags)
	{
		EVec3 vecCorners[8];
		
		vecCorners[0] = EVec3(pBox->mins.X, pBox->mins.Y, pBox->mins.Z);
		vecCorners[1] = EVec3(pBox->mins.X, pBox->mins.Y, pBox->maxs.Z);
		vecCorners[2] = EVec3(pBox->mins.X, pBox->maxs.Y, pBox->mins.Z);
		vecCorners[3] = EVec3(pBox->mins.X, pBox->maxs.Y, pBox->maxs.Z);
		vecCorners[4] = EVec3(pBox->maxs.X, pBox->mins.Y, pBox->mins.Z);
		vecCorners[5] = EVec3(pBox->maxs.X, pBox->mins.Y, pBox->maxs.Z);
		vecCorners[6] = EVec3(pBox->maxs.X, pBox->maxs.Y, pBox->mins.Z);
		vecCorners[7] = EVec3(pBox->maxs.X, pBox->maxs.Y, pBox->maxs.Z);

		if (pMatrix != nullptr)
		{
			for (int i = 0; i < 8; i++)
			{
				EVec3 transformed;
				EMat4::TransformPointPerspective(&transformed, pMatrix, &vecCorners[i]);
				vecCorners[i].X = transformed.X;
				vecCorners[i].Y = transformed.Y;
				vecCorners[i].Z = transformed.Z;
			}
		}

		AddLine(pThis, edx, &vecCorners[0], &vecCorners[1], pColour, flags);
		AddLine(pThis, edx, &vecCorners[0], &vecCorners[2], pColour, flags);
		AddLine(pThis, edx, &vecCorners[0], &vecCorners[4], pColour, flags);

		AddLine(pThis, edx, &vecCorners[3], &vecCorners[2], pColour, flags);
		AddLine(pThis, edx, &vecCorners[3], &vecCorners[1], pColour, flags);
		AddLine(pThis, edx, &vecCorners[3], &vecCorners[7], pColour, flags);

		AddLine(pThis, edx, &vecCorners[5], &vecCorners[4], pColour, flags);
		AddLine(pThis, edx, &vecCorners[5], &vecCorners[7], pColour, flags);
		AddLine(pThis, edx, &vecCorners[5], &vecCorners[1], pColour, flags);

		AddLine(pThis, edx, &vecCorners[6], &vecCorners[7], pColour, flags);
		AddLine(pThis, edx, &vecCorners[6], &vecCorners[4], pColour, flags);
		AddLine(pThis, edx, &vecCorners[6], &vecCorners[2], pColour, flags);
	}

	static void __fastcall AddCircle(EDebugLineManager* pThis, void* edx, EMat4* pMat, float rad, EVec4* vecCol, unsigned int flags)
	{
		EVec3 radVec(rad, 0.0f, 0.0f);
		for (int i = 0; i < 28; i++)
		{
			EVec3 vec1(0.0f, i * 12.85714f, 0.0f);
			EVec3 vec2(0.0f, (i+1) * 12.85714f, 0.0f);

			EVec3 rotated = radVec.RotateByEulerAngles(&vec1);
			EVec3 vecStart = pMat->MulVec3(&rotated);

			rotated = radVec.RotateByEulerAngles(&vec2);
			EVec3 vecEnd = pMat->MulVec3(&rotated);

			AddLine(pThis, edx, &vecStart, &vecEnd, vecCol, flags);
		}
	}

	static void __fastcall AddCylinder(EDebugLineManager* pThis, void* edx, EMat4* pMat, float flHeight, float flRadBot, float flRadTop, EVec4* pCol, unsigned int flags)
	{
		EMat4 matCopy(*pMat);
		AddCircle(pThis, edx, &matCopy, flRadBot, pCol, flags);
		matCopy.Translate(0, flHeight, 0);
		AddCircle(pThis, edx, &matCopy, flRadTop, pCol, flags);

		EVec3 topRad(flRadTop, 0.0f, 0.0f);
		EVec3 botRad(flRadBot, 0.0f, 0.0f);
		for (int i = 0; i < 6; i++)
		{
			EVec3 vec1(0.0f, i * 60.0f, 0.0f);
			EVec3 vec2(0.0f, flHeight, 0.0f);

			EVec3 rotated = topRad.RotateByEulerAngles(&vec1);
			rotated = rotated + vec2;
			EVec3 vecStart = pMat->MulVec3(&rotated);
			rotated = botRad.RotateByEulerAngles(&vec1);
			EVec3 vecEnd = pMat->MulVec3(&rotated);
			AddLine(pThis, edx, &vecStart, &vecEnd, pCol, flags);
		}
	}

	static void __fastcall AddSphere(EDebugLineManager* pThis, void* edx, ESphere* pSphere, EVec4* pCol)
	{
		ECamera* pCamera = GameApp_GetMainChannel(g_pGameApp);
		EBox3 sphereBox(pSphere);
		if (!_ECamera::BoxVisible(pCamera, sphereBox, nullptr, false))
		{
			return;
		}

		float screenDiag = _ECamera::CalculateScreenDiagonal(pCamera, &sphereBox);
		int divScreenDiag = (int)(screenDiag / 5.0f);
		
		int clamped = PIN(divScreenDiag, 2, 24);
		int half = clamped << 2;
		float deg = 360.0f / (float)half;

		float curSin = 0.0f;
		float curCos = 1.0f;
		for (int i = 0; i < half; i++)
		{
			float local_18 = (float)(i + 1) * deg;
			float sin = Sine(local_18);
			float cos = Cosine(local_18);

			EVec3 radVec1(0.0f, pSphere->Radius * curSin, pSphere->Radius * curCos);
			EVec3 radVec2(0.0f, pSphere->Radius * sin, pSphere->Radius * cos);

			EVec3 vecEnd = *(EVec3*)pSphere + radVec2;
			EVec3 vecStart = *(EVec3*)pSphere + radVec1;
			AddLine(pThis, edx, &vecStart, &vecEnd, pCol, 1);
			
			SWAP(&radVec1.X, &radVec1.Y);
			SWAP(&radVec2.X, &radVec2.Y);

			vecEnd = *(EVec3*)pSphere + radVec2;
			vecStart = *(EVec3*)pSphere + radVec1;
			AddLine(pThis, edx, &vecStart, &vecEnd, pCol, 1);

			SWAP(&radVec1.Y, &radVec1.Z);
			SWAP(&radVec2.Y, &radVec2.Z);

			vecEnd = *(EVec3*)pSphere + radVec2;
			vecStart = *(EVec3*)pSphere + radVec1;
			AddLine(pThis, edx, &vecStart, &vecEnd, pCol, 1);

			curSin = sin;
			curCos = cos;
		}
	}

	static void __fastcall ResetFrame(EDebugLineManager* pThis, void* edx)
	{
		m_Lines.clear();
		m_LineCounts[0] = 0;
		m_LineCounts[1] = 0;
	}
};