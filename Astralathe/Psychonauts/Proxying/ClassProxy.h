#pragma once

#include "Utility.h"

template <class T>
class ClassProxy;

template <class T, typename N>
class VarProxy
{
private:
	ClassProxy<T>& parentProxy;
	int offsetFromClass;

public:
	VarProxy(ClassProxy<T>& parent, int offset)
		: parentProxy(parent), offsetFromClass(offset)
	{
	}

	inline N* GetPointer()
	{
		return (N*)(parentProxy.UnderlyingData + offsetFromClass);
	}
	inline N GetValue()
	{
		return *(GetPointer());
	}
	inline void SetValue(N value)
	{
		*(N*)(parentProxy.UnderlyingData + offsetFromClass) = value;
	}
};

template <class T>
class ClassProxy
{
	template <class T, typename N>
	friend class VarProxy;

protected:
	char* UnderlyingData;

	/// <summary>
	/// For ClassProxies that implement the ability to construct new instances of their wrapped class.
	/// Not every ClassProxy needs this (most, actually) because most don't implement this ability and only exist
	/// to manipulate existing class instances.
	/// For those that do, this will by default signal that UnderlyingData should be freed when the class is destroyed.
	/// </summary>
	bool SelfMade = false;

public:
	ClassProxy() { UnderlyingData = nullptr;  SelfMade = true; }
	ClassProxy(T* toProxy)
	{
		UnderlyingData = (char*)toProxy;
	}

	~ClassProxy()
	{
		if (SelfMade && UnderlyingData != nullptr)
		{
			free(UnderlyingData);
		}
	}

	inline T* AsBase()
	{
		return (T*)UnderlyingData;
	}

	inline bool IsValid()
	{
		return UnderlyingData != nullptr;
	}
};
