#pragma once

#include "ClassProxy.h"
#include "EArrayProxy.h"
#include "Psychonauts/EEntityManager.h"
#include "Psychonauts/EEntityInfo.h"
#include "Psychonauts/EArray.h"

class EEntityManagerProxy : public ClassProxy<EEntityManager>
{
public:

	VarProxy<EEntityManager, EArray<EEntityInfo>> Array;

	EEntityManagerProxy(EEntityManager* d) : ClassProxy(d),
		Array(*this, 0)
	{}

	EArrayProxy<EEntityInfo> GetArrayProxy()
	{
		return EArrayProxy<EEntityInfo>(Array.GetPointer(), 8);
	}
};

extern EEntityManagerProxy* gEntities_Proxy;