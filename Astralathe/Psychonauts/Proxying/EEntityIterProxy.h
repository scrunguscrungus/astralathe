#pragma once

#include "ClassProxy.h"
#include "Psychonauts/Proxying/EEntityProxy.h"
#include "Psychonauts/Proxying/EEntityManagerProxy.h"
#include "Psychonauts/Proxying/EEntityInfoProxy.h"
#include "Psychonauts/EEntityIter.h"

class EEntityIterProxy : public ClassProxy<EEntityIter>
{
public:
	VarProxy<EEntityIter, EEntityManager*> pEntManager;
	VarProxy<EEntityIter, int> iCurrent;

	EEntityIterProxy(EEntityIter* d) : ClassProxy(d),
		pEntManager(*this, 4),
		iCurrent(*this, 8)
	{}

	EEntityIterProxy(EDomain* pDomain, bool bRecurse, bool param_3) :
		pEntManager(*this, 4),
		iCurrent(*this, 8)
	{
		SelfMade = true;

		UnderlyingData = (char*)malloc(14);
		_EEntityIter::Construct((EEntityIter*)UnderlyingData, pDomain, bRecurse, param_3);
	}

	EEntity* operator*()
	{
		EEntityManagerProxy proxy(pEntManager.GetValue());
		EArrayProxy<EEntityInfo> entInfo = proxy.GetArrayProxy();

		EEntityInfo* info = entInfo[iCurrent.GetValue()];
		return EEntityInfoProxy(info).Entity.GetValue();
	}	

	//In the original, this is functionally the same as operator*
	//Here we use it for proxy shorthand
	std::shared_ptr<EEntityProxy> operator->()
	{
		return std::make_shared<EEntityProxy>(**this);
	}

	operator bool()
	{
		return iCurrent.GetValue() < (int)EEntityManagerProxy(pEntManager.GetValue()).GetArrayProxy().Num();
	}

	EEntityIterProxy& operator++()
	{
		_EEntityIter::OperatorIterate((EEntityIter*)UnderlyingData);
		return *this;
	}

	static void InitCallables()
	{
		SetupFunction(_EEntityIter::Construct, FROM_PATTERN(_EEntityIter::patConstructor));
		SetupFunction(_EEntityIter::OperatorIterate, FROM_PATTERN(_EEntityIter::patOperatorIterate));
	}
};