#pragma once

#include "ClassProxy.h"
#include "Psychonauts/GameApp.h"
#include "Psychonauts/ERenderer.h"
#include "Psychonauts/PsychoMaths.h"

class GameAppProxy : public ClassProxy<GameApp>
{
public:

	VarProxy<GameApp, EScriptVM*> m_ScriptVM;
	VarProxy<GameApp, char> m_pszCurrentLevel; //Current 4 letter level code (i.e. CAMA)
	VarProxy<GameApp, char> m_pszCurrentLevelFull; //Full name of the current level (i.e. CAMA_NIGHT)
	VarProxy<GameApp, char> m_pszCurrentLanguage;
	VarProxy<GameApp, bool> m_bHideHUD;

	VarProxy<GameApp, EMat4> m_matModelToWorldMatrix;
	VarProxy<GameApp, EMat4> m_matWorldToModelMatrix;

	VarProxy<GameApp, ERenderer*> m_pRenderer;
	VarProxy<GameApp, ECamera*> m_pRenderCamera;

	GameAppProxy(GameApp* d) : ClassProxy<GameApp>(d),
		m_bHideHUD(*this, 214),
		m_ScriptVM(*this, 39476),
		m_pszCurrentLevel(*this, 39066),
		m_pszCurrentLevelFull(*this, 39116),
		m_matModelToWorldMatrix(*this, 0x240),
		m_matWorldToModelMatrix(*this, 0x280),
		m_pRenderer(*this, 0x194),
		m_pRenderCamera(*this, 0x8c90),
		m_pszCurrentLanguage(*this, 38228)
	{
	}

	void SetModelToWorld(EMat4* newMtW)
	{
		m_matModelToWorldMatrix.SetValue(*newMtW);
	}
};