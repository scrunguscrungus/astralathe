#pragma once

#include "ClassProxy.h"
#include "Psychonauts/EEntity.h"
#include "Psychonauts/EEntityInfo.h"

class EEntityInfoProxy : public ClassProxy<EEntityInfo>
{
public:
	VarProxy<EEntityInfo, EEntity*> Entity;

	EEntityInfoProxy(EEntityInfo* d) : ClassProxy(d),
		Entity(*this, 0)
	{}
};