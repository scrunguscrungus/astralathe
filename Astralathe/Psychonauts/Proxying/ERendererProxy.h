#pragma once

#include "ClassProxy.h"
#include "Psychonauts/ERenderer.h"

class ERendererProxy : public ClassProxy<ERenderer>
{
public:
	VarProxy<ERenderer, unsigned int> m_iWidth;
	VarProxy<ERenderer, unsigned int> m_iHeight;
	VarProxy<ERenderer, int> m_iCurrentPushBufferLength;

	ERendererProxy(ERenderer* d) : ClassProxy<ERenderer>(d),
		m_iCurrentPushBufferLength(*this, 700),
		m_iWidth(*this, 708),
		m_iHeight(*this, 712)
	{}
};

template <typename T>
void RendererPushData(ERenderer* pRenderer, unsigned int** ppPushBuffer, T* data)
{
	memcpy(*ppPushBuffer, data, sizeof(T));
	
	*ppPushBuffer += sizeof(T) / 4;
		
	ERendererProxy renderer(pRenderer);
	renderer.m_iCurrentPushBufferLength.SetValue(renderer.m_iCurrentPushBufferLength.GetValue() + sizeof(T));
}