#pragma once

#include "ClassProxy.h"
#include "Psychonauts/ERenderState.h"

class ERenderStateProxy : public ClassProxy<ERenderState>
{
public:
	VarProxy<ERenderState, bool> m_bUnkBool1;
	VarProxy<ERenderState, bool> m_bUnkBool2;
	VarProxy<ERenderState, bool> m_bUnkBool3;

	VarProxy<ERenderState, unsigned int> m_ShadeMode;
	VarProxy<ERenderState, unsigned int> m_FillMode;
	VarProxy<ERenderState, unsigned int> m_CullMode;
	VarProxy<ERenderState, unsigned int> m_ZBufferMode;
	VarProxy<ERenderState, float> m_WireframeOffset;

	ERenderStateProxy(ERenderState* d) : ClassProxy(d),
		m_ShadeMode(*this, 0x10),
		m_FillMode(*this, 0x14),
		m_CullMode(*this, 0x18),
		m_ZBufferMode(*this, 0x20),
		m_WireframeOffset(*this, 0x4C),
		m_bUnkBool1(*this, 0x24),
		m_bUnkBool2(*this, 0x1D),
		m_bUnkBool3(*this, 0x25)
	{}

	ERenderStateProxy() :
		m_ShadeMode(*this, 0x10),
		m_FillMode(*this, 0x14),
		m_CullMode(*this, 0x18),
		m_ZBufferMode(*this, 0x20),
		m_WireframeOffset(*this, 0x4C),
		m_bUnkBool1(*this, 0x24),
		m_bUnkBool2(*this, 0x1D),
		m_bUnkBool3(*this, 0x25)
	{
		SelfMade = true;

		UnderlyingData = (char*)malloc(0x80);
		_ERenderState::Construct((ERenderState*)UnderlyingData);
	}

	static void InitCallables()
	{
		SetupFunction(_ERenderState::Construct, FROM_PATTERN(_ERenderState::patConstructor));
	}
};