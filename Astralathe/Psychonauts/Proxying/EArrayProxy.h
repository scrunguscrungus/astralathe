#pragma once

#include "ClassProxy.h"
#include "Psychonauts/EArray.h"

template <class T>
class EArrayProxy : public ClassProxy<EArray<T>>
{
private:
	int DataSize;
public:

	VarProxy<EArray<T>, unsigned int> memType_Num; //Bitfield of memory type and element count, we only care about element count
	VarProxy<EArray<T>, T*> Data;

	EArrayProxy(EArray<T>* d, int elementSize = 1) : ClassProxy<EArray<T>>(d),
		memType_Num(*this, 0),
		Data(*this, 8),
		DataSize(elementSize)
	{}

	unsigned int Num()
	{
		return memType_Num.GetValue() >> 5;
	}

	T* operator[](int idx)
	{
		return (T*)((char*)Data.GetValue() + DataSize * idx);
	}
	T* operator[](unsigned int idx)
	{
		return (T*)((char*)Data.GetValue() + DataSize * idx);
	}
};