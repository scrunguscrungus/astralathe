#ifndef LUA_H
#define LUA_H

//This header contains general Lua definitions as well as chunks pulled out of the Lua 4.0
//source code that are relevant for Astralathe's use.

struct lua_State;

/*
** types returned by `lua_type'
*/
#define LUA_TNONE	(-1)

#define LUA_TUSERDATA	0
#define LUA_TNIL	1
#define LUA_TNUMBER	2
#define LUA_TSTRING	3
#define LUA_TTABLE	4
#define LUA_TFUNCTION	5

#define LUA_MINSTACK	20

typedef struct IOCtrl {
	int ref[2];  /* ref for strings _INPUT/_OUTPUT */
	int iotag;    /* tag for file handles */
	int closedtag;  /* tag for closed handles */
} IOCtrl;

static const char* const filenames[] = { "_INPUT", "_OUTPUT" };

typedef int (*lua_CFunction) (lua_State* L);

struct luaL_reg {
	const char* name;
	lua_CFunction func;
};

#ifndef LUAL_BUFFERSIZE
#define LUAL_BUFFERSIZE	  512 /*BUFSIZ*/
#endif

typedef struct luaL_Buffer {
	char* p;			/* current position in buffer */
	int level;
	lua_State* L;
	char buffer[LUAL_BUFFERSIZE];
} luaL_Buffer;

#define luaL_putchar(B,c) \
  ((void)((B)->p < &(B)->buffer[LUAL_BUFFERSIZE] || luaL_prepbuffer(B)), \
   (*(B)->p++ = (char)(c)))

#define luaL_addsize(B,n)	((B)->p += (n))

#define plua_dofile "55 8B EC 51 8B 45 ? 50 8B 4D ? 51 E8 ? ? ? ? 83 C4 08 89 45 ? 83 7D ? 00 75 ? 6A FF"
typedef int ( *tlua_dofile )( lua_State*, const char* );
extern tlua_dofile lua_dofile;

#define plua_dobuffer "55 8B EC 51 8B 45 ? 50 8B 4D ? 51 8B 55 ? 52 8B 45 ?"
//typedef int ( *tlua_dobuffer )( lua_State* L, char* buff, size_t size, char* name );
//tlua_dobuffer lua_dobuffer;
extern int _cdecl lua_dobuffer(lua_State* L, const char* buff, int size, const char* name);

#define plua_getglobal "55 8B EC 51 8B 45 ? 8B 08 89 4D ? 8B 55 ? 52 8B 45 ? 50 E8 ? ? ? ? 83 C4 08 50 8B 4D ? 51 8B 55 ?"
typedef int ( *tlua_getglobal )( lua_State*, const char* );
extern tlua_getglobal lua_getglobal;

#define plua_type "55 8B EC 83 EC 08 8B 45 ? 50 8B 4D ? 51 E8 ? ? ? ? 83 C4 08 89 45 ? 83 7D ? 00 75 ? C7 45 ? FF FF FF FF"
typedef int ( *tlua_type )( lua_State*, int );
extern tlua_type lua_type;

#define plua_gettop "55 8B EC 8B 45 ? 8B 4D ? 8B 00"
typedef int ( *tlua_gettop )( lua_State* L );
extern tlua_gettop lua_gettop;

#define plua_settop "55 8B EC 83 7D ? 00 7C ? 8B 45 ? 50"
typedef void ( *tlua_settop )( lua_State*, int index );
extern tlua_settop lua_settop;

#define plua_setglobal "55 8B EC 51 8B 45 ? 8B 08 89 4D ? 8B 55 ? 52 8B 45 ? 50 E8 ? ? ? ? 83 C4 08 50 8B 4D ? 51 E8 ? ? ? ?"
typedef void ( *tlua_setglobal )( lua_State*, const char* name );
extern tlua_setglobal lua_setglobal;

#define plua_gettable "55 8B EC 83 EC 0C 83 7D ? 00 7C ? 8B 45 ? 8B 48 ? 8B 55 ? 8D 44 ? ? 89 45 ? EB ? 8B 4D ? 8B 11 8B 45 ? 8D 0C ? 89 4D ? 8B 55 ? 89 55 ? 8B 45 ? 8B 08 89 4D ? 8B 55 ? 52"
typedef void ( *tlua_gettable )( lua_State* L, int index );
extern tlua_gettable lua_gettable;

#define plua_settable "55 8B EC 83 EC 0C 83 7D ? 00 7C ? 8B 45 ? 8B 48 ? 8B 55 ? 8D 44 ? ? 89 45 ? EB ? 8B 4D ? 8B 11 8B 45 ? 8D 0C ? 89 4D ? 8B 55 ? 89 55 ? 8B 45 ? 8B 08 89 4D ? 8B 55 ? 83 EA 10"
typedef void ( *tlua_settable )( lua_State*, int );
extern tlua_settable lua_settable;

#define plua_settag "55 8B EC 51 8B 45 ? 50 8B 4D ? 51 E8 ? ? ? ? 83 C4 08 8B 55 ?"
typedef void (*tlua_settag)(lua_State* L, int tag);
extern tlua_settag lua_settag;

#define plua_settagmethod "55 8B EC 83 EC 0C 8B 45 ? 50 8B 4D ? 51 8B 55 ?"
typedef void(*tlua_settagmethod)(lua_State* L, int tag, const char* event);
extern tlua_settagmethod lua_settagmethod;

#define plua_pushstring "55 8B EC 83 EC 10 83 7D ? 00 75 ? 8B 45 ? 50 E8 ? ? ? ?"
typedef void ( *tlua_pushstring )( lua_State*, const char* );
extern tlua_pushstring lua_pushstring;

#define plua_pushlstring "55 8B EC 8B 45 ? 50 8B 4D ? 51 8B 55 ? 52 E8 ? ? ? ? 83 C4 0C 8B 4D ?"
typedef void (*tlua_pushlstring)(lua_State* L, const char* s, size_t len);
extern tlua_pushlstring lua_pushlstring;

#define plua_pushvalue "55 8B EC 8B 45 ? 50 8B 4D ? 51 E8 ? ? ? ? 83 C4 08 8B 10"
typedef void ( *tlua_pushvalue )( lua_State* L, int index );
extern tlua_pushvalue lua_pushvalue;

#define plua_pushnumber "55 8B EC 8B 45 ? 8B 08 D9 45 ?"
typedef void(*tlua_pushnumber)(lua_State* L, float n);
extern tlua_pushnumber lua_pushnumber;

#define plua_pushnil "55 8B EC 8B 45 ? 8B 08 C7 01 01 00 00 00"
typedef void ( *tlua_pushnil )( lua_State* );
extern tlua_pushnil lua_pushnil;

#define plua_pushusertag "55 8B EC 83 7D ? FF 74 ?"
typedef void(*tlua_pushusertag)(lua_State*, void* u, int tag);
extern tlua_pushusertag lua_pushusertag;

#define plua_pushcclosure "55 8B EC 8B 45 ? 50 8B 4D ? 51 8B 55 ? 52 E8 ? ? ? ? 83 C4 0C 5D"
typedef void(*tlua_pushcclosure)(lua_State* L, lua_CFunction fn, int n);
extern tlua_pushcclosure lua_pushcclosure;

#define plua_strlen "55 8B EC 83 EC 08 8B 45 ? 50 8B 4D ? 51 E8 ? ? ? ? 83 C4 08 89 45 ? 83 7D ? 00 74 ? 8B 55 ? 83 3A 03 74 ? 8B 45 ? 50 8B 4D ? 51 E8 ? ? ? ? 83 C4 08 85 C0 75 ? 8B 55 ? 8B 42 ? 8B 48 ?"
typedef size_t(*tlua_strlen)(lua_State* L, int index);
extern tlua_strlen lua_strlen;

#define plua_call "55 8B EC 83 EC 10 8B 45 ? 8D 0C C5 ? ? ? ?"
typedef int ( *tlua_call )( lua_State* L, int nargs, int nresults);
extern tlua_call lua_call;

#define plua_tag "55 8B EC 83 EC 08 8B 45 ? 50 8B 4D ? 51 E8 ? ? ? ? 83 C4 08 89 45 ? 83 7D ? 00 75 ? C7 45 ? FE FF FF FF"
typedef int ( *tlua_tag )( lua_State* L, int index );
extern tlua_tag lua_tag;

#define plua_newtag "55 8B EC 6A FF 68 FD FF FF 7F"
typedef int(*tlua_newtag)(lua_State* L);
extern tlua_newtag lua_newtag;

#define plua_ref "55 8B EC 83 EC 08 56 8B 45 ? 8B 08"
typedef int ( *tlua_ref )( lua_State* L, int lock );
extern tlua_ref lua_ref;

#define plua_unref "55 8B EC 83 7D ? 00 7C ? 8B 45 ? 6B C0 0C"
typedef void ( *tlua_unref )( lua_State* L, int ref );
extern tlua_unref lua_unref;

#define plua_remove "55 8B EC 51 8B 45 ? 50 8B 4D ? 51 E8 ? ? ? ? 83 C4 08 89 45 ? 8B 55 ? 83 C2 08"
typedef void( *tlua_remove )( lua_State*, int );
extern tlua_remove lua_remove;

#define plua_newuserdata "55 8B EC 51 6A 00 8B 45 ?"
typedef void* (*tlua_newuserdata)(lua_State* L, size_t size);
extern tlua_newuserdata lua_newuserdata;

#define plua_touserdata "55 8B EC 83 EC 08 8B 45 ? 50 8B 4D ? 51 E8 ? ? ? ? 83 C4 08 89 45 ? 83 7D ? 00 74 ? 8B 55 ? 83 3A 00"
typedef void*(*tlua_touserdata)(lua_State*, int);
extern tlua_touserdata lua_touserdata;

#define plua_tonumber "55 8B EC 83 EC 08 8B 45 ? 50 8B 4D ? 51 E8 ? ? ? ? 83 C4 08 89 45 ? 83 7D ? 00 74 ? 8B 55 ? 83 3A 02"
typedef double(*tlua_tonumber)(lua_State* L, int index);
extern tlua_tonumber lua_tonumber;

#define plua_isnumber "55 8B EC 83 EC 0C 8B 45 ? 50 8B 4D ? 51 E8 ? ? ? ? 83 C4 08 89 45 ? 83 7D ? 00 75 ? C7 45 ? 00 00 00 00 EB ? 8B 55 ? 83 3A 02"
typedef int(*tlua_isnumber)(lua_State* L, int index);
extern tlua_isnumber lua_isnumber;

#define pLua_DoCallStackDump "55 8B EC 83 EC 74 8B 45 ? 83 C0 01"

#define pluaL_check_lstr "55 8B EC 51 8B 45 ? 50 8B 4D ? 51 E8 ? ? ? ? 83 C4 08 89 45 ? 83 7D ? 00 75 ? 6A 03"
typedef char* (*tluaL_check_lstr)(lua_State*, int numArg, size_t* len);
extern tluaL_check_lstr luaL_check_lstr;

#define plua_error "55 8B EC 83 7D ? 00 74 ? 8B 45 ? 50"
typedef void(*tlua_error)(lua_State* L, const char* s);
extern tlua_error lua_error;


#define pluaL_argerror "55 8B EC 83 EC 60 8D 45 ? 50 6A 00"
typedef void (*tluaL_argerror)(lua_State* L, int numarg, const char* extramsg);
extern tluaL_argerror luaL_argerror;

#define pluaL_checkstack "55 8B EC 8B 45 ? 50 E8 ? ? ? ? 83 C4 04 39 45 ?"
typedef void(*tluaL_checkstack)(lua_State* L, int space, const char* msg);
extern tluaL_checkstack luaL_checkstack;

#define pluaL_pushresult "55 8B EC 8B 45 ? 50 E8 ? ? ? ? 83 C4 04 8B 4D ? 83 79 ? 00"
typedef void(*tluaL_pushresult)(luaL_Buffer* B);
extern tluaL_pushresult luaL_pushresult;

#define pgetfilebyref "55 8B EC 51 8B 45 ? 50 E8 ? ? ? ? 83 C4 04 8B 4D ?"
typedef void*(*tgetfilebyref)(lua_State* L, IOCtrl* ctrl, int inout);
extern tgetfilebyref getfilebyref;

#define pgethandle "55 8B EC 83 EC 08 8B 45 ? 50 8B 4D ? 51 E8 ? ? ? ? 83 C4 08 89 45 ? 83 7D ? 00 74 ? 8B 55 ? 52"
typedef void* (*tgethandle)(lua_State* L, IOCtrl* ctrl, int f);
extern tgethandle gethandle;

#define pluaL_buffinit "55 8B EC 8B 45 ? 8B 4D ? 89 48 ? 8B 55 ? 83 C2 0C"
typedef void (*tluaL_buffinit)(lua_State* L, luaL_Buffer* B);
extern tluaL_buffinit luaL_buffinit;

#define pluaL_prepbuffer "55 8B EC 8B 45 ? 50 E8 ? ? ? ? 83 C4 04 85 C0 74 ? 8B 4D ? 51"
typedef char* (*tluaL_prepbuffer)(luaL_Buffer* B);
extern tluaL_prepbuffer luaL_prepbuffer;

#define pluaL_opt_number "55 8B EC 8B 45 ? 50 8B 4D ? 51 E8 ? ? ? ? 83 C4 08 83 F8 FF 75 ? D9 45 ?"
typedef	float(*tluaL_opt_number)(lua_State* L, int narg, float def);
extern tluaL_opt_number luaL_opt_number;

const char* luaL_opt_lstr(lua_State* L, int narg, const char* def, size_t* len);

//#define pio_close "55 8B EC 51 6A FF 8B 45 ?"
//extern lua_CFunction io_close;

#define luaL_opt_long(L,n,d)	((long)luaL_opt_number(L, n,d))
#define luaL_opt_string(L,n,d)	(luaL_opt_lstr(L, (n), (d), NULL))
#define luaL_check_string(L,n)	(luaL_check_lstr(L, (n), NULL))
#define luaL_arg_check(L, cond,numarg,extramsg) if (!(cond)) \
                                               luaL_argerror(L, numarg,extramsg)

#define lua_pop(L,n)		lua_settop(L, -(n)-1)

#define lua_pushuserdata(L,u)	lua_pushusertag(L, u, 0)

#define lua_isfunction(L,n)	(lua_type(L,n) == LUA_TFUNCTION)
#define lua_istable(L,n)	(lua_type(L,n) == LUA_TTABLE)
#define lua_isuserdata(L,n)	(lua_type(L,n) == LUA_TUSERDATA)
#define lua_isnil(L,n)		(lua_type(L,n) == LUA_TNIL)
#define lua_isnull(L,n)		(lua_type(L,n) == LUA_TNONE)

#endif