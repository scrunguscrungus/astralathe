#pragma once

class EDebugLineManager
{

};

namespace _EDebugLineManager
{
	constexpr const char* patConstructor = "55 8B EC 51 89 4D ? 8B 45 ? C7 00 00 00 00 00 6A 00 6A 00";

	constexpr const char* patGeneralA = "55 8B EC 51 89 4D ? 8B E5 5D C2 10 00";
	constexpr const unsigned int AddLineIdx = 0;
	//Actual: 1
	constexpr const unsigned int AddArrowIdx = 0;
	//Actual: 2
	constexpr const unsigned int AddBoxIdx = 0;
	//Actual: 3
	constexpr const unsigned int AddCircleIdx = 0;

	constexpr const char* patAddCylinder = "55 8B EC 51 89 4D ? 8B E5 5D C2 18 00";
	constexpr const unsigned int AddCylinderIdx = 0;

	constexpr const char* patAddSphere = "55 8B EC 51 89 4D ? 8B E5 5D C2 08 00";
	constexpr const unsigned int AddSphereIdx = 0;

	constexpr const char* patGeneralD = "55 8B EC 51 89 4D ? 8B E5 5D C3";
	constexpr const unsigned int DrawAllIdx = 2;
	//Actual: 3
	constexpr const unsigned int ResetFrameIdx = 2; //Seems the same as DrawAll, but this is because when we hook DrawAll its pattern no longer matches. Ouch.

	enum Hooks
	{
		AddLine = 0,
		ResetFrame,
		DrawAll,
		AddBox
	};


}