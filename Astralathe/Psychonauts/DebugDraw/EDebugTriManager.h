#pragma once

#include "Psychonauts/PsychoMaths.h"

class EDebugTriManager
{

};

namespace _EDebugTriManager
{
	//SHARED WITH ADDFRUSTRUM AND ADDTRANSFORMEDSPHERE WHICH WE DON'T IMPLEMENT YET
	constexpr const char* patAddTri = "55 8B EC 51 89 4D ? 8B E5 5D C2 0C 00";
	constexpr const unsigned int AddTriIdx = 2;

	//Shared with DebugLineManager patGeneralD
	constexpr const char* patGeneric = "55 8B EC 51 89 4D ? 8B E5 5D C3";
	//Actual: 5
	constexpr const unsigned int DrawAllIdx = 3;
	//Actual: 6
	constexpr const unsigned int ResetFrameIdx = 3;
}