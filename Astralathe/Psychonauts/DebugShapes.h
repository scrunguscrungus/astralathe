#pragma once

#include "PsychoMaths.h"

struct VERTEX_NOTEX_DIFF
{
	EVec3 Position;
	unsigned int Colour = 0;
};

class EDebugLine
{
public:
	unsigned int _dummy = 0;
	VERTEX_NOTEX_DIFF Start;
	VERTEX_NOTEX_DIFF End;
	unsigned int Flags = 0;

	unsigned int GetSortKey()
	{
		return ((Flags & 2) != 0) ? 1 : 0;
	}
};

class EDebugTri
{
public:
	VERTEX_NOTEX_DIFF Vert1;
	VERTEX_NOTEX_DIFF Vert2;
	VERTEX_NOTEX_DIFF Vert3;
	unsigned int Flags = 0;

	unsigned int GetSortKey()
	{
		if ((this->Flags & 2) == 0)
		{
			if ((this->Vert1.Colour & 0xff000000) == 0xff000000)
			{
				return 0;
			}
			return 1;
		}
		return 2;
	}
};