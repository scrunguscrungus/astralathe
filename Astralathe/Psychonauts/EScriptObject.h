#ifndef SCRIPTOBJECT_H
#define SCRIPTOBJECT_H


#define pEScriptObject_CallMethodf "55 8B EC 83 EC 08 8D 45 ? 89 45 ? 8B 4D ? 51 8B 55 ? 52 8B 45 ? 50 8B 4D ? E8 ? ? ? ?"
#define pEScriptObject_SendMessage "55 8B EC 51 89 4D ? 8B 45 ? 50 68 ? ? ? ? 8B 4D ? 51 8B 55 ? 52 8B 45 ? 50 8B 4D ? 51 E8 ? ? ? ? 83 C4 18 8B E5 5D C2 10 00"
#define pEScriptObject_SetTableValue "55 8B EC 83 EC 08 89 4D ? A1 ? ? ? ? 05 34 9A 00 00 89 45 ? 8B 4D ? 51 8B 55 ? 52 8B 45 ? 50 8B 4D ? E8 ? ? ? ? 8B E5 5D C2 08 00"
#define pEScriptObject_SetTableValue_idx 4
#define pEScriptObject_GetName 0x1CAE50

class EScriptObject {
};

typedef void( __thiscall* tEScriptObject_SendMessage )( EScriptObject*, const char*, EScriptObject*, const char*, int );
extern tEScriptObject_SendMessage EScriptObject_SendMessage;

typedef void( __stdcall* tEScriptObject_CallMethodf )( EScriptObject*, const char*, const char*, ... );
extern tEScriptObject_CallMethodf EScriptObject_CallMethodf;

typedef char*( __thiscall* tEScriptObject_GetName )( EScriptObject* );
extern tEScriptObject_GetName EScriptObject_GetName;

typedef void( __thiscall* tEScriptObject_SetTableValue )( EScriptObject*, const char*, int );
extern tEScriptObject_SetTableValue EScriptObject_SetTableValue;

#endif