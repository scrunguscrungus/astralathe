#ifndef EMAINPAGE_H
#define EMAINPAGE_H

#define pEMainPage_StartUp 0x2571B0
#define pEMainPage_DrawMenu "55 8B EC 81 EC 1C 05 00 00"
#define pEMainPage_RenderMenu 0x257F00
#define pEMainPage_ItemSelected 0x257A70


class EMainPage {
public:
	char field_0x0[304];
	char bFlagThatFixesInput; //Cool name, I know
};

typedef void( __thiscall* tEMainPage_DrawMenu )( EMainPage* );
extern tEMainPage_DrawMenu EMainPage_DrawMenu;

#endif