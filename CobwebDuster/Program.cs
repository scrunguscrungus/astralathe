﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Diagnostics;

using PsychoPortal;
using System.Collections.Generic;

namespace CobwebDuster
{
	class Program
	{
		const string Version = "1.2"; //Change whenever something is adjusted that warrants re-extraction
		static void WriteVersionFile()
		{
			Directory.CreateDirectory( "native_assets" );
			File.WriteAllText( "native_assets/na_ver.txt", Version );
		}

		static int Main(string[] args)
		{
			Trace.Listeners.Clear();

			TextWriterTraceListener logOut = new TextWriterTraceListener("./astralathe_cobwebduster_log.log");
			logOut.Name = "LogFile";
			logOut.TraceOutputOptions = TraceOptions.DateTime;

			Trace.Listeners.Add(logOut);

			ConsoleTraceListener ctl = new ConsoleTraceListener(false);

			Trace.Listeners.Add(ctl);

			Trace.AutoFlush = true;

			Console.WriteLine("CobwebDuster 1.1");
			Console.WriteLine("Psychonauts asset unpacker for use with Astralathe");
			Console.WriteLine();
			Console.WriteLine("This tool will perform first-time setup to prepare your Psychonauts installation for modding.");
			Console.WriteLine("This setup is necessary for some mods to function but can be skipped if you do not plan to install mods for Psychonauts.");
			Console.WriteLine();
			Console.WriteLine("Setup will require approximately 420MB of free space.");
			Console.WriteLine();
			while ( true )
			{
				Console.WriteLine("Proceed?");
				Console.WriteLine("\t1. Yes");
				Console.WriteLine("\t2. No");
				Console.WriteLine("\t3. No, don't ask me again");
				ConsoleKey choice = Console.ReadKey(true).Key;
				Console.WriteLine();

				switch (choice)
				{
					case ConsoleKey.D1:
						if ( !Unpack() )
						{
							Console.WriteLine("An error occurred during unpacking.");
							Console.ReadKey(true);
							return 1;
						}
						//Console.WriteLine("Process complete!\nPress any key to close.");
						//Console.ReadKey(true);
						return 0;
					case ConsoleKey.D2:
						return 0;
					case ConsoleKey.D3:
						Directory.CreateDirectory("native_assets");
						Console.WriteLine("You will not be prompted again. If you wish to unpack in the future, please run Astralathe_CobwebDuster manually or delete the \"native_assets\" folder in your game directory.");
						Console.WriteLine("Press enter to continue");
						Console.ReadLine();
						return 0;
				}
			}
		}

		static void Error(string error)
		{
			Trace.WriteLine(error);
			Console.ReadKey(true);
		}

		static string MakeNAPath(string path)
		{
			string napath;
			if ( path.ToLower().StartsWith("workresource") )
			{
				napath = path.ToLower().Replace("workresource", "native_assets");
			}
			else
			{
				napath = Path.Combine("native_assets", path);
			}
			napath = napath.Replace('\\', '/');
			string naDir = Path.GetDirectoryName(napath);
			Directory.CreateDirectory(naDir);
			return napath;
		}

		static string MakeLuaClassPath(string canonicalname)
		{
			string path = "Scripts/" + canonicalname.Replace('.', '/') + ".lua";
			return MakeNAPath(path);
		}

		//Takes
		static DDS_PixelFormat GetPixelFormat(TextureFormat texFormat)
		{
			//Shortcut consts to save me some typing.
			const DDS_PixelFormat.DDS_PixelFormatFlags RGB = DDS_PixelFormat.DDS_PixelFormatFlags.DDPF_RGB;
			const DDS_PixelFormat.DDS_PixelFormatFlags RGBA = DDS_PixelFormat.DDS_PixelFormatFlags.DDPF_ALPHAPIXELS | DDS_PixelFormat.DDS_PixelFormatFlags.DDPF_RGB;

			DDS_PixelFormat fmt = new DDS_PixelFormat();
			fmt.StructSize = 32; //Ray why do you make me set this manually??
			switch ( texFormat )
			{
				//DXT is easy
				case TextureFormat.Format_DXT1:
					fmt.FourCC = DDS_PixelFormat.D3DFMT_DXT1;
					fmt.Flags |= DDS_PixelFormat.DDS_PixelFormatFlags.DDPF_FOURCC;
					break;
				case TextureFormat.Format_DXT3:
					fmt.FourCC = DDS_PixelFormat.D3DFMT_DXT3;
					fmt.Flags |= DDS_PixelFormat.DDS_PixelFormatFlags.DDPF_FOURCC;
					break;
				case TextureFormat.Format_DXT5:
					fmt.FourCC = DDS_PixelFormat.D3DFMT_DXT5;
					fmt.Flags |= DDS_PixelFormat.DDS_PixelFormatFlags.DDPF_FOURCC;
					break;

				//Here comes the rest
				case TextureFormat.Format_8888:
					fmt.Flags |= RGBA;
					fmt.RGBBitCount = 0x20;
					fmt.RBitMask = 0x00ff0000;
					fmt.GBitMask = 0x0000ff00;
					fmt.BBitMask = 0x000000ff;
					fmt.ABitMask = 0xff000000;
					break;
				case TextureFormat.Format_0888:
					fmt.Flags |= RGB;
					fmt.RGBBitCount = 0x18;
					fmt.RBitMask = 0xff0000;
					fmt.GBitMask = 0x00ff00;
					fmt.BBitMask = 0x0000ff;
					break;

				case TextureFormat.Format_1555:
					fmt.Flags |= RGBA;
					fmt.RGBBitCount = 0x10;
					fmt.RBitMask = 0x7c00;
					fmt.GBitMask = 0x03e0;
					fmt.BBitMask = 0x001f;
					fmt.ABitMask = 0x8000;
					break;
				case TextureFormat.Format_4444:
					fmt.Flags |= RGBA;
					fmt.RGBBitCount = 0x10;
					fmt.RBitMask = 0x0f00;
					fmt.GBitMask = 0x00f0;
					fmt.BBitMask = 0x000f;
					fmt.ABitMask = 0xf000;
					break;

				case TextureFormat.Format_565:
					fmt.Flags |= RGB;
					fmt.RGBBitCount = 0x10;
					fmt.RBitMask = 0xf800;
					fmt.GBitMask = 0x7e0;
					fmt.BBitMask = 0x001f;
					break;

				case TextureFormat.Format_L8:
					fmt.RGBBitCount = 8;
					fmt.RBitMask = 0xff;
					fmt.Flags |= DDS_PixelFormat.DDS_PixelFormatFlags.DDPF_LUMINANCE;
					break;

				case TextureFormat.Format_V8U8:
					fmt.RGBBitCount = 0x10;
					fmt.RBitMask = 0x00ff;
					fmt.GBitMask = 0xff00;
					fmt.Flags |= (DDS_PixelFormat.DDS_PixelFormatFlags)0x80000;
					break;

				case TextureFormat.Format_P8:
					fmt.RGBBitCount = 8;
					fmt.RBitMask = 0;
					fmt.GBitMask = 0;
					fmt.GBitMask = 0;
					fmt.ABitMask = 0;
					fmt.Flags |= (DDS_PixelFormat.DDS_PixelFormatFlags)0x20;
					Trace.WriteLine("P8 texture, not sure if conversion is correct?");
					break;

				default:
					throw new Exception($"Unknown format {texFormat}");
			}

			return fmt;
		}

		static DDS TexFrameToDDS(TextureFrame frame)
		{
			DDS converted = new DDS
			{
				Header = new DDS_Header
				{
					Width = frame.Width,
					Height = frame.Height,
					MipMapCount = frame.MipMapLevels == 0 ? (uint)frame.Faces[0].Surfaces.Length : frame.MipMapLevels,
					PixelFormat = GetPixelFormat(frame.Format),
					StructSize = 124
				},
				Palette = frame.Palette,
				Faces = frame.Faces
			};
			if ( frame.Type == TextureType.Cubemap )
			{
				converted.Header.Caps2 |= DDS_Header.DDS_Caps2Flags.DDS_CUBEMAP_ALLFACES;
			}
			return converted;
		}

		static void WriteATX(string path, string basename, GameTexture texture)
		{
			StringBuilder atxOut = new StringBuilder();

			atxOut.AppendLine($"numframes\t{texture.Frames.Length}");
			atxOut.AppendLine($"startframe\t{texture.AnimInfo.LoopFrame}");
			atxOut.AppendLine($"framerate\t{texture.AnimInfo.FramesPerSecond}");
			string playmode;
			switch ( texture.AnimInfo.PlayMode )
			{
				case AnimPlayMode.Loop:
					playmode = "loop";
					break;
				case AnimPlayMode.LoopOnce:
					playmode = "looponce";
					break;
				case AnimPlayMode.Stop:
					playmode = "stop";
					break;
				case AnimPlayMode.Oscillate:
					playmode = "oscillate";
					break;
				default:
					throw new Exception("Texture is using a playmode that I didn't think was used in the base game?");
			}
			atxOut.AppendLine($"playmode\t{playmode}");

			for ( int i = 0; i < texture.Frames.Length; i++ )
			{
				string texname = $"{basename}_{i}";
				atxOut.AppendLine($"texture\t{texname}");
			}

			Trace.WriteLine($"Writing ATX file {path}");
			File.WriteAllText(path, atxOut.ToString());

		}

		//Lots of asset duplication in the PPFs, save time by skipping stuff we already extracted
		static HashSet<string> hashAlreadySeenFiles = new HashSet<string>();

		static bool Unpack()
		{
			Trace.WriteLine("Beginning unpacking process...");
			if ( !Directory.Exists("WorkResource/PCLevelPackFiles") )
			{
				Error("Failed to find the WorkResource/PCLevelPackFiles folder");
				return false;
			}
			int doFileIndex = 0;
			string[] PPFs = Directory.GetFiles("WorkResource/PCLevelPackFiles", "*.ppf", SearchOption.TopDirectoryOnly);
			foreach ( string PPF in PPFs )
			{
				string ppfName = Path.GetFileNameWithoutExtension(PPF);
				//2024/11/28 - Common extraction enabled because there's a few assets in there like models
				//that folks might want access to
				//if (ppfName == "common") //Ignore common because the game loads it at startup
				//	continue;

				Trace.WriteLine($"Loading {ppfName}.ppf");
				PackPack pack;
				try
				{
					pack = Binary.ReadFromFile<PackPack>(PPF, onPreSerializing: (_,p) => p.Pre_IsCommon = ppfName == "common");
				}
				catch ( Exception e )
				{
					Error($"FAILED to read PPF!\n{e.Message}\nPress any key to continue.");
					return false;
				}

				TexturePack tpf = pack.TexturePack;
				{
					Trace.WriteLine("Dumping textures");
					foreach ( GameTexture tex in tpf.Textures )
					{

						string napath = MakeNAPath(tex.FileName);
						if ( hashAlreadySeenFiles.Contains(napath) )
						{
#if DEBUG
							Console.WriteLine($"Skipping {napath}, already extracted");
#endif
							continue;
						}
						else
						{
							hashAlreadySeenFiles.Add(napath);
						}
						if ( tex.Frames.Length < 2 )
						{
							TextureFrame frame = tex.Frames.First();
							DDS dds;
							try
							{
								dds = TexFrameToDDS(frame);
							}
							catch ( Exception e )
							{
								Error($"Conversion of {tex.FileName} to DDS failed: {e.Message}");
								return false;
							}

							Trace.WriteLine($"Writing converted texture {napath}");
							Binary.WriteToFile(dds, napath);
						}
						else
						{
							string filepath = Path.GetDirectoryName(napath);
							string filename = Path.GetFileNameWithoutExtension(napath);
							string atxfilename = filename + ".atx";

							try
							{
								WriteATX(Path.Combine(filepath, atxfilename), filename, tex);
							}
							catch ( Exception e )
							{
								Error($"Creation of ATX for {tex.FileName} failed: {e.Message}");
								return false;
							}

							for (int i = 0; i < tex.Frames.Length; i++)
							{
								TextureFrame frame = tex.Frames[i];

								string texname = $"{filepath}{Path.DirectorySeparatorChar}{filename}_{i}{Path.GetExtension(napath)}";

								DDS dds;

								try
								{
									dds = TexFrameToDDS(frame);
								}
								catch (Exception e)
								{
									Error($"Conversion of frame {i} of {tex.FileName} to DDS failed: {e.Message}");
									return false;
								}

								Trace.WriteLine($"Writing texture frame {texname}");
								Binary.WriteToFile(dds, texname);
							}
						}
					}
				}

				MeshPack mpf = pack.MeshPack;
				foreach ( PackedScene scene in mpf.MeshFiles )
				{
					string napath = MakeNAPath(scene.FileName);
					if (hashAlreadySeenFiles.Contains(napath))
					{
#if DEBUG
						Console.WriteLine($"Skipping {napath}, already extracted");
#endif
						continue;
					}
					else
					{
						hashAlreadySeenFiles.Add(napath);
					}
					Trace.WriteLine($"Writing {napath}");
					Binary.WriteToFile(scene.Scene, napath);
				}

				//Level scene
				{
					//NOT NEEDED AND CONFUSES THE GAME
					//string napath = MakeNAPath($"Levels/{ppfName}.plb");
					//Console.WriteLine($"Writing {napath}");
					//Binary.WriteToFile(pack.Scene, napath);

					//Referenced scenes
					if ( pack.ReferencedScenes != null )
					{
						Trace.WriteLine("Dumping referenced scenes");
						for (int i = 0; i < pack.ReferencedScenes.Length; i++)
						{
							Scene scene = pack.ReferencedScenes[i];
							string name = pack.Scene.RootDomain.RuntimeReferences[i];

							string napath_ref = MakeNAPath(name);
							if (hashAlreadySeenFiles.Contains(napath_ref))
							{
#if DEBUG
							Console.WriteLine($"Skipping {napath_ref}, already extracted");
#endif
								continue;
							}
							else
							{
								hashAlreadySeenFiles.Add(napath_ref);
							}
							Trace.WriteLine($"Writing {napath_ref}");
							Binary.WriteToFile(scene, napath_ref);
						}
					}
				}

				ScriptPack lpf = pack.ScriptPack;

				Trace.WriteLine("Dumping script classes");
				foreach ( ScriptClass sclass in lpf.Classes )
				{
					string napath = MakeLuaClassPath(sclass.ClassName);
					if (hashAlreadySeenFiles.Contains(napath))
					{
#if DEBUG
						Console.WriteLine($"Skipping {napath}, already extracted");
#endif
						continue;
					}
					else
					{
						hashAlreadySeenFiles.Add(napath);
					}
					using (FileStream fs = File.Open(napath, FileMode.Create))
					{
						Trace.WriteLine($"Writing {sclass.ClassName} to {napath}");
						fs.Write(sclass.ScriptCode, 0, sclass.ScriptCode.Length);
					}
				}

				Trace.WriteLine("Dumping DoFiles");
				
				foreach ( ScriptDoFile sdf in lpf.DoFiles )
				{
					if ( !sdf.Pre_HasName )
					{
						Console.WriteLine("DoFile has no name");
						sdf.Name = $"DoFile_{doFileIndex}.lua";
						doFileIndex++;
					}
					string napath = MakeNAPath(sdf.Name);
					if (hashAlreadySeenFiles.Contains(napath))
					{
#if DEBUG
						Console.WriteLine($"Skipping {napath}, already extracted");
#endif
						continue;
					}
					else
					{
						hashAlreadySeenFiles.Add(napath);
					}
					using ( FileStream fs = File.Open(napath, FileMode.Create))
					{
						Trace.WriteLine($"Writing {sdf.Name} to {napath}");
						fs.Write(sdf.Buffer, 0, sdf.Buffer.Length);
					}
				}
			}

			return true;
		}
	}
}
