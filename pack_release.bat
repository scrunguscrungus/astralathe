@echo off

set /p version="Version: "

set zipname="Astralathe_V%version%.zip"

cd Release

7z a -tzip "%zipname%" -i@"../release_files_to_pack.txt"

pause
