ASTRALATHE_ENABLED = 1

_ERRORMESSAGE = function(msg)
	GamePrint("LUA ERROR: "..msg)
end

--HOOK: AddSpriteHandle
if _AddSpriteHandle then
	AddSpriteHandle = _AddSpriteHandle
end
_AddSpriteHandle = AddSpriteHandle
function HookedAddSpriteHandle(tex, x, y, a, xs, ys, rot, show, uvx, uvy, r, g, b, bFullscreen)
	if not a then
        return _AddSpriteHandle(tex, x, y)
    elseif not xs then
        return _AddSpriteHandle(tex, x, y, a)
    elseif not ys then
        return _AddSpriteHandle(tex, x, y, a, xs)
    elseif not rot then
        return _AddSpriteHandle(tex, x, y, a, xs, ys)
    elseif not show then
        return _AddSpriteHandle(tex, x, y, a, xs, ys, rot)
    elseif not uvx then
        return _AddSpriteHandle(tex, x, y, a, xs, ys, rot, show)
    elseif not uvy then
        return _AddSpriteHandle(tex, x, y, a, xs, ys, rot, show, uvx)
    elseif not r then
        return _AddSpriteHandle(tex, x, y, a, xs, ys, rot, show, uvx, uvy)
    elseif not g then
        return _AddSpriteHandle(tex, x, y, a, xs, ys, rot, show, uvx, uvy, r)
    elseif not b then
        return _AddSpriteHandle(tex, x, y, a, xs, ys, rot, show, uvx, uvy, r, g)
    else
        return _AddSpriteHandle(tex, x, y, a, xs, ys, rot, show, uvx, uvy, r, g, b)
    end
end
AddSpriteHandle = HookedAddSpriteHandle