ASTRALATHE_WIDESCREEN = 1

if _AddSpriteHandle then
	AddSpriteHandle = _AddSpriteHandle

end

_AddSpriteHandle = AddSpriteHandle

--Base game fullscreen sprites hack
_FULLSCREEN_SPRITES = {
	'textures/leveltextures/ll_lungfishlair/ll_lungfish_view.dds'
}

function IsFullscreenSpriteTex(tex)
	for i,v in _FULLSCREEN_SPRITES do
        if v == tex then
            return 1
        end
    end
    return nil
end

function HookedAddSpriteHandle(tex, x, y, a, xs, ys, rot, show, uvx, uvy, r, g, b, bFullscreen)
	
	local fst = IsFullscreenSpriteTex(tex)
	if fst ~= nil or bFullscreen then
		w,h = GetViewPortDimensions()

		local hTex = LoadTexture(tex) --This might leak?
		local texW,texH = GetTextureDimensions(hTex)

		local aspect = w/h
		local fHudScale = 1.0 / ( ( ( 4.0 / 3.0 ) ) / ( aspect ) );
		
		xs = xs * fHudScale
	end

	if not a then
        return _AddSpriteHandle(tex, x, y)
    elseif not xs then
        return _AddSpriteHandle(tex, x, y, a)
    elseif not ys then
        return _AddSpriteHandle(tex, x, y, a, xs)
    elseif not rot then
        return _AddSpriteHandle(tex, x, y, a, xs, ys)
    elseif not show then
        return _AddSpriteHandle(tex, x, y, a, xs, ys, rot)
    elseif not uvx then
        return _AddSpriteHandle(tex, x, y, a, xs, ys, rot, show)
    elseif not uvy then
        return _AddSpriteHandle(tex, x, y, a, xs, ys, rot, show, uvx)
    elseif not r then
        return _AddSpriteHandle(tex, x, y, a, xs, ys, rot, show, uvx, uvy)
    elseif not g then
        return _AddSpriteHandle(tex, x, y, a, xs, ys, rot, show, uvx, uvy, r)
    elseif not b then
        return _AddSpriteHandle(tex, x, y, a, xs, ys, rot, show, uvx, uvy, r, g)
    else
        return _AddSpriteHandle(tex, x, y, a, xs, ys, rot, show, uvx, uvy, r, g, b)
    end
end

AddSpriteHandle = HookedAddSpriteHandle