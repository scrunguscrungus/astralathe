#include "Logging.h"
#include "AstralatheConfig.h"

#include <stdio.h>
#include <iostream>

std::ofstream logstream;
static std::filesystem::path logPath;

ILogger* g_Logger;

void InitConsole()
{
	AllocConsole();
	FILE* pDummy;
	freopen_s(&pDummy, "CONIN$", "r", stdin);
	freopen_s(&pDummy, "CONOUT$", "w", stdout);
	freopen_s(&pDummy, "CONOUT$", "w", stderr);
}

void LogInit(const char* pszFilename)
{
	logPath = std::filesystem::path(pszFilename);

	logstream.open( logPath, std::ios::out | std::ios::trunc );
	logstream.close();
}

void LogSetAdditionalLogger(ILogger* logger)
{
	g_Logger = logger;
}

void VLogMessageRaw( const char* msg, va_list args )
{
	logstream.open( logPath, std::ios::out | std::ios::app );

	char outmsg[ 2048 ];
	vsprintf_s( outmsg, msg, args );
	logstream << outmsg;

	logstream.close();

	vprintf( msg, args );
}
void LogMessageRaw( const char* msg, ... )
{
	va_list args;
	va_start( args, msg );

	VLogMessageRaw( msg, args );

	va_end( args );
}

void LogToLogger(const char* msg, va_list args)
{
	if (g_Logger == nullptr)
	{
		return;
	}

	char buf[4096];
	vsprintf_s(buf, msg, args);

	g_Logger->LogAstralatheMessage(buf);

	va_end(args);
}

void LogMessage( const char* msg, ... )
{
	va_list args;
	va_start(args, msg);

	va_list args2;
	va_copy(args2, args);
	LogToLogger(msg, args2);

	if ( !AstralatheConfig::GetConfigValue( CONFIG_ASTRALATHEOUTPUT ) )
		return;

	LogMessageRaw( "[Astralathe] " );
	VLogMessageRaw( msg, args );

	va_end( args );
}
void LogMessage( ConfigOption filter, const char* msg, ... )
{

	if (!AstralatheConfig::GetConfigValue(filter))
		return;

	va_list args;
	va_start(args, msg);

	LogToLogger(msg, args);

	if ( !AstralatheConfig::GetConfigValue( CONFIG_ASTRALATHEOUTPUT ) )
		return;

	LogMessageRaw( "[Astralathe] " );
	VLogMessageRaw( msg, args );

	va_end( args );
}