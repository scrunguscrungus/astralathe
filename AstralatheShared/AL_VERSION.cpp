#include "AL_VERSION.h"

namespace AstralatheVersion
{
	VersionInfo AstralatheVersion::ParseVersion(std::string verString)
	{
		VersionInfo i;
		std::istringstream iss(verString);
		std::string element;

		if (std::getline(iss, element, '.'))
		{
			//Hack to strip V from the front of GitLab tags
			if (tolower(element[0]) == 'v')
			{
				element = element.substr(1);
			}

			try
			{
				i.major = std::stoi(element);
			}
			catch (...)
			{
				return ERROR_VERSION; //Bad major
			}
		}
		else
		{
			return ERROR_VERSION; //Malformed version number (No major version)
		}

		if (std::getline(iss, element, '.'))
		{
			try
			{
				i.minor = std::stoi(element);
			}
			catch (...)
			{
				return ERROR_VERSION; //Bad minor
			}
		}
		else
		{
			return ERROR_VERSION; //Also malformed version number (No minor version)
		}

		//Optional, okay for this line to fail
		if (std::getline(iss, element, '.'))
		{
			try
			{
				i.rev = std::stoi(element);
			}
			catch (...)
			{
				i.rev = 0;
			}
		}

		return i;
	}
}