#include "AstralatheConfig.h"
#include "Logging.h"

#include <stdio.h>
#include <iostream>

CIniReader AstralatheConfig::config( "astralathe.ini" );

const char* pszConfigNames[] = {
#define defEnum(x) #x,
#include "ConfigEnum.h"
#undef defEnum
};

std::unordered_map<ConfigOption, MappedConfigOption> ConfigOptions = {
	//Logging
	{ CONFIG_DEBUGOUTPUT , new AstralatheConfigOption<bool>("DebugLogging", true, "Logging", false) },
	{ CONFIG_ASTRALATHEOUTPUT , new AstralatheConfigOption<bool>("AstralatheLogging", true, "Logging", false) },
	{ CONFIG_VERBOSEHASHBASESPEW , new AstralatheConfigOption<bool>("VerboseHashBaseSpew", false , "Logging", false) },
	{ CONFIG_VERBOSEFILEMANAGER , new AstralatheConfigOption<bool>("VerboseFileManagerSpew", false , "Logging", false) },
	{ CONFIG_DEBUGLOADSINGLEMESH , new AstralatheConfigOption<bool>("VerboseLoadSingleMesh", false , "Logging", false) },
	{ CONFIG_DEBUGIMGUI, new AstralatheConfigOption<bool>("DebugImGui", false , "Logging", false) },
	
	//Debugging
	{ CONFIG_DEBUGMENU , new AstralatheConfigOption<bool>("DebugMenu", true , "Debugging", false) },
	{ CONFIG_LEVELSELECT , new AstralatheConfigOption<bool>("LevelSelect", false , "Debugging") },
	{ CONFIG_CHEATKEYS , new AstralatheConfigOption<bool>("CheatHotkeys", false , "Debugging", false) },

	//Fixes
	{ CONFIG_FIX_FX, new AstralatheConfigOption<bool>("FixScreenEffects", true, "Fixes")},
	{ CONFIG_FIX_FX_SCALE, new AstralatheConfigOption<float>("ScreenEffectScale", 1.0f, "FixScreenEffects", false) },
	{ CONFIG_FIX_WORLDTOSCREEN , new AstralatheConfigOption<bool>("FixWorldToScreen", true , "Fixes") },
	{ CONFIG_WIDESCREEN_AUTODETECT, new AstralatheConfigOption<bool>("AutodetectWidescreen", true, "Widescreen", false)},
	{ CONFIG_FIX_WIDESCREEN , new AstralatheConfigOption<bool>("FixWidescreen", false , "Fixes", false) },
	{ CONFIG_WIDESCREEN_HUDOFFSET, new AstralatheConfigOption<float>("WidecreenHudOffset", 100.0f, "Widescreen", false)},

	//For Fun
	{ CONFIG_MAJESCO , new AstralatheConfigOption<bool>("Majesco", false , "For Fun") },
	{ CONFIG_OLDMENU , new AstralatheConfigOption<bool>("EnableOldMenu", false , "For Fun") },

	//Enhancements
	{ CONFIG_ENHANCEMENT_DISCORDRP, new AstralatheConfigOption<bool>("Discord Rich Presence", true, "Enhancements")},

	//Misc
	{ CONFIG_USE_STEAM, new AstralatheConfigOption<bool>("Use Steam", true, "Misc.")},
	{ CONFIG_AUTOHOOK, new AstralatheConfigOption<bool>("Autohook", true, "Misc.", false) }
};

struct VisitInitConfigVar
{
	VisitInitConfigVar(CIniReader* c) : config(c) {}

	void operator()(AstralatheConfigOption<bool>* conf)
	{
		LogMessage("Config BOOL: %s\n", conf->GetName());
		bool bVal = config->ReadBoolean(conf->GetCategory(), conf->GetName(), conf->GetDefault());
		conf->SetValue(bVal);
	}
	void operator()(AstralatheConfigOption<float>* conf)
	{
		LogMessage("Config FLOAT: %s\n", conf->GetName());
		float fVal = config->ReadFloat(conf->GetCategory(), conf->GetName(), conf->GetDefault());
		conf->SetValue(fVal);
	}
	void operator()(AstralatheConfigOption<int>* conf)
	{
		LogMessage("Config INT: %s\n", conf->GetName());
		int iVal = config->ReadInteger(conf->GetCategory(), conf->GetName(), conf->GetDefault());
		conf->SetValue(iVal);
	}

	CIniReader* config;
};


void AstralatheConfig::Init()
{
	//Read config, create defaults if not exist
	printf( "[Astralathe] ---CONFIG INIT---\n" );
	for (std::pair<ConfigOption, MappedConfigOption> const& o : ConfigOptions)
	{
		MappedConfigOption Option = o.second;
			
		std::visit(VisitInitConfigVar(&config), Option);
	}
	Write(); //Write the config so we save defaults if we need to
	printf( "[Astralathe] ---CONFIG INITIALISED---\n" );
}

const char* AstralatheConfig::GetConfigOptionName(ConfigOption option)
{
	if (option <= CONFIG_VERSION || option >= CONFIG_COUNT)
		return "CONFIG_OUT_OF_RANGE";

	return pszConfigNames[(int)option];
}
bool AstralatheConfig::GetConfigValue(ConfigOption option)
{
	return GetConfigValue<bool>(option);
}

struct VisitWriteConfigVar
{
	VisitWriteConfigVar(CIniReader* c) : config(c) {}

	void operator()(AstralatheConfigOption<bool>* conf)
	{
		config->WriteBoolean(conf->GetCategory(), conf->GetName(), conf->GetValue());	
	}
	void operator()(AstralatheConfigOption<float>* conf)
	{
		config->WriteFloat(conf->GetCategory(), conf->GetName(), conf->GetValue());
	}
	void operator()(AstralatheConfigOption<int>* conf)
	{
		config->WriteInteger(conf->GetCategory(), conf->GetName(), conf->GetValue());
	}

	CIniReader* config;
};

void AstralatheConfig::Write()
{
	for (std::pair<ConfigOption, MappedConfigOption> const& o : ConfigOptions)
	{
		MappedConfigOption Option = o.second;

		std::visit(VisitWriteConfigVar(&config), Option);
	}
}